<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";

   $sql = "SELECT 
               Ipal.idInspeccion AS 'Consecutivo', 
               Ipal.idIpal AS 'idIpal', 
                Inspecciones.fechaIngreso AS 'Fecha',
                Ipal.Empresa AS 'Empresa', 
                Ipal.Direccion AS 'Direccion', 
                DatosUsuarios.idLogin AS 'idLogin', 
                DatosUsuarios.Nombre AS 'Usuario', 
                Ipal.NoContrato AS 'NoContrato', 
                Ipal.Observaciones AS 'Observaciones', 
                Ipal.Zona AS 'Zona',
                Ipal.Cudrillero AS 'Recibio',
                Ipal.Cuadrillero2 AS 'CargoEmpresa',
                Ipal.resultado AS 'Resultado' 
            FROM 
               Ipal
                INNER JOIN DatosUsuarios ON Ipal.idLogin = DatosUsuarios.idLogin
                INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
            WHERE
               Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
               AND Inspecciones.Estado = 1
            ORDER BY
              Ipal.resultado DESC;";

   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
         public $Consecutivo;
         public $idIpal;
         public $Fecha;
         public $Empresa;
         public $Direccion;
         public $idLogin;
         public $Usuario;
         public $NoContrato;
         public $Observaciones;
         public $Zona;
         public $Recibio;
         public $CargoEmpresa;
         public $Resultado;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->idIpal = utf8_encode($row['idIpal']);
            $Descripciones[$idx]->Consecutivo = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->Fecha = utf8_encode($row['Fecha']);
            $Descripciones[$idx]->Empresa = utf8_encode($row['Empresa']);
            $Descripciones[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Descripciones[$idx]->idLogin = utf8_encode($row['idLogin']);
            $Descripciones[$idx]->Usuario = utf8_encode($row['Usuario']);
            $Descripciones[$idx]->NoContrato = utf8_encode($row['NoContrato']);
            $Descripciones[$idx]->Observaciones = utf8_encode($row['Observaciones']);
            $Descripciones[$idx]->Zona = utf8_encode($row['Zona']);
            $Descripciones[$idx]->Recibio = utf8_encode($row['Recibio']);
            $Descripciones[$idx]->CargoEmpresa = utf8_encode($row['CargoEmpresa']);
            $Descripciones[$idx]->Resultado = utf8_encode($row['Resultado']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>