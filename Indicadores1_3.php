<?php
    include_once "../../Include/VarGlobales.PHP";
    include_once "../../Include/BdSqlClases.php";

     $q = new ConecteMysql($ServidorBD,$UsrBD,$ClaveBD,$NomBD);
    
    /*
    $Desde = $_POST['fechaDesde'];
    $Hasta = $_POST['fechaHasta'];
    $Nivel = $_POST['Nivel'];
    */
    $Desde = '2014-01-01';
    $Hasta = '2014-11-19';
    $Nivel = '2';
    

    $fechaDesde = "";
    $fechaHasta = "";

    if ($Desde <> "")
    {
      $fechaDesde = " and CONVERT(VARCHAR(10),Acc_Horas_Hombre.Fecha_HH,120) >= '$Desde' ";
    }
    if ($Hasta <> "")
    {
      $fechaHasta = " and CONVERT(VARCHAR(10),Acc_Horas_Hombre.Fecha_HH,120) <= '$Hasta' ";
    }

    $fechaDesdeA = "";
    $fechaHastaA = "";

    if ($Desde <> "")
    {
      $fechaDesdeA = " and CONVERT(VARCHAR(10),Acc_FechaAcc,120) >= '$Desde' ";
    }
    if ($Hasta <> "")
    {
      $fechaHastaA = " and CONVERT(VARCHAR(10),Acc_FechaAcc,120) <= '$Hasta' ";
    }
     
  $sql ="SELECT 
          EMPRESA_CONTRATOS.E" . $Nivel . " AS 'SubGerencia',
          SUM(AccHorasHombre.HorasHombre) AS 'HorasHombre',
          COUNT(Acc_Principal.Acc_id_General) AS 'ATC',
          ROUND(((COUNT(Acc_Principal.Acc_id_General)/AccHorasHombre.HorasHombre) * 1000000),2,0) AS 'IFC',
          SUM(Acc_Incapacidades.INC_DIAS_INCAPACIDAD) AS 'Dias de Incapacidad',
          ROUND(((SUM(Acc_Incapacidades.INC_DIAS_INCAPACIDAD)/AccHorasHombre.HorasHombre) * 1000),2,0) AS 'IGC'
        FROM
          Acc_Principal
            LEFT JOIN Acc_Incapacidades ON Acc_Principal.Acc_id_General = Acc_Incapacidades.Acc_id_General
            INNER JOIN EMPRESA_CONTRATOS ON Acc_Principal.EC_ID = EMPRESA_CONTRATOS.EC_ID
            LEFT JOIN (
              SELECT 
                Contrato, SUM(HorasHombre) AS 'HorasHombre'
              FROM 
                Acc_Horas_Hombre 
              WHERE 
                Contrato <> ' ' 
                $fechaDesde 
                $fechaHasta 
              GROUP BY Contrato)
              AS AccHorasHombre ON EMPRESA_CONTRATOS.EC_CONTRATO = AccHorasHombre.Contrato
        WHERE
          Acc_Incapacidades.Acc_id_General IS NOT NULL
          AND Acc_Principal.Acc_Directriz = 'COMPUTABLE'
          $fechaDesdeA 
          $fechaHastaA
        GROUP BY 
          EMPRESA_CONTRATOS.E" . $Nivel . ",
          AccHorasHombre.HorasHombre
        ORDER BY 1 asc;";
  
  $q->ejecutar($sql, 73, "Semaforos/Indicadores1_2.php");
  
  class Respuesta
  {
    public $SubGerencia;
    public $HorasHombre;
    public $HorasHombre2;
    public $ATC;
    public $IFC;
    public $IFT;
    public $Incapacidad;
    public $IncapacidadTotales;
    public $IGC;
    public $IGT;
  };
  
  if ($q->filas() > 0)
  {     
    $idx = 0; 
    $datos = array();
    while($q->Cargar())
    {
        $datos[$q->dato(0)] = new Respuesta();
        $datos[$q->dato(0)]->HorasHombre = $datos[$q->dato(0)]->HorasHombre + $q->dato(1);
        $datos[$q->dato(0)]->ATC = $datos[$q->dato(0)]->ATC + $q->dato(2);
        $datos[$q->dato(0)]->IFC = $datos[$q->dato(0)]->IFC + $q->dato(3);
        $datos[$q->dato(0)]->Incapacidad = $datos[$q->dato(0)]->Incapacidad + $q->dato(4);
        $datos[$q->dato(0)]->IGC = $datos[$q->dato(0)]->IGC + $q->dato(5);
    }
    /*
    foreach ($datos as $key => $value) 
    {
      $Respuestas[$idx] = new Respuesta();
      $Respuestas[$idx]->SubGerencia = $key;
      $Respuestas[$idx]->HorasHombre = $value->HorasHombre;
      $Respuestas[$idx]->ATC = $value->ATC;
      $Respuestas[$idx]->IFC = $value->IFC;
      $Respuestas[$idx]->Incapacidad = $value->Incapacidad;
      $Respuestas[$idx]->IGC = $value->IGC;
      $idx++;
    }
/**/
  $fechaDesde = str_replace("Acc_Horas_Hombre", "AHH", $fechaDesde);
  $fechaHasta = str_replace("Acc_Horas_Hombre", "AHH", $fechaHasta);

    $sql ="SELECT
            HHH.Division AS 'Div', 
            COUNT(P.Acc_id_General) AS 'Acc', 
            HHH.TotalHoras as HorasHombre,
            ROUND(COUNT(P.Acc_id_General) / HHH.TotalHoras * 1000000, 2, 0) AS 'IFC', 
            SUM(AI.INC_DIAS_INCAPACIDAD) AS 'Dias de Incapacidad', 
            ROUND(SUM(AI.INC_DIAS_INCAPACIDAD) / HHH.TotalHoras * 1000, 2, 0) AS 'IGC'
          FROM            
              (SELECT 
                  E" . $Nivel . ", 
                  EC_ID, 
                  EC_CONTRATO
                FROM
                  EMPRESA_CONTRATOS AS EC1
              ) AS EC 
              LEFT JOIN Acc_Principal AS P ON EC.EC_ID = P.EC_ID AND EC.EC_ID = P.EC_ID 
              LEFT JOIN
                (SELECT
                    EC.E" . $Nivel . " AS Division, 
                    SUM(AHH.HorasHombre) AS TotalHoras
                  FROM
                    Acc_Horas_Hombre AS AHH LEFT JOIN
                    EMPRESA_CONTRATOS AS EC ON AHH.Contrato = EC.EC_CONTRATO
                  WHERE
                    Contrato <> ' ' 
                    $fechaDesde 
                    $fechaHasta                 
                  GROUP BY 
                    EC.E" . $Nivel . "
                ) AS HHH ON EC.E" . $Nivel . " = HHH.Division 
              LEFT JOIN Acc_Incapacidades AS AI ON P.Acc_id_General = AI.Acc_id_General 
          WHERE 
            HHH.TotalHoras <> 0 
            AND HHH.TotalHoras IS NOT NULL 
            AND HHH.TotalHoras <> ''
            $fechaDesdeA 
            $fechaHastaA
          GROUP BY 
            HHH.Division, 
            HHH.TotalHoras;";
    
    $q->ejecutar($sql, 155, "Semaforos/Indicadores1_2.php");

    if ($q->filas() > 0)
    {     
      while($q->Cargar())
      {
          if (!isset($datos[$q->dato(0)]))
          {
            $datos[$q->dato(0)] = new Respuesta();
          }
          $datos[$q->dato(0)]->HorasHombre2 = $datos[$q->dato(0)]->HorasHombre + $q->dato(2);
          $datos[$q->dato(0)]->IFT = $datos[$q->dato(0)]->IFC + $q->dato(3);
          $datos[$q->dato(0)]->IncapacidadTotales = $datos[$q->dato(0)]->Incapacidad + $q->dato(4);
          $datos[$q->dato(0)]->IGT = $datos[$q->dato(0)]->IGC + $q->dato(5);
      }
      foreach ($datos as $key => $value) 
      {
        $Respuestas[$idx] = new Respuesta();
        $Respuestas[$idx]->SubGerencia = $key;
        $Respuestas[$idx]->HorasHombre = $value->HorasHombre;
        $Respuestas[$idx]->ATC = $value->ATC;
        $Respuestas[$idx]->IFC = $value->IFC;
        $Respuestas[$idx]->Incapacidad = $value->Incapacidad;
        $Respuestas[$idx]->IGC = $value->IGC;
        $Respuestas[$idx]->HorasHombre2 = $value->HorasHombre2;
        $Respuestas[$idx]->IFT = $value->IFT;
        $Respuestas[$idx]->IncapacidadTotales = $value->IncapacidadTotales;
        $Respuestas[$idx]->IGT = $value->IGT;
        $idx++;
      }
    }
/**/

    echo json_encode($datos);

  } else
  {
    echo 0;
  }
?>