<?php
/*
	function sqlite_open($location) 
	{ 
	    $handle = new SQLite3($location); 
	    return $handle; 
	} 
	function sqlite_query($dbhandle,$query) 
	{ 
	    $array['dbhandle'] = $dbhandle; 
	    $array['query'] = $query; 
	    $result = $dbhandle->query($query); 
	    return $result; 
	} 
	function sqlite_fetch_array(&$result,$type) 
	{ 
	    #Get Columns 
	    $i = 0; 
	    while ($result->columnName($i)) 
	    { 
	        $columns[ ] = $result->columnName($i); 
	        $i++; 
	    } 
	    
	    $resx = $result->fetchArray(SQLITE3_ASSOC); 
	    return $resx; 
	} 

	$caminodb = "db/prueba01.db"; 
	$db = sqlite_open($caminodb) or die("No puedo abrir la base de datos ");

	sqlite_query($db, "CREATE TABLE personas(id INTEGER PRIMARY KEY, nombre, apellidos)");

*/
	/*
	$idx = 0;
	$Horas = 0;
	$maxHoras = 192;
	 
	$fechaInicio = strtotime('2014-10-01 08:00:00')	;
	$fechaFin = strtotime('2014-10-31 17:00:00');
	$fechaActual = $fechaInicio;
	while ($fechaActual <= $fechaFin)
	{
		if ($maxHoras > 0 AND $Horas >= $maxHoras)
		{
			$fechaActual = strtotime('+1 day', $fechaFin );
		} else
		{
			if (date('w', $fechaActual) > 0)
			{			
					while ($idx <= 7)
					{
						if (date('H', $fechaActual) != 13 )
							{
								$idx++;
								$Horas++;
								//echo "<br>" . date('Y-m-d H:i:s', $fechaActual) . ": $Horas Horas";
								$Prefijo = '059' . date('YmdHis', $fechaActual);
								echo "(null, 0, '" . date('Y-m-d H:i:s', $fechaActual) . "', '59', '" . $Prefijo . "', 16, 0, 0, 0, 0, 88, 1), <br>";
							}
								$fechaActual = strtotime('+1 hour', $fechaActual );
					}
				$fechaActual = strtotime('-10 hour', $fechaActual );
			}		
		}
			$fechaActual = strtotime('+1 day', $fechaActual );
			$idx = 0;
	}*/

	include("php/conectar.php"); 
	$link = Conectar();

	$sql = "SHOW COLUMNS FROM Baremo";
	$result = $link->query(utf8_decode($sql));

	$idx = 0;

	class Titulo
    {
	    public $Campo;
	    public $Tipo;
	    public $Nulo;
	    public $Llave;
	    public $Defecto;
	    public $Extra;
    }

	while ($row = mysqli_fetch_assoc($result))
	{ 
		$Titulos[$idx] = new Titulo();
		$Titulos[$idx]->Campo = utf8_encode($row['Field']);
		$Titulos[$idx]->Tipo = utf8_encode($row['Type']);
		$Titulos[$idx]->Nulo = utf8_encode($row['Null']);
		$Titulos[$idx]->Llave = utf8_encode($row['Key']);
		$Titulos[$idx]->Defecto = utf8_encode($row['Default']);
		$Titulos[$idx]->Extra = utf8_encode($row['Extra']);

		$idx++;
	}
     
	mysqli_free_result($result);  
	echo json_encode($Titulos);

?>