<?php
//plupload_done
//plupload_failed
    date_default_timezone_set('America/Bogota');

    include("../../../php/conectar.php");
    $link=Conectar();

    $nomArchivo = addslashes($_POST['NomArchivo']);
    $archivo = "./files/" . $nomArchivo;

    $sql = "SELECT COUNT(*) AS 'Cantidad' FROM EEC_ComConsolidado_In WHERE nomArchivo = '$nomArchivo';";
   $result = $link->query($sql);

   $fila =  $result->fetch_array(MYSQLI_ASSOC);

   if ($fila['Cantidad'] == 0)
   {
        $fecha = date("Y-m-d H:i:s");

        $fila = 1;

        $resultado = "";
        $numErrores = 0;
        $numInsertados = 0;
        $values = "";
        $values2 = "";
        if (($gestor = fopen($archivo, "r")) !== FALSE) 
        {
            while (($datos = fgetcsv($gestor, 0, "|")) !== FALSE) 
            {
                $numero = count($datos);
                if ($numero <> 29)
                {
                    $resultado .= "En la fila <b>$fila</b> el numero de campos no coincide <br>";
                    $numErrores++;
                } else
                {
                    if ($fila > 1)
                    {
                        $values .= ", ";
                        $values2 .= ", ";
                    }
                    $values .= "(";
                            $tmpCoordenadas = str_replace(":", ",", $datos[23]);
                        $values2 .= "(" .
                                        "'" . $datos[0] . "'," .
                                        "'" . addslashes($datos[2]) . " Sucursal: " . addslashes($datos[21]) . "'," .
                                        "'" . date("Y-m-d H:i:s", strtotime($datos[6])) . "'," .
                                        "'" . date("Y-m-d H:i:s", strtotime($datos[6])) . "'," .
                                        "'" . addslashes($datos[22]) . "'," .
                                        "'" . addslashes($datos[17]) . "'," .
                                        "'" . addslashes($datos[3]) . "'," .
                                        "'" . addslashes($datos[15]) . "'," .
                                        "'" . addslashes($datos[14]) . " " . addslashes($datos[13]) . "'," .
                                        "'" . $tmpCoordenadas . "')";

                    for ($c=0; $c < $numero; $c++) //Validación Campo por campo
                    {
                        if ($c > 0)
                        {
                            $values .= ", ";
                        }
                        if ($c == 6)
                        {
                            $values .= "'" . date("Y-m-d H:i:s", strtotime($datos[$c])) . "'";
                        } else
                        {
                            $values .= "'" . addslashes($datos[$c]) . "'";
                        }
                        
                    }              
                    $values .= ", '$fecha', '$nomArchivo')";
                }
                $fila++;

            }
            fclose($gestor);
            if (strlen($values) > 10)
            {
                
                $sql = "INSERT INTO Contratos (Nombre, Descripcion, FechaInicio, FechaTerminacion, ValorContrato, Objeto, Responsabilidades, Contratante, Contratista, Coordenadas) VALUES $values2";
                $link->query($sql);
                $numInsertados = $link->affected_rows;

                $sql = "INSERT IGNORE INTO EEC_ComConsolidado_In 
                            (IDOT, SE, TIPO, ACTIVIDAD, ACTA, FORMATO, FECHACUMPLE, IDCUADRILLA, TCUADRILLA, ZONA, CICLO, GRUPO, CLASIFICACION, MUNICIPIO, DIRECCION, NOMBRECLIENTE, MEDIDORACTIVA, OBSSOLICITUD, OBSRESPUESTA, ANOMALIAS, RESULTADO, IDSUCURSARL, DEUDA, COORDENADAS, PTO_FIS_WM, CTO_DTB, CNR, ESTADO, VALIDACION, FechaCargue, nomArchivo) 
                            VALUES $values;";
                
                $link->query($sql);
                if ($numInsertados < 0)
                    {$numInsertados = 0;} else
                {
                    $sql = "INSERT INTO Mensajes (idRemitente, idDestinatario, Fecha, Asunto, Mensaje, Estado) VALUES
                        (1, 1, '" . date("Y-m-d H:i:s") . "', 'Asignacion de Ordenes', 'Te asignaron $numInsertados Ordenes de Comercial, por favor revisa el Panel', 'Pendiente');";
                        $link->query(utf8_decode($sql));
                }
            }
            $duplicados = $fila - 1 - $numInsertados - $numErrores;
            echo "<h4>$nomArchivo</h4><br><b>Lineas Leidas: </b>" . ($fila - 1). "<br><b>Lineas Insertadas: </b>$numInsertados<br><b>Anomalias: </b>$numErrores<br><b>Lineas Duplicadas: </b>$duplicados<br>$resultado";

        }
    }
?>
