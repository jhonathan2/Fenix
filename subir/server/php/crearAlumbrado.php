<?php
//plupload_done
//plupload_failed
    date_default_timezone_set('America/Bogota');

    include("../../../php/conectar.php");
    $link=Conectar();

    $nomArchivo = addslashes($_POST['NomArchivo']);
    $archivo = "./files/" . $nomArchivo;

    $sql = "SELECT COUNT(*) AS 'Cantidad' FROM EEC_ApConsolidado_In WHERE nomArchivo = '$nomArchivo';";
   $result = $link->query($sql);

   $fila =  $result->fetch_array(MYSQLI_ASSOC);

   if ($fila['Cantidad'] == 0)
   {
        $fecha = date("Y-m-d H:i:s");

        $fila = 1;

        $resultado = "";
        $numErrores = 0;
        $numInsertados = 0;
        $values = "";
        $values2 = "";
        if (($gestor = fopen($archivo, "r")) !== FALSE) 
        {
            while (($datos = fgetcsv($gestor, 0, "|")) !== FALSE) 
            {
                $numero = count($datos);
                if ($numero <> 15)
                {
                    $resultado .= "En la fila <b>$fila</b> el numero de campos no coincide <br>";
                    $numErrores++;
                } else
                {
                    if ($fila > 1)
                    {
                        $values .= ", ";
                        $values2 .= ", ";
                    }
                    $values .= "(";

                        $values2 .= "(" .
                                        "'Alumbrado " . $datos[6] . "'," .
                                        "'" . addslashes($datos[4]) . " Sucursal: 6000 " . "'," .
                                        "'" . date("Y-m-d H:i:s", strtotime($datos[2])) . "'," .
                                        "'" . date("Y-m-d H:i:s", strtotime($datos[2])) . "'," .
                                        "'0'," .
                                        "'" . addslashes($datos[10]) . "'," .
                                        "'_'," .
                                        "'" . addslashes($datos[9]) . "'," .
                                        "'" . addslashes($datos[8]) . "')";

                    for ($c=0; $c < $numero; $c++) //Validación Campo por campo
                    {
                        if ($c > 0)
                        {
                            $values .= ", ";
                        }
                        if ($c == 3)
                        {
                            $values .= "'" . date("Y-m-d H:i:s", strtotime(addslashes($datos[$c]))) . "'";
                        } else
                        {
                            $values .= "'" . addslashes($datos[$c]) . "'";
                        }
                        
                    }              
                    $values .= ", '$fecha', '$nomArchivo')";
                }
                $fila++;

            }
            fclose($gestor);
            if (strlen($values) > 10)
            {
                $sql = "INSERT INTO Contratos (Nombre, Descripcion, FechaInicio, FechaTerminacion, ValorContrato, Objeto, Responsabilidades, Contratante, Contratista) VALUES $values2";
                $link->query(utf8_decode($sql));

                $sql = "INSERT INTO EEC_ApConsolidado_In 
                            (ID, FECHA_ENTR, OT, TIPO_OT, USUARIO_PROG, CD, LUMINARIA, DIRECCION, MUNICIPIO, OBSERVACION, Trafo_nox, Trafo_noy, ilu_nox, ilu_noy, fechaCargue, nomArchivo) 
                            VALUES $values;";
                $link->query(utf8_decode($sql));
                $numInsertados = $link->affected_rows;
                if ($numInsertados < 0)
                    {$numInsertados = 0;} else
                {
                    $sql = "INSERT INTO Mensajes (idRemitente, idDestinatario, Fecha, Asunto, Mensaje, Estado) VALUES
                        (1, 1, '" . date("Y-m-d H:i:s") . "', 'Asignacion de Ordenes', 'Te asignaron <b>$numInsertados</b> Ordenes de Alumbrado, por favor revisa el Panel', 'Pendiente');";
                        $link->query(utf8_decode($sql));
                }
            }
            $duplicados = $fila - 1 - $numInsertados - $numErrores;
            echo "<h4>$nomArchivo</h4><br><b>Lineas Leidas: </b>" . ($fila - 1). "<br><b>Lineas Insertadas: </b>$numInsertados<br><b>Anomalias: </b>$numErrores<br><b>Lineas Duplicadas: </b>$duplicados<br>$resultado";


        }
    }
?>
