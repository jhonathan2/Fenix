function cargarProfile(pIdLogin)
{
  $.post("php/cargarUsuario.php",
  {
    idLogin : pIdLogin
  }, function(data, textStatus, xhr)
  {
    if (data.idLogin == 0)
    {
      Mensaje("Error", "No se encontraron los Datos del Usuario.");
    } else
    {
      $(".cargarArchivos_Usuario").attr("idLogin", data.idLogin);
      $(".cargarArchivos_Usuario").attr("Nombre", data.Nombre);
      $(".cargarMateriales_Usuario").attr("Usuario", data.Nombre);
      $("#txtProfile_Imagen").attr("src", data.foto);
      $("#txtProfile_Nombre").html(data.Nombre + "<br/><small>" + data.Cargo + "</small>");
      $("#txtProfile_Usuario").text(data.Usuario);
      $("#txtProfile_Empresa").text(data.Empresa);
      $("#txtProfile_Estado").text(data.Estado);
      $("#txtProfile_Correo").text(data.Correo);
      $("#txtProfile_Perfil").text(data.Perfil);

      $("#txtProfile_Novedades dt").remove();
      $("#txtProfile_Novedades dd").remove();
      var tds = "";

      $.each(data.Novedades, function(index, value)
        {
          //tds += "<dt><strong>" + value.Titulo + " <br/><small>" + value.Fecha + "</small></strong></dt>";
          tds += "<dt title='" + value.Titulo + "'><strong>" + value.Titulo + "</strong></dt>";
          tds += "<dd>" + value.Descripcion + "</dd>";
        });
      $("#txtProfile_Novedades").append(tds);
    }

  }, "json").always(function() 
  {
    
  }).fail(function() {
    Mensaje("Error", "No fue posible cargar los Datos del Usuario.");
  });
  $("#Profile_CrearNovedad").unbind("submit");
  $("#Profile_CrearNovedad").on("submit", function(evento)
            {
              evento.preventDefault();
              var pTitulo = $('#txtProfileCrearNovedad_Nombre').val();
              var pNovedad = $('#txtProfileCrearNovedad_Observaciones').val();

              var tds = "";
              $.post("php/crearNovedad.php",
              {
                usuario : $("#txtProfile_Usuario").text(),
                titulo : pTitulo,
                novedad : pNovedad
              }, function(data, textStatus, xhr)
              {
                if (data == 1)
                {
                  Mensaje("Ok", "La Novedad ha sido almacenada.");
                  tds += "<dt title='" + pTitulo + "'><strong>" + pTitulo + "</strong></dt>";
                  tds += "<dd>" + pNovedad + "</dd>";
                  $("#txtProfile_Novedades").append(tds);
                  $("#Profile_CrearNovedad")[0].reset();
                } else
                {
                  Mensaje("Error", data);    
                }
              }).always(function() 
              {
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No fue posible almacenar la Novedad, por favor intenta nuevamente.");
              });
              
            });
}