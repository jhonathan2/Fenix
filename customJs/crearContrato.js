function crearContrato()
{
  $("#grpContratoFechaInicio input, #grpContratoFechaTerminacion input").datepicker("destroy");
    $("#grpContratoFechaInicio input, #grpContratoFechaTerminacion input").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });

          $("#grpContratoFechaInicio .add-on").on("click", function()
          {
            $("#grpContratoFechaInicio input").datepicker("show");
          });
          $("#grpContratoFechaTerminacion .add-on").on("click", function()
          {
            $("#grpContratoFechaTerminacion input").datepicker("show");
          });

          $("#frmCrearContrato").on("submit", function(evento)
            {
              evento.preventDefault();
              $.post("php/crearContrato.php",
              {
                nombre : $('#txtContrato_Nombre').val(),
                descripcion : $('#txtContrato_Descripcion').val(),
                fechaInicio : $('#txtContrato_FechaInicio').val(),
                fechaTerminacion : $('#txtContrato_FechaTerminacion').val(),
                valor : $('#txtContrato_Valor').val(),
                objeto : $('#txtContrato_Objeto').val(),
                responsabilidades : $('#txtContrato_Responsabilidades').val(),
                contratante : $('#txtContrato_Contratante').val(),
                contratista : $('#txtContrato_Contratista').val(),
                pUsuario : $('#txtContrato_Usuario').val(),
                idLogin : Usuario.id
              }, function(data, textStatus, xhr)
              {
                if (data == 1)
                {
                  Mensaje("Ok", "El contrato ha sido almacenado.");    
                  $("#frmCrearContrato")[0].reset();
                } else
                {
                  Mensaje("Error", data);    
                }
              }).always(function() 
              {
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No fue posible almacenar el contrato, por favor intenta nuevamente.");
              });
              
            });
  $("#txtContrato_Usuario").cargarUsuarios({idUsuario: Usuario.id},function(Usuarios)
          {
            var idx = 0;
            var tds = '<option value="0">Seleccione un Usuario</option>';
            $.each(Usuarios, function(index, Usuario)
              {
                if (Usuario.idPerfil == 7)
                {
                  tds += '<option value="' + Usuario.idLogin + '">' + Usuario.Nombre + '</option>';

                  idx++
                 }
              });
            if (idx == 0)
            {
              Mensaje("Error", "No fue posible cargar los Usuarios, por favor actuliza la página.");
            } else
            {
              $("#txtContrato_Usuario option").remove();
              $("#txtContrato_Usuario").append(tds);
            } 
             $("#frmCrearContrato .chosen").chosen();
          }, "json");


}