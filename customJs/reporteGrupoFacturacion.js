function reporteGrupoFacturacion()
{
  /*
  
  $("#btnGrupoFacturacionDetalle_Archivos").unbind( "click" );
  $("#btnGrupoFacturacionDetalle_Archivos").live("click", btnGrupoFacturacionDetalle_Archivos_Click);
  
  $("#frmReporteGrupoFacturacionCompleto").on("submit", function(evento)
    {
      evento.preventDefault();
      ReporteGrupoFacturacion_CargarIpalCompleto();
    });
  */
  $( ".lblLinkFacturacion").unbind( "click" );
  $(".lblLinkFacturacion").live("click", lblGrupoFacturacion_Click);

  $("#frmReporteGrupoFacturacion").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteGrupoFacturacion_CargarFacturacion();
    });
  
}
function reporteGrupoFacturacion_CargarFacturacion()
 {
  $(".icoCargando").show();
  $.post("php/facturacion/cargarFacturacion.php", {Desde: $("#txtReporteGrupoFacturacion_Desde").val(), Hasta : $("#txtReporteGrupoFacturacion_Hasta").val()}, 
        function(data)
        {
            $("#tblGrupoFacturacion").dataTable().fnDestroy();
            $("#tblGrupoFacturacion tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";
         
            $.each(data, function(index, value)
              {
                tds += '<tr>';
                tds += '<td></td>';
                tds += '<td><a href="#" class="lblLinkFacturacion" pIdFacturacion="' + value.idFacturacion + "_" + value.Grupo + '">' + value.idFacturacion + "-" + value.Grupo  + '</a></td>';
                tds += '<td>' + value.Fecha + '</td>';
                tds += '<td>' + value.Colaboradora.toUpperCase() + '</td>';
                tds += '<td>' + value.Municipio + '</td>';
                tds += '<td>' + value.Grupo + '</td>';
                tds += '<td>' + value.Puntos + '</td>';
                tds += '<td>' + value.Observaciones + '</td>';
                tds += '<td>' + value.ObservacionesGenerales + '</td>';
                tds += '<td><a href="#" class="lblLinkUsuario" idLogin="' + value.idLogin + '">' + value.Nombre + '</a></td>'
                tds += '</tr>';

              });
            $("#tblGrupoFacturacion tbody").append(tds);
            $("#tblGrupoFacturacion").crearTabla1({lblMenu : "Grupos de Facturación por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }

 function lblGrupoFacturacion_Click(evento)
 {
  evento.preventDefault();
    var pIdFacturacion = $(this).attr("pIdFacturacion");
    $(this).cargarModulo({pagina: "reporteGrupoFacturacionDetalle", titulo : "Detalles de Facturación " + $(this).text(), icono : "icon-eye-open"},
      function()
      {
        $.post("php/facturacion/cargarDetalle.php", {Parametro : pIdFacturacion },
          function(data)
          {
            $('#txtGrupoFacturacionDetalle_Fecha').text(data.Fecha);
            $('#txtGrupoFacturacionDetalle_HoraIni').text(data.HoraIni);
            $('#txtGrupoFacturacionDetalle_HoraFin').text(data.HoraFin);
            $('#txtGrupoFacturacionDetalle_Colaboradora').text(data.Colaboradora);
            $('#txtGrupoFacturacionDetalle_Municipio').text(data.Municipio);
            $('#txtGrupoFacturacionDetalle_SubZona').text(data.SubZona);
            $('#txtGrupoFacturacionDetalle_Supervisor').text(data.Supervisor);
            $('#txtGrupoFacturacionDetalle_SupervisorCedula').text(data.SupervisorCedula);
            $('#txtGrupoFacturacionDetalle_HoraLlegada').text(data.HoraLlegada);
            $('#txtGrupoFacturacionDetalle_HoraSalida').text(data.HoraSalida);
            $('#txtGrupoFacturacionDetalle_Charla').text(data.Charla);
            $('#txtGrupoFacturacionDetalle_CharlaTema').text(data.CharlaTema);
            $('#txtGrupoFacturacionDetalle_FormatoSup').text(data.FormatoSup);
            $('#txtGrupoFacturacionDetalle_Cuadrillas').text(data.Cuadrillas);
            $('#txtGrupoFacturacionDetalle_Inspecciones').text(data.Inspecciones);
            $('#txtGrupoFacturacionDetalle_CumplePlanSup').text(data.CumplePlanSup);
            $('#txtGrupoFacturacionDetalle_Observaciones').text(data.Observaciones);
            $('#txtGrupoFacturacionDetalle_ObservacionesGenerales').text(data.ObservacionesGenerales);


            $("#tblGrupoFacturacionDetalle").dataTable().fnDestroy();
            $("#tblGrupoFacturacionDetalle tbody tr").remove();

            var tds = "";
            $.each(data.Detalle, function(index, value)
              {
                var Estado = "success";
                if (value.Resultado == "No")
                {
                  Estado = "error";
                }
                if (value.Resultado == "NA")
                {
                  Estado = "info";
                }

                tds += '<tr><td></td>';
                tds += '<td>' + value.Consecutivo+ '</td>';
                tds += '<td>' + value.NumCuenta+ '</td>';
                tds += '<td>' + value.NumMedidor+ '</td>';
                tds += '<td>' + value.Sucursal.toUpperCase()+ '</td>';
                tds += '<td>' + value.Direccion+ '</td>';
                tds += '<td>' + value.Barrio+ '</td>';
                tds += '<td>' + value.Ciclo+ '</td>';
                tds += '<td>' + value.Grupo+ '</td>';
                tds += '<td>' + value.Lectura+ '</td>';
                tds += '<td>' + value.Anomalia+ '</td>';
                tds += '<td>' + value.EntregaFactura+ '</td>';
                tds += '<td>' + value.EntregaOportuna+ '</td>';
                tds += '<td>' + value.Cumple+ '</td>';
                tds += '<td>' + value.Observaciones+ '</td>';
                tds += '</tr>';
              });
            $("#tblGrupoFacturacionDetalle tbody").append(tds);
            $("#tblGrupoFacturacionDetalle").crearTabla1({lblMenu : "Resultados por página"});

          }, 'json').always(function() 
        {
          //Cuando Finaliza
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
      })
 }
 
 /*
 function reporteGrupoFacturacion_CargarFacturacion()
 {
  $.post("php/ipal/cargarIpalCompleto.php", {Desde: $("#txtReporteGrupoFacturacionCompleto_Desde").val(), Hasta : $("#txtReporteGrupoFacturacionCompleto_Hasta").val()}, 
        function(data)
        {
            $("#tblIpalesCompleto").dataTable().fnDestroy();
            $("#tblIpalesCompleto tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";

            $.each(data, function(index, value)
              {
                var Estado = "success";
                if (value.Resultado != "Cumple")
                {
                  Estado = "error";
                } 
                tds += '<tr>';
                tds += '<td></td>';
                tds += '<td><a href="#" class="lblLinkIpal" idIpal="' + value.idIpal + '">' + value.idIpal + '</a></td>';
                tds += '<td>' + value.Fecha + '</td>';
                tds += '<td>' + value.Empresa + '</td>';
                tds += '<td>' + value.Direccion + '</td>';
                tds += '<td>' + value.Celular + '</td>';
                tds += '<td>' + value.VehiculoTipo + '</td>';
                tds += '<td>' + value.VehiculoPlaca + '</td>';
                tds += '<td>' + value.GruaPlaca + '</td>';
                tds += '<td>' + value.CanastaPlaca + '</td>';
                tds += '<td>' + value.MotoPlaca + '</td>';
                tds += '<td>' + value.NoContrato + '</td>';
                tds += '<td>' + value.Trabajo + '</td>';
                tds += '<td>' + value.Proceso + '</td>';
                tds += '<td><a href="#" class="lblLinkUsuario" idLogin="' + value.idLogin + '">' + value.Usuario + '</a></td>'
                tds += '<td>' + value.Observaciones + '</td>';
                tds += '<td>' + value.Zona + '</td>';
                tds += '<td>' + value.Recibio + '</td>';
                tds += '<td>' + value.CargoEmpresa + '</td>';
                tds += '<td class="alert alert-block alert-'+ Estado +'">' + value.Resultado + '</td>';
                tds += '</tr>';

              });
            $("#tblIpalesCompleto tbody").append(tds);
            $("#tblIpalesCompleto").crearTabla1({lblMenu : "Ipales por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }
*/
 
/*
function btnGrupoFacturacionDetalle_Archivos_Click()
{
  var pPrefijo = $(this).attr("Prefijo");
  var pConsecutivo = $(this).attr("Consecutivo");
  console.log(pPrefijo);
  cargarArchivos("Ipal: " + pConsecutivo, "Ipales\/" + pPrefijo); 
}*/