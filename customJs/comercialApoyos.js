var intentoGuardar = 0;
function ComercialApoyos()
{

    $(".toggle").toggleButtons({
              width: 200,
              label: {
                  enabled: "Si",
                  disabled: "No"
              }
          });

  //verContratos();
  $("#txtComercialApoyos_Capa").on("change", function()
    {
      $("#txtComercialApoyos_Fase").val($("#txtComercialApoyos_Capa").val());
    });

  $("#btnComercialApoyos_TomarCoordenadas").on("click", function(event) 
  {
    event.preventDefault();
    Mensaje("Ubicar", "Se han tomado coordenadas de: Autonorte");
    $("#txtComercialApoyos_Coordenadas").val("4.688587, -74.056584");
    
  });

  $("#grpComercialApoyosFechaInicio input").val("2014-08-08");
  $("#txtComercialApoyos_Ejecutor").val("admin");

  $("#frmComercialApoyos .chosen").chosen();
  $("#grpComercialApoyosFechaInicio input, #grpComercialApoyosFechaTerminacion input").datepicker("destroy");
    $("#grpComercialApoyosFechaInicio input, #grpComercialApoyosFechaTerminacion input").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });

          $("#grpComercialApoyosFechaInicio .add-on").on("click", function()
          {
            $("#grpComercialApoyosFechaInicio input").datepicker("show");
          });
          $("#grpComercialApoyosFechaTerminacion .add-on").on("click", function()
          {
            $("#grpComercialApoyosFechaTerminacion input").datepicker("show");
          });

          $("#frmComercialApoyos").on("submit", function(evento)
            {
              evento.preventDefault();
              var tds = ", Vuelve a tomar las coordenadas";
              if (intentoGuardar < 3)
              {

                if ($("#txtComercialApoyos_TipoOt").val() != "Madera") 
                {
                  tds += ", Confirma el Material";
                }
                if ($("#txtComercialApoyos_TipoOt2").val() != "2") 
                {
                  tds += ", Confirma la altura del Poste";
                }
                if ($("#txtComercialApoyos_Municipio2").val() != "750") 
                {
                  tds += ", Confirma la Carga de Rotura";
                }
                if ($("#txtComercialApoyos_Fase").val() != "2") 
                {
                  tds += ", Confirma el punto Fisico";
                }
                if ($("#txtComercialApoyos_Ubicacion").val() != "Rural") 
                {
                  tds += ", Confirma la Ubicación";
                }
                if ($("#txtAlumbrado_Municipio").val() != "Girardot") 
                {
                  tds += ", Confirma el Municipio";
                }

                intentoGuardar++;
                Mensaje("Asegurate", "Por favor " + tds);    
              } else
              {
                intentoGuardar == 0;
                Mensaje("Ok", "El Formulario ha sido Guardado.");    
                $("#frmComercial")[0].reset();
                
              }
              /*
              $.post("php/ComercialApoyos.php",
              {
                nombre : $('#txtComercialApoyos_Nombre').val(),
                descripcion : $('#txtComercialApoyos_Descripcion').val(),
                fechaInicio : $('#txtComercialApoyos_FechaInicio').val(),
                fechaTerminacion : $('#txtComercialApoyos_FechaTerminacion').val(),
                valor : $('#txtComercialApoyos_Valor').val(),
                objeto : $('#txtComercialApoyos_Objeto').val(),
                responsabilidades : $('#txtComercialApoyos_Responsabilidades').val(),
                contratante : $('#txtComercialApoyos_Contratante').val(),
                contratista : $('#txtComercialApoyos_Contratista').val()
              }, function(data, textStatus, xhr)
              {
                if (data == 1)
                {
                  Mensaje("Ok", "El ComercialApoyos ha sido almacenado.");    
                } else
                {
                  Mensaje("Error", data);    
                }
              }).always(function() 
              {
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No fue posible almacenar el ComercialApoyos, por favor intenta nuevamente.");
              });
*/          

              
            });
}