$(document).ready("facturacion");

function facturacion()
{
  $("#grpFacturacionFechaInicio input").datepicker(
    {
      changeMonth: true,
      changeYear: true
    });

   $("#grpFacturacionFechaInicio .add-on").on("click", function()
    {
      $("#grpFacturacionFechaInicio input").datepicker("show");
    });

   $('#tabsFacturacion .timepicker-24').timepicker({
      minuteStep: 1,
      showSeconds: false,
      showMeridian: false,
      disableMousewheel : true,
      disableFocus: true
  });

  $("#txtFacturacion_FechaInicio").on("change", consultarFacturacion);

   /*
   var f = new Date();
  $("#txtFacturacion_FechaInicio").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2));
  $("#txtFacturacion_HoraIni").val( CompletarConCero(f.getHours(), 2) + ":" + CompletarConCero(f.getMinutes(), 2));
  */

   $("#tabsFacturacion .factRadio").buttonset();

   $("#tabsFacturacion .chosen").chosen();
  
   $("#frmFacturacion").on("submit", function(evento)
    {
      evento.preventDefault();
      var pPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "");
      pPrefijo = CompletarConCero(Usuario.id, 3) + pPrefijo;

      $.post("php/facturacion/crearFacturacion.php", 
        {
          idLogin : Usuario.id,
          Prefijo : pPrefijo,
          Fecha : $('#txtFacturacion_FechaInicio').val(),
          HoraIni : $('#txtFacturacion_HoraIni').val(),
          HoraFin : $('#txtFacturacion_HoraFin').val(),
          Colaboradora : $('#txtFacturacion_Colaboradora').val(),
          Municipio : $('#txtFacturacion_Municipio').val(),
          SubZona : $('#txtFacturacion_Subzona').val(),
          Supervisor : $('#txtFacturacion_Supervisor').val(),
          SupervisorCedula : $('#txtFacturacion_SupervisorCedula').val(),
          HoraLlegada : $('#txtFacturacion_HoraLlegada').val(),
          HoraSalida : $('#txtFacturacion_HoraSalida').val(),
          Charla : $('input:radio[name=radFacturacion]:checked').val(),
          CharlaTema : $('#txtFacturacion_CharlaTema').val(),
          ObservacionesGenerales : $('#txtFacturacion_ObservacionesGenerales').val()
        }, function(data)
        {
          Mensaje("Ok", "La Ejecución ha sido almacenada.");
        }).always(function() 
        {
          //Cuando Finaliza
        }).fail(function() {
          Mensaje("Error", "No fue posible almacenar La Ejecución, por favor intenta nuevamente.");
        })
    });

    $("#frmFacturacionAuditoria").on("submit", function(evento)
      {
        evento.preventDefault();
        var pPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "");
        pPrefijo = CompletarConCero(Usuario.id, 3) + pPrefijo;

        $.post("php/facturacion/crearAuditoria.php",
          {
            Prefijo : pPrefijo,
            FormatoSup : $('input:radio[name=radFacturacionFormSup]:checked').val(),
            CuadrillasACargo : $('#txtFacturacionAuditoria_Cuadrillas').val(),
            InspeccionesRealizadas : $('#txtFacturacionAuditoria_Inspecciones').val(),
            CumplePlanSup : $('input:radio[name=radFacturacionPlanSup]:checked').val(),
            Observaciones : $('#txtFacturacionAuditoria_Observaciones').val()
          }, function(data)
          {
            if (data == 1) //No existe la Información General
            {
              Mensaje("Error", "No se almacenó la Auditoría, Hay que guardar primero la Información General.");
            }
            else if (data == 2) //Se guardo Bien
            {
              Mensaje("Ok", "La Auditoría fue Guardada.");   
            } else
            {
              Mensaje("Error", "No fue posible almacenar la Auditoría, por favor revisa los datos e intenta nuevamente.");
            }
          }).fail(function()
          {
            Mensaje("Error", "No se logró establecer comunicación con el Servidor.");
          });
        });
  $("#frmFacturacionTerreno").on("submit", function(evento)
    {
      evento.preventDefault();
      var pPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "");
      pPrefijo = CompletarConCero(Usuario.id, 3) + pPrefijo;

      $.post("php/facturacion/crearFacturacionTerreno.php", 
        {
          idLogin : Usuario.id,
          Prefijo : pPrefijo,
          PrefijoIns : $("#txtPrefijo").val(),
          NumCuenta : $('#txtFacturacionTerreno_NumCuenta').val(),
          NumMedidor : $('#txtFacturacionTerreno_NumMedidor').val(),
          Sucursal : $('#txtFacturacionTerreno_Sucursal').val(),
          Direccion : $('#txtFacturacionTerreno_Direccion').val(),
          Barrio : $('#txtFacturacionTerreno_Barrio').val(),
          Ciclo : $('#txtFacturacionTerreno_Ciclo').val(),
          Grupo : $('#txtFacturacionTerreno_Grupo').val(),
          Lectura : $('#txtFacturacionTerreno_Lectura').val(),
          Anomalia : $('input:radio[name=radFacturacionAnomalia]:checked').val(),
          EntregaFact : $('input:radio[name=radFacturacionEntregaFact]:checked').val(),
          EntregaOpor : $('input:radio[name=radFacturacionEntregaOpor]:checked').val(),
          Cumple : $('input:radio[name=radFacturacionCumple]:checked').val(),
          Observaciones : $('#txtFacturacionTerreno_Observaciones').val()
        }, function(data)
        {
          if (data == 1) //No existe la Información General
            {
              Mensaje("Error", "No se almacenó la Inspección, Hay que guardar primero la Información General.");
            }
            else if (data == 2) //Se guardo Bien
            {
              Mensaje("Ok", "La Inspección fue Guardada.");   
              $("#modulo_facturacion").remove();
            } else
            {
              Mensaje("Error", "No fue posible almacenar la Inspección, por favor revisa los datos e intenta nuevamente.");
            }
        }).always(function() 
        {
          //Cuando Finaliza
        }).fail(function() {
          Mensaje("Error", "No fue posible almacenar La Ejecución, por favor intenta nuevamente.");
        })
    });
}
function consultarFacturacion()
{
  var pPrefijo = $('#txtFacturacion_FechaInicio').val().replace(/-/g, "");
      pPrefijo = CompletarConCero(Usuario.id, 3) + pPrefijo;

  $.post("php/facturacion/buscarFacturacion.php", {Prefijo : pPrefijo}, 
    function(data)
    {
      if (data != 0)
      {
        $("#txtFacturacion_HoraIni").val(data.HoraIni);
        $("#txtFacturacion_HoraFin").val(data.HoraFin);
        $("#txtFacturacion_Colaboradora").val(data.Colaboradora);
        $("#txtFacturacion_Municipio").val(data.Municipio);
        $("#txtFacturacion_Subzona").val(data.SubZona);
        $("#txtFacturacion_Supervisor").val(data.Supervisor);
        $("#txtFacturacion_SupervisorCedula").val(data.SupervisorCedula);
        $("#txtFacturacion_HoraLlegada").val(data.HoraLlegada);
        $("#txtFacturacion_HoraSalida").val(data.HoraSalida);
        if (data.Charla == "Si")
        {
          $("input:radio[name=radFacturacion]")[0].checked = true;
        }
        if (data.Charla == "No")
        {
          $("input:radio[name=radFacturacion]")[1].checked = true;
        }
        $("#txtFacturacion_Charla").buttonset("refresh");
        $("#txtFacturacion_CharlaTema").val(data.CharlaTema);
        $("#ObservacionesGenerales").val(data.ObservacionesGenerales);
      } else
      {
        Mensaje("Hey", "Recuerda guardar la Información general antes de Iniciar la Inspección.");
      }
    } 
    , "json").fail(function()
    {
      Mensaje("Error", "No se logró establecer comunicación con el Servidor.");
    });
}