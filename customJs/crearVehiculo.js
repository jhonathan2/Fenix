function crearVehiculo()
{
        $.post("php/cargarEmpresas.php", {usuario : Usuario.id}, function(data)
        {
          var idx = 0;
          var tds = "";
          $.each(data, function(index, empresa)
            {
              tds += '<option value="' + empresa.idEmpresa + '">' + empresa.Nombre + '</option>';

              idx++
            });
          if (idx == 0)
          {
            Mensaje("Error", "No fue posible cargar las Empresas, por favor actuliza la página.");
          } else
          {
            $("#txtCrearVehiculo_Empresa option").remove();
            $("#txtCrearVehiculo_Empresa").append(tds);
          }
        }, "json").always(function() 
          {
            $("#frmCrearVehiculo .chosen").chosen();
          }).fail(function() {
            Mensaje("Error", "No fue posible cargar las Empresas, por favor actuliza la página.");
          }); 
      
  
  
  $("#frmCrearVehiculo").on("submit", function(evento)
    {
      evento.preventDefault();
      if ($("#txtCrearVehiculo_Perfil").val() == "")
      {
        Mensaje("Error", "Por favor selecciona el Perfil");
        $("#txtCrearVehiculo_Perfil").focus();
      } else
      {
        if ($("#txtCrearVehiculo_Empresa").val() == "")
        {
          Mensaje("Error", "Por favor selecciona la Empresa");
          $("#txtCrearVehiculo_Empresa").focus();
        } else
        {
            $.post("php/crearUsuario.php",
            {
              usuario : $('#txtCrearVehiculo_Nombre').val(),
              correo : $("#txtCrearVehiculo_Correo").val() + " Km",
              perfil : 8,
              cargo : "Vehículo",
              nombre : $('#txtCrearVehiculo_Marca').val() + " " + $('#txtCrearVehiculo_Modelo').val(),
              clave : "clavevehiculo",
              clave2 : "clavevehiculo",
              empresa : $('#txtCrearVehiculo_Empresa').val(),
              imagen : $("#frmCrearVehiculo .fileupload-preview img").attr("src")
            }, function(data, textStatus, xhr)
            {
              if (data == 1)
              {
                Mensaje("Ok", "El Vehiculo ha sido almacenado.");    
                $("#frmCrearVehiculo")[0].reset();
              } else
              {
                Mensaje("Error", data);    
              }
            }).always(function() 
            {
              //Cuando Finaliza
            }).fail(function() {
              Mensaje("Error", "No fue posible almacenar el Vehiculo, por favor intenta nuevamente.");
            });
        }
      }
    });
}