function ans()
{
  fechasRango($("#txtANS_Desde"), $("#txtANS_Hasta"));

  var f = new Date();
        f = f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2);
        $("#txtANS_Desde").val(f);
        $("#txtANS_Hasta").val(f);

  $("#frmANS").on("submit", frmANS_Submit);
}

function frmANS_Submit(evento)
{
  evento.preventDefault();
  $.post('php/cargarANS.php', {idLogin : Usuario.id, Desde : $("#txtANS_Desde").val(), Hasta : $("#txtANS_Hasta").val()}, 
    function(data, textStatus, xhr) 
            {
              var totalRealizadas = parseInt(data.comercialRealizadas) + parseInt(data.alumbradoRealizadas) ;
                var porcentajeRealizadas = ((totalRealizadas * 100)/data.total).toFixed(2)  + "%";

                $("#txtANS").slideDown();
                if (totalRealizadas == 0)
                {
                  $("#txtANS .metro-overview").slideUp();
                  $("#txtANS h4").text("No hay datos para ese rango de Fechas.");

                } else
                {
                  $("#txtANS .metro-overview").slideDown();
                  $("#txtANS h4").text("");
                }
                
                

                $("#txtANS .numbers").text("0 de 0");
                $("#txtANS .percent").text("0%");
                $("#txtANS .bar").css("width", "0%");
                
                $("#txtANS_Total .numbers").text(totalRealizadas + " de " + data.total);
                $("#txtANS_Total .percent").text(porcentajeRealizadas);
                $("#txtANS_Total .bar").css("width", porcentajeRealizadas);
                

                if (data.alumbradoRealizadas > 0)
                {
                  totalRealizadas = data.alumbradoRealizadas + " de " + data.alumbrado;
                  porcentajeRealizadas = ((data.alumbradoRealizadas * 100)/data.alumbrado).toFixed(2) + "%";
                  $("#txtANS_Alumbrado .numbers").text(totalRealizadas);
                  $("#txtANS_Alumbrado .percent").text(porcentajeRealizadas);
                  $("#txtANS_Alumbrado .bar").css("width", porcentajeRealizadas);
                } 
                if (parseInt(data.comercialRealizadas) > 0)
                {
                  totalRealizadas = data.comercialRealizadas + " de " + data.comercial;
                  porcentajeRealizadas = ((data.comercialRealizadas * 100)/data.comercial).toFixed(2) + "%";
                  $("#txtANS_Comercial .numbers").text(totalRealizadas);
                  $("#txtANS_Comercial .percent").text(porcentajeRealizadas);
                  $("#txtANS_Comercial .bar").css("width", porcentajeRealizadas);
                } 
                if (data.tecnica > 0)
                {
                  $("#txtANS_Tecnica").text("+0 de " + data.tecnica + " 0%");  
                } 
                
                $("#txtANS_Ipales .numbers").text(data.ipales);
                $("#txtANS_ComercialSitio .numbers").text(data.comercialSitioRealizadas);

                var d1 = [];
                var d2 = [];
                var datos2 = new Array;
                var idx = 0;
                var idx2 = 0;
                var tds = "";
                var Procentaje = 0;
                var PromProg = 0;
                var PromReal = 0;
                var PromProm = 0;
                $("#tblANS tbody tr").remove();
                $.each(data.DatosANS, function(index, value)
                  {
                    d1.push([index, parseInt(value.Programadas)]);
                    d2.push([index, parseInt(value.Realizadas)]);
                    datos2[idx] = {"data": [d1[index]], "label": value.fechaCargue + " Cargadas"};
                    idx++;
                     datos2[idx] = {"data": [d2[index]], "label": value.fechaCargue + " Realizadas"};
                    idx++;


                    Porcentaje = (parseInt(value.Realizadas) * 100)/(parseInt(value.Programadas));
                    PromProm += Porcentaje;
                    Porcentaje = (Porcentaje).toFixed(2) + "%";
                    PromProg += parseInt(value.Programadas);
                    PromReal += parseInt(value.Realizadas);

                    tds += "<tr>";
                      tds += "<td>" + value.fechaCargue + "</td>"
                      tds += "<td>" + value.Programadas + "</td>"
                      tds += "<td>" + value.Realizadas  + "</td>"
                      tds += "<td>" + Porcentaje + "</td>"
                    tds += "</tr>";
                    idx2 = index;
                  }
                );
                idx2++;
                tds += "<tr>";
                      tds += "<td><strong>Promedio</strong></td>"
                      tds += "<td><strong>" +  Math.floor(PromProg/idx2) + "</strong></td>"
                      tds += "<td><strong>" +  Math.floor(PromReal/idx2)  + "</strong></td>"
                      tds += "<td><strong>" + (PromProm/idx2).toFixed(2) + "%</strong></td>"
                    tds += "</tr>";

                $("#tblANS tbody").append(tds);
                  cargarGraficaBarras(datos2, "Preguntar por el título", "ANS_Grafica");
                  cargarConvenciones(datos2, "ANS_GraficaConvenciones", "ANS_Grafica");
                

            }, 'json').always(function() 
              {
                $(".knob").knob();
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No hay conexión con el servidor");
                $("#txtANS").slideUp();
                $("#txtErrorANS").slideDown();
              });
}
