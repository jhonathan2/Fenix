function Alumbrado()
{
  btnAlumbrado_Reset_click();
  $("#btnAlumbrado_BorrarDatosUsuario").on("click", btnAlumbrado_BorrarDatosUsuario_Click);
  $("#btnAlumbrado_Reset").on("click", btnAlumbrado_Reset_click);

  $("#txtAlumbrado_Capa").on("change", function()
    {
      //$("#txtAlumbrado_Fase").val($("#txtAlumbrado_Capa").val());
    });

  $("#btnAlumbrado_TomarCoordenadas").on("click", function(event) 
  {
    event.preventDefault();
    ObtenerCoordenadas("txtAlumbrado_CoordenadasTrafo");
  });
  $("#btnAlumbrado_TomarCoordenadasIluminaria").on("click", function(event) 
  {
    event.preventDefault();
    ObtenerCoordenadas("txtAlumbrado_CoordenadasIluminaria");
  });
  $("#btnAlumbrado_TomarCoordenadasRele").on("click", function(event) 
  {
    event.preventDefault();
    ObtenerCoordenadas("txtAlumbrado_CoordenadasRele");
  });

  btnAlumbrado_BorrarDatosUsuario_Click();

  /*
  $("#frmAlumbrado_DatosUsuario .chosen").chosen();
  $("#frmAlumbrado .chosen").chosen();*/
  $("#grpAlumbradoFechaInicio input, #grpAlumbradoFechaTerminacion input").datepicker("destroy");
    $("#grpAlumbradoFechaInicio input, #grpAlumbradoFechaTerminacion input").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });

          $("#grpAlumbradoFechaInicio .add-on").on("click", function()
          {
            $("#grpAlumbradoFechaInicio input").datepicker("show");
          });
          $("#grpAlumbradoFechaTerminacion .add-on").on("click", function()
          {
            $("#grpAlumbradoFechaTerminacion input").datepicker("show");
          });

          $("#frmAlumbrado").on("submit", function(evento)
            {
              evento.preventDefault();
              $.post("php/alumbrado/crearAlumbrado.php",
              {
                idLogin: Usuario.id,
                Prefijo : $('#txtPrefijo').val(),
                 OT  : $('#txtAlumbrado_Nombre').val(),
                 Tipo  : $('#txtAlumbrado_TipoOt').val(),
                 FechaIngreso  : $('#txtAlumbrado_FechaInicio').val(),
                 Ejecutor   : $('#txtAlumbrado_Ejecutor').val(),
                 Municipio  : $('#txtAlumbrado_Municipio').val(),
                 CD  : $('#txtAlumbrado_CD').val(),
                 CoorTransformador  : $('#txtAlumbrado_CoordenadasTrafo').val(),
                 CapaTrafo  : $('#txtAlumbrado_Capa').val(),
                 Fase  : $('#txtAlumbrado_Fase').val(),
                 Ubicacion  : $('#txtAlumbrado_Ubicacion').val(),
                 Barrio  : $('#txtAlumbrado_Barrio').val(),
                 Direccion  : $('#txtAlumbrado_Direccion').val(),
                 Red  : $('#txtAlumbrado_Red').val(),
                 CodPoste  : $('#txtAlumbrado_CodPoste').val(),
                 TipoControl  : $('#txtAlumbrado_TipoControl').val(),
                 CantLuminarias  : $('#txtAlumbrado_CantIlu').val(),
                 TipoLuminarias  : $('#txtAlumbrado_TipoIluminarias').val(),
                 TipoIluminaria  : $('#txtAlumbrado_TipoIluminaria').val(),
                 Propietario  : $('#txtAlumbrado_Propietario').val(),
                 Via  : $('#txtAlumbrado_Via').val(),
                 TipoControl2  : $('#txtAlumbrado_Rele').val(),
                 TipoRed  : $('#txtAlumbrado_TipoRed').val(),
                 Materiales  : $('#txtAlumbrado_Materiales').val(),
                 Estado  : $('#txtAlumbrado_Estado').val(),
                 TiempoOperacion  : $('#txtAlumbrado_TiempoOp').val(),
                 Red2  : $('#txtAlumbrado_Red2').val(),
                 Observaciones  : $('#txtAlumbrado_Observaciones').val(),
                 Estado2  : $('#txtAlumbrado_EstadoOp').val(),
                 DireccionLuminaria : $("#txtAlumbrado_DireccionLuminaria").val(),
                 UsoRed : $("#txtAlumbrado_UsoRed").val(),
                 Luminaria : $("#txtAlumbrado_Luminaria").val(),
                 CoordenadasIluminaria : $("#txtAlumbrado_CoordenadasIluminaria").val(),
                 CoordenadasRele : $("#txtAlumbrado_CoordenadasRele").val()
              }, function(data, textStatus, xhr)
              {
                if (data == 1)
                {
                  Mensaje("Ok", "El Alumbrado ha sido almacenado.");    
                  $("#frmAlumbrado")[0].reset();
                  $("#modulo_alumbrado").hide();

                  btnAlumbrado_Reset_click();
                } else
                {
                  Mensaje("Error", data);    
                }
              }).always(function() 
              {
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No fue posible almacenar el Alumbrado, por favor intenta nuevamente.");
              });
              
            });
}
function btnAlumbrado_Reset_click()
{
   var f = new Date();
   pPrefijo = CompletarConCero(Usuario.id, 3) + f.getFullYear() + CompletarConCero(f.getMonth() +1, 2) + CompletarConCero(f.getDate(), 2) + CompletarConCero(f.getHours(), 2) + CompletarConCero(f.getMinutes(), 2) + CompletarConCero(f.getSeconds(), 2);
  $("#alumbradoSubir").attr("src" ,"subir/index.html?Prefijo=Alumbrado/" + pPrefijo);
  $("#txtAlumbrado_Prefijo").val(pPrefijo);
 
}
function btnAlumbrado_BorrarDatosUsuario_Click(evento)
{
  if (evento)
  {
    evento.preventDefault();
  }
  $("#frmAlumbrado_DatosUsuario")[0].reset();
  var f = new Date();
  pFecha = f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2);

  $("#grpAlumbradoFechaInicio input").val(pFecha);
  $("#txtAlumbrado_Ejecutor").val(Usuario.nombre); 
}