function verContratos()
{       
   // initiate layout and plugins
   //$("#frmVerContratos .chosen").chosen();
   $('.tooltips').tooltip();
   $(".eliminarContrato").live("click", eliminarContrato);

   $("#frmVerContratos .toggle").toggleButtons({
              width: 200,
              label: {
                  enabled: "Si",
                  disabled: "No"
              }
          });

   $(".btnVerContrato_Archivos").live("click", btnVerContrato_Archivos_Click);
   $(".btnVerContrato_Detalles").live("click", btnVerContrato_Detalles_Click);
   $(".btnVerContrato_Empresas").live("click", btnVerContrato_Empresas_Click);
   $(".btnEmpresas_Contratos_Borrar").live("click", btnEmpresas_Contratos_Borrar_Click);
   

   $("#txtVerContratos_BuscarPor").chosen().change(txtVerContratos_BuscarPor_Click);

   $("#btnVerContratos_Buscar").on("click", btnVerContratos_Buscar_Click);
}

function btnVerContrato_Archivos_Click()
{
    var Contrato = $(this).parent("div").parent("div").find("h4");
    var idContrato = $(this).attr("idContrato");

    cargarArchivos($(Contrato).text(), "OT\/" + idContrato);
    /*
    $(this).cargarModulo({pagina : "verContratos_Archivos", titulo : "Archivos de " + $(Contrato).text(), icono : "icon-copy"}, function()
      {
        iniciarFinder(idContrato);      
      });
    */
}
function cerrarPorlet_()
 {
      $("#txtVerContratos_ContenedorResultados .icon-chevron-down").on("click", ocultarPortlet);

     jQuery('#txtVerContratos_ContenedorResultados .icon-remove').on ("click", function () {
        jQuery(this).parent(".tools").parent(".widget-title").parent(".widget").remove();
    });
}
function ocultarPortlet()
{
    var obj = $(this).parent(".tools").parent(".widget-title").parent(".widget").find(".widget-body");
    if (jQuery(this).hasClass("icon-chevron-down")) {
        jQuery(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
        obj.slideUp(200);
    } else {
        jQuery(this).removeClass("icon-chevron-up").addClass("icon-chevron-down");
        obj.slideDown(200);
    }
}
function txtVerContratos_BuscarPor_Click(evento, Objeto)
{
$.each(Objeto, function(index, value)
  {
    var str = value + ", ";
    if (index == "selected")
    {
      $("#txtVerContratos_Parametros").val($("#txtVerContratos_Parametros").val() + str)
    } else
    {
      $("#txtVerContratos_Parametros").val($("#txtVerContratos_Parametros").val().replace(str, ""));
    }
  });
}

function btnVerContratos_Buscar_Click()
 {
    $(".icoCargando").show();
    var pParametros = $("#txtVerContratos_Parametros").val();
    if (pParametros != "")
    {
        $.post("php/buscarContratos.php", {parametros: pParametros, criterio : $("#txtVerContratos_Criterio").val(), ejecutados : checkValor("txtVerContratos_Ejecutadas")}, function(data)
        {
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            //$("#txtVerContratos_ContenedorResultados div").remove();
            //$('#' + idTabla).dataTable().clear();
            $("#tblOts").dataTable().fnDestroy();
            $("#tblOts tbody tr").remove();

            var tds = "";
            $.each(data, function(index, value)
              {
                tds += '<tr id="trContrato_' + value.idContrato + '">';
                tds +=  '<td><div class="btn-group">';
                tds +=    '<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"> </i><span class="caret"></span></button>';
                tds +=    '<ul class="dropdown-menu">';
                tds +=      '<li class="btn-large btnVerContrato_Archivos" idContrato="' + value.idContrato + '"><a href="#"><i class="icon-folder-open icon-white"></i> Archivos</a></li>';
                tds +=      '<li class="btn-large btnVerContrato_Detalles"><a href="#"><i class="icon-search icon-white"></i> Detalles</a></li>';
                tds +=      '<li class="btn-large btnVerContrato_Empresas" Coordenadas="' + value.Coordenadas + '"><a href="#"><i class="icon-map-marker icon-white"></i> Como Llegar</a></li>';
                tds +=    '</ul>';
                tds +=  '</div></td>';
                tds += '<td>' + value.Nombre + '</td>';
                tds += '<td>' + value.Descripcion + '</td>';
                tds += '<td>' + value.Responsabilidades + '</td>';
                tds += '<td>' + value.Objeto + '</td>';
                tds += '<td>' + value.Contratante + '</td>';
                tds += '<td>' + value.Contratista + '</td>';
                if (Usuario.id == 63 || Usuario.id < 70) 
                {
                  tds += '<td><a class="eliminarContrato" Nombre="' + value.Nombre + '" idContrato="'+ value.idContrato +'"><i class="icon-trash"></i> Eliminar</a></td>';
                } else
                {
                  tds += "<td><td>";
                }
                tds += '</tr>';

                /*
                  tds += '<div class="span4">';
                  tds += '<div class="widget">';
                  tds += '   <div class="widget-title">';
                  tds += '       <h4 idContrato="' + value.idContrato + '"><i class="icon-reorder"></i>' + value.Nombre + '</h4>';
                  tds += '       <span class="tools">';
                  tds += '          <a class="icon-chevron-down" href="javascript:;"></a>';
                  tds += '          <a class="icon-remove" href="javascript:;"></a>';
                  tds += '       </span>';
                  tds += '   </div>';
                  tds += '   <div class="widget-body">';
                  tds += '       <div>';
                  tds += '           <b>Tarea:</b>' + value.Descripcion + '<br />';
                  tds += '           <b>Descripción:</b>' + value.Objeto + '<br />';
                  tds += '           <b>Cliente:</b>' + value.Contratante + '<br />';
                  tds += '           <b>Dirección:</b>' + value.Contratista + '<br />';
                  tds += '           <b>Tiempo para Ejecución:</b>0 días<br />';
                  tds += '       </div>';
                  tds += '   </div>';
                  tds += '   <div class="form-actions">';
                  tds += '     <button  class="btn btn-warning btnVerContrato_Archivos" idContrato="' + value.idContrato + '"><i class="icon-folder-open icon-white"></i> Archivos</button>';
                  tds += '     <button class="btn btn-info btnVerContrato_Detalles"><i class="icon-search icon-white"></i> Detalles</button>';
                  tds += '     <button class="btn btn-primary btnVerContrato_Empresas"><i class="icon-map-marker icon-white"></i> Como Llegar</button>';
                  tds += '   </div>';
                  tds += '</div>';
                  tds += '</div>';
                  */
              });
            $("#tblOts tbody").append(tds);
            $("#tblOts").crearTabla1({lblMenu : "Tareas por página"});

            /*
            $("#txtVerContratos_ContenedorResultados").append(tds);
            PortletDraggable.init();                    
            cerrarPorlet_();
            */
          }

        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue conectar con el Servidor");
        });
    } else
    {
      Mensaje("Hey", "Tienes que seleccionar por lo menos un parámetro");
    }
      
}
function btnVerContrato_Detalles_Click()
{
  var Contrato = $(this).parent("div").parent("div").find("h4");
  var pIdContrato = $(Contrato).attr("idContrato");
   $(this).cargarModulo({pagina : "editarContrato", titulo : "Editar " + $(Contrato).text() ,icono : "icon-edit"}, function()
    {
      cargarDatosContrato(pIdContrato);
    });
}
/*
function iniciarFinder(idContrato)
{
  if($('#verContratos_Archivos').elfinder('instance'))
    { $('#verContratos_Archivos').elfinder('destroy');}
              
    var elf = $('#verContratos_Archivos').elfinder({
          url : 'Archivos/php/connector.php?Contrato=' + idContrato,
          lang: 'es',  
          handlers:
          {
            upload : function(event) 
                { 
                  
                },
            open: function(event)
                { 
                  
                },
            rm : function(event)
                {
                  
                }
          }           
    }).elfinder('instance');
}*/
function btnVerContrato_Empresas_Click()
{
  var pCoordenadas = $(this).attr("Coordenadas");
  if (pCoordenadas != "")
  { 
      popupWin = window.open("https://www.google.com/maps/dir/" + $("#txtCoordenadas").val() + "/" + pCoordenadas , 'open_window');    
  } else
  {
    Mensaje("Hey", "Esta orden no tiene Coordenadas");
  }
  
  /*
  var Contrato = $(this).parent("div").parent("div").find("h4");
  var pIdContrato = $(Contrato).attr("idContrato");

  $(this).cargarModulo({pagina : "verEmpresas_Contratos", titulo : "Empresas de " + $(Contrato).text(), icono : "icon-cogs"}, function()
      {
        $("#tblEmpresas_Contratos tbody td").remove();
        $("#tblEmpresas_Contratos").attr("idContrato", pIdContrato);
        $.post("php/cargarEmpresas_Contratos.php", {usuario : Usuario.id, idContrato : pIdContrato}, 
          function(Empresas)
          {
            if (Empresas.EmpresasAsociadas == 0)
            {
              Mensaje("Alerta", "No hay Empresas Asociadas");
            } else
            {
              var tds = "";

              $.each(Empresas.EmpresasAsociadas, function (index, Empresa) 
              {
                tds += '<tr class="odd gradeX">';
                    tds += '<td>' + Empresa.idEmpresa + '</td>';
                    tds += '<td>' + Empresa.Nombre + '</td>';
                    tds += '<td>' + Empresa.Identificacion + '</td>';
                    tds += '<td>' + Empresa.Direccion + '</td>';
                    tds += '<td>' + Empresa.Telefono + '</td>';
                    tds += '<td>' + Empresa.Correo + '</td>';
                    tds += '<td class="hidden-phone">';
                    tds += '<button class="btn btn-danger btnEmpresas_Contratos_Borrar"><i class="icon-trash icon-white"></i></button>';
                tds += '</td></tr>';

              });

              $("#tblEmpresas_Contratos tbody").append(tds);
              $("#tblEmpresas_Contratos").crearTabla1({lblMenu : "Empresas por página"});
            }
            $("#txtEmpresas_Contratos_AgregarEmpresa").attr("data-source", Empresas.Empresas);
          },"json").fail(function()
          {
            Mensaje("Error", "No hay conexión con el servidor");
          });
      });*/
}
function btnEmpresas_Contratos_Borrar_Click()
{
  var objFila = $(this).parent("td").parent("tr").find("td");
  var idContrato = $(this).parent("td").parent("tr").parent("tbody").parent("table").attr("idContrato");
  //$(objFila).parent("tr").remove();
  var table = $('#tblEmpresas_Contratos').dataTable();
  //console.log(table);
  console.log($(this).parent('td').parent("tr"));
    //table.row( $(this).parents('tr') ).remove();  
    /*
  alert(idContrato);
  alert($(objFila[0]).text());
  */
}
function eliminarContrato()
{
  var Nombre = $(this).attr("Nombre");

  var idContrato = $(this).attr("idContrato");
  $.post("php/eliminarContrato.php", {Nombre : Nombre, idContrato:idContrato}, function()
  {
    $("#trContrato_" + idContrato).remove();
  });
}