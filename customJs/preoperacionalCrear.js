function preoperacionalCrear()
{
  var f = new Date();
  var pFecha = f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2);
  $("#txtPreoperacionalCrear_Fecha").val(pFecha);

  $("#frmPreoperacionalCrear .chosen").chosen();

  $("#btnPreoperacionalCrear_Reset").on("click", btnPreoperacionalCrear_Reset_Click);
  $('#frmPreoperacionalCrear .text-toggle-button').toggleButtons({
        width: 200,
        label: {
            enabled: "Si",
            disabled: "No"
        }, onChange: function ($el, status, e) 
        {
          if (status)
          {
            $("#txtPreoperacionalCrear_Correctos span").text(parseInt($("#txtPreoperacionalCrear_Correctos span").text()) + 1);
            $("#txtPreoperacionalCrear_Incorrectos span").text(parseInt($("#txtPreoperacionalCrear_Incorrectos span").text()) - 1);
          } else
          {
            $("#txtPreoperacionalCrear_Correctos span").text(parseInt($("#txtPreoperacionalCrear_Correctos span").text()) - 1);
            $("#txtPreoperacionalCrear_Incorrectos span").text(parseInt($("#txtPreoperacionalCrear_Incorrectos span").text()) + 1);
          }

          
          $("#txtPreoperacionalCrear_Cumplimiento span").text(((parseInt($("#txtPreoperacionalCrear_Correctos span").text())/48)*100).toFixed(2));
          
          if (parseFloat($("#txtPreoperacionalCrear_Cumplimiento span").text()) < 80)
          {
            $("#txtPreoperacionalCrear_Evaluacion").removeClass("label-warning");
            $("#txtPreoperacionalCrear_Evaluacion").addClass("label-important");
            $("#txtPreoperacionalCrear_Evaluacion").text("No Cumple");
          } else
          {
            $("#txtPreoperacionalCrear_Evaluacion").addClass("label-warning");
            $("#txtPreoperacionalCrear_Evaluacion").removeClass("label-important");
            $("#txtPreoperacionalCrear_Evaluacion").text("Cumplió");
          }
        }
      });


  $("#txtPreoperacionalCrear_Fecha").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });
  $("#grpPreoperacionalCrearFechaInicio .add-on").on("click", function()
          {
            $("#txtPreoperacionalCrear_Fecha").datepicker("show");
          });

  $("#frmPreoperacionalCrear").on("submit", function(evento)
    {
      evento.preventDefault();
      var objChk = $("#preoperacionalCrear_Preguntas .text-toggle-button");
      var objObservaciones = $("#preoperacionalCrear_Preguntas textarea");


      $.each(objChk, function(index, control)
        {
          if($(objObservaciones[index]).val()!= "")
          {
            
          }
        });
    });
}
function btnPreoperacionalCrear_Reset_Click()
{
  var objChk = $("#preoperacionalCrear_Preguntas .text-toggle-button");

  $.each(objChk, function(index, control)
    {
      if (!$(control).toggleButtons("status"))
      {
        $(control).toggleButtons('setState', true);
      }
    });

  var f = new Date();
  var pFecha = f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2);
  $("#txtPreoperacionalCrear_Fecha").val(pFecha);
}