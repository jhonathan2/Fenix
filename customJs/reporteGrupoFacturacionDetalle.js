function reporteGrupoFacturacionDetalle()
{
  
  $("#frmReporteGrupoFacturacionDetalle").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteGrupoFacturacionDetalle_CargarFacturacionDetalle();
    });
  
}
function reporteGrupoFacturacionDetalle_CargarFacturacionDetalle()
 {
  $(".icoCargando").show();
  $.post("php/facturacion/cargarFacturacionDetalle.php", {Desde: $("#txtReporteGrupoFacturacionDetalle_Desde").val(), Hasta : $("#txtReporteGrupoFacturacionDetalle_Hasta").val()}, 
        function(data)
        {
            $("#tblGrupoFacturacionDetalle").dataTable().fnDestroy();
            $("#tblGrupoFacturacionDetalle tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";
         
            $.each(data, function(index, value)
              {
                tds += '<tr>';
                tds += '<td></td>';
                tds += '<td>' + value.Consecutivo + '</td>'
                tds += '<td>' + value.idFacturacionTerreno + '</td>'
                tds += '<td>' + value.idFacturacion + '</td>'
                tds += '<td>' + value.Prefijo + '</td>'
                tds += '<td>' + value.PrefijoIns + '</td>'
                tds += '<td>' + value.NumCuenta + '</td>'
                tds += '<td>' + value.NumMedidor + '</td>'
                tds += '<td>' + value.Sucursal + '</td>'
                tds += '<td>' + value.Direccion + '</td>'
                tds += '<td>' + value.Barrio + '</td>'
                tds += '<td>' + value.Ciclo + '</td>'
                tds += '<td>' + value.Grupo + '</td>'
                tds += '<td>' + value.Lectura + '</td>'
                tds += '<td>' + value.Anomalia + '</td>'
                tds += '<td>' + value.EntregaFact + '</td>'
                tds += '<td>' + value.EntregaOpor + '</td>'
                tds += '<td>' + value.Cumple + '</td>'
                tds += '<td>' + value.Observaciones + '</td>'
                tds += '</tr>';

              });
            $("#tblGrupoFacturacionDetalle tbody").append(tds);
            $("#tblGrupoFacturacionDetalle").crearTabla1({lblMenu : "Grupos de Facturación por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }

 