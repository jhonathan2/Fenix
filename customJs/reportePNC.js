function reportePNC()
{
  $("#frmReportePNC").on("submit", function(evento)
    {
      evento.preventDefault();
      frmReportePNC_Submit();
    });
  
}
 
 function frmReportePNC_Submit()
 {
  $(".icoCargando").show();
  $.post("php/reportePNC.php", {Desde: $("#txtReportePNC_Desde").val(), Hasta : $("#txtReportePNC_Hasta").val()}, 
        function(data)
        {
            $("#tblPNC").dataTable().fnDestroy();
            $("#tblPNC tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";

            $.each(data, function(index, value)
              {
                tds += '<tr>';
                tds += '<td>' + value.Proceso + '</td>';
                tds += '<td>' + value.EmpresaColaboradora + '</td>';
                tds += '<td>' + value.NoOrden + '</td>';
                tds += '<td>' + value.FechaAnalis + '</td>';
                tds += '<td>' + value.FechaEjecucionOt + '</td>';
                tds += '<td>' + value.FechaCargueEpica + '</td>';
                tds += '<td>' + value.Cuadrilla + '</td>';
                tds += '<td>' + value.Zona + '</td>';
                tds += '<td>' + value.Subzona + '</td>';
                tds += '<td>' + value.Municipio + '</td>';
                tds += '<td>' + value.MotivoDelRechazo + '</td>';
                tds += '<td>' + value.Observaciones + '</td>';
                tds += '<td>' + value.AreaDeOrigen + '</td>';
                tds += '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
              });
            $("#tblPNC tbody").append(tds);
            $("#tblPNC").crearTabla1({lblMenu : "PNC por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }
