function crearMaterial()
{
  $("#grpMaterialFechaInicio input, #grpMaterialFechaTerminacion input").datepicker("destroy");
    $("#grpMaterialFechaInicio input, #grpMaterialFechaTerminacion input").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });

          $("#grpMaterialFechaInicio .add-on").on("click", function()
          {
            $("#grpMaterialFechaInicio input").datepicker("show");
          });
          $("#grpMaterialFechaTerminacion .add-on").on("click", function()
          {
            $("#grpMaterialFechaTerminacion input").datepicker("show");
          });

          $("#frmCrearMaterial").on("submit", function(evento)
            {
              evento.preventDefault();
              var pImagen = $("#frmCrearMaterial .fileupload-preview img").attr("src");
              if (typeof pImagen == "undefined")
              {
                pImagen = "";
              }
                
              $.post("php/crearMaterial.php",
              {
                nombre : $('#txtMaterial_Nombre').val(),
                marca : $('#txtMaterial_Marca').val(),
                referencia : $('#txtMaterial_Referencia').val(),
                imagen : pImagen,
                fechaCompra : $('#txtMaterial_FechaInicio').val(),
                fechaAsignacion : $('#txtMaterial_FechaTerminacion').val(),
                usuario : $('#txtMaterial_Usuario').val(),
                valor : $('#txtMaterial_Valor').val(),
                descripcion : $('#txtMaterial_Descripcion').val(),
                accesorios : $('#txtMaterial_Accesorios').val(),
                observaciones : $('#txtMaterial_Observaciones').val()
              }, function(data, textStatus, xhr)
              {
                if (data == 1)
                {
                  Mensaje("Ok", "El Material ha sido almacenado.");    
                  //$("#frmCrearMaterial")[0].reset();
                } else
                {
                  Mensaje("Error", data);    
                }
              }).always(function() 
              {
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No fue posible almacenar el Material, por favor intenta nuevamente.");
              });
              
            });
  $("#txtMaterial_Usuario").cargarUsuarios({idUsuario: Usuario.id},function(Usuarios)
          {
            var idx = 0;
            var tds = '<option value="0">Seleccione un Usuario</option>';
            $.each(Usuarios, function(index, Usuario)
              {
                if (Usuario.idPerfil != 8)
                {
                  tds += '<option value="' + Usuario.idLogin + '">' + Usuario.Nombre + '</option>';

                  idx++
                }
              });
            if (idx == 0)
            {
              Mensaje("Error", "No fue posible cargar los Usuarios, por favor actualiza la página.");
            } else
            {
              $("#txtMaterial_Usuario option").remove();
              $("#txtMaterial_Usuario").append(tds);
            } 
             $("#frmCrearMaterial .chosen").chosen();
          }, "json");
}
