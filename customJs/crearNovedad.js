function crearNovedad()
{
  $("#frmCrearNovedad .chosen").chosen();
  $("#txtCrearNovedad_Tipo").on("change", function()
  {
    if ($("#txtCrearNovedad_Tipo").val()==1)
    {
      $("#lblCrearNovedad_Usuario").text("Nombre de Usuario");
    } else
    {
      $("#lblCrearNovedad_Usuario").text("Placa");
    }
  });
  $("#frmCrearNovedad").on("submit", function(evento)
            {
              evento.preventDefault();
              $.post("php/crearNovedad.php",
              {
                usuario : $('#txtCrearNovedad_Usuario').val(),
                titulo : $('#txtCrearNovedad_Nombre').val(),
                novedad : $('#txtCrearNovedad_Observaciones').val()
              }, function(data, textStatus, xhr)
              {
                if (data == 1)
                {
                  Mensaje("Ok", "La Novedad ha sido almacenada.");    
                  $("#frmCrearNovedad")[0].reset();
                } else
                {
                  Mensaje("Error", data);    
                }
              }).always(function() 
              {
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No fue posible almacenar la Novedad, por favor intenta nuevamente.");
              });
              
            });
}