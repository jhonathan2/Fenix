var ipalItemsEvaluados;
var pPrefijo = " ";

function cargarIpal()
{
  $("#txtIpal_OtraEmpresa").hide();
  $("#txtIpal_Nombre").on("change", txtIpal_Nombre_change);
  $("#btnIpal_AgregarCuadrillero").on("click", btnIpal_AgregarCuadrillero_Click);
  $("#frmIpal .delete").live("click", function()
    {
      var objTr = $(this).parent("td").parent("tr");
      $(objTr).remove();
    });
  //ipalItemsEvaluados = new Array();
  ipalItemsEvaluados = {};
  var f = new Date();
  //pPrefijo = CompletarConCero(Usuario.id, 3) + f.getFullYear() + CompletarConCero(f.getMonth() +1, 2) + CompletarConCero(f.getDate(), 2) + CompletarConCero(f.getHours(), 2) + CompletarConCero(f.getMinutes(), 2) + CompletarConCero(f.getSeconds(), 2);
  pPrefijo = $("#txtPrefijo").val();
  $("#ipalSubir").attr("src" ,"subir/index.html?Prefijo=Ipales/" + pPrefijo);
  $("#txtIpal_Responsable").val(Usuario.nombre);
  $("#txtIpal_ResponsableCargo").val(Usuario.cargo + "-" + Usuario.empresa);
  $.post("php/ipal/cargarDescripcion.php", {},
    function(data)
    {
      var tds ="";
      $.each(data, function(index, value)
        {
          tds +=  '<li class="span12 cargarIpal" idGrupo="' + value.id + '">';
          tds +=    '<a href="#tab' + value.id + '" data-toggle="tab" class="step active">';
          tds +=      '<span class="number">' + value.id + '</span>';
          tds +=      '<span class="desc"><i class="icon-ok"></i> ' + value.Nombre + '</span>';
          tds +=    '</a>';
          tds +=  '</li>';
        });
          $("#form_wizard_1 form ul").append(tds);
          objWizard();
    },"json");

  $("#btnIpal_IrAMenu").on("click", function(evento)
  {
    evento.preventDefault();
    App.scrollTo($('.form-wizard'));
  });

  $("#btnIpalReset").on("click", nuevoIpal);

  $("#frmIpal").on("submit", function(evento)
  {
    evento.preventDefault();
    
    var f = new Date();

    var objCuadrillaCedula = $(".txtIpal_CuadrilleroCedula");
    var objCuadrillaNombre = $(".txtIpal_CuadrilleroNombre");
    var objCuadrillaCargo = $(".txtIpal_CuadrilleroCargo");

    var Cuadrillas = "";
    $.each(objCuadrillaCedula, function(index, value)
      {
        Cuadrillas += $(objCuadrillaCedula[index]).val() + ";" + $(objCuadrillaNombre[index]).val() + ";" + $(objCuadrillaCargo[index]).val() + "_._";
      });
    var pEmpresa = "";
    if ($("#txtIpal_OtraEmpresa").val() != "")
    {
      pEmpresa = $("#txtIpal_OtraEmpresa").val();
    } else
    {
      pEmpresa = $("#txtIpal_Nombre").val();
    }

    $.post("php/ipal/crearIpal.php", {
         Empresa : pEmpresa,
         Direccion : $("#txtIpal_Direccion").val(),
         Celular : $("#txtIpal_Celular").val(),
         vehiculoTipo : $("#txtIpal_TipoVehiculo").val(),
         vehiculoPlaca : $("#txtIpal_PlacaVehiculo").val(),
         gruaPlaca : $("#txtIpal_PlacaGrua").val(),
         canastaPlaca : $("#txtIpal_PlacaCanasta").val(),
         motoPlaca : $("#txtIpal_PlacaMoto").val(),
         noContrato : $("#txtIpal_NoContrato").val(),
         Trabajo : $("#txtIpal_Trabajo").val(),
         Proceso : $("#txtIpal_Proceso").val(),
         Zona : $("#txtIpal_Zona").val(),
         Cuadrilla : Cuadrillas,
         jefeCuadrilla : $("#txtIpal_Recibio").val(),
         cargoEmpresa : $("#txtIpal_RecibioCargo").val(),
         Observaciones : $("#txtIpal_Observaciones").val(),
         Resultados : ipalItemsEvaluados,
         Prefijo : pPrefijo,
         idLogin : Usuario.id
    },
      function(data)
      {
        if (!isNaN(data))
        {
          Mensaje("Ok", "El Ipal ha sido almacenado.");    
          nuevoIpal();
        } else
        {
          Mensaje("Error", data);
        }
      }).always(function() 
        {
          //Cuando Finaliza
        }).fail(function() {
          Mensaje("Error", "No fue posible almacenar el Ipal, por favor intenta nuevamente.");
        });
  })
}

function objWizard() 
{
    if (!jQuery().bootstrapWizard) {
        return;
    }

    $('#form_wizard_1').bootstrapWizard({
        'nextSelector': '.button-next',
        'previousSelector': '.button-previous',
        onTabClick: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set wizard title
            App.scrollTo($('#bar'));
        },
        onNext: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set done steps
        },
        onPrevious: function (tab, navigation, index) {
            var total = navigation.find('li').length;
            var current = index + 1;
            // set wizard title
        },
        onTabShow: function (tab, navigation, index) {
          $("#form_wizard_1 .page-title:visible").text($(tab).find(".desc").text());
            var total = navigation.find('li').length;
            //var current = index + 1;
            var li_list = navigation.find('li');
            jQuery(li_list[index]).addClass("done");
            var current = $(".done").length;
            var $percent = (current / total) * 100;
            
            $('#form_wizard_1').find('.bar').css({
                width: $percent + '%'
            });
        }
    });
}

function chkIpal_IniciarBotones()
{
  $('#form_wizard_1 .text-toggle-button').toggleButtons({
            width: 200,
            label: {
                enabled: "Si Aplica",
                disabled: "No Aplica"
            }
        });

    $(".chkIpal_Aplica").on("change", chkIpal_Aplica_Click);
}
function chkIpal_Aplica_Click()
{
  var Estado = $(this).find(".chkIpal_Aplica_Control").is(":checked");
  var Numeral = $(this).find(".chkIpal_Aplica_Control").attr("Numeral");
  var objControl = $(this).parent(".controls");

  if (!Estado)
  {
    $(objControl).find(".text-toggle-button2").remove();
    ipalItemsEvaluados[Numeral] = "NA";
  } else
  {
    if ($(objControl).find(".text-toggle-button2").length == 0)
    {
      ipalItemsEvaluados[Numeral] = "Si";
      $(objControl).append('<div class="text-toggle-button2 noMostrar"><input type="checkbox" class="toggle chkIpalEvaluado" Numeral="' + Numeral + '" checked/></div>');
      $(objControl).find(".text-toggle-button2").toggleButtons({
              width: 200,
              label: {
                  enabled: "Si",
                  disabled: "No"
              }
          });
    }
  }

}
function cargarItemsIpal()
{
  var idGrupo = $(this).attr("idGrupo");
  $("#tab1 div").remove();

 $.post("php/ipal/cargarItems.php", {grupo : idGrupo},
    function(data)
    {
      var tds ="";
      $.each(data, function(index, value)
        {
          tds +='<div class="control-group">';
          tds +=    '<label class="control-label span12">' + value.Nombre + '</label>';
          tds +=    '<div class="controls">';
          tds +=        '<div class="text-toggle-button chkIpal_Aplica">';
          tds +=            '<input type="checkbox" class="toggle chkIpal_Aplica_Control" Numeral="' + value.Numeral + '"/>';
          tds +=        '</div>';
          tds +=    '</div>';
          tds +='</div>';
        });

          $("#tab1").append(tds);
          chkIpal_IniciarBotones();
    },"json"); 
}
function nuevoIpal()
{
  $("#modulo_ipal").remove();
  //$(this).cargarModulo({pagina : "ipal", titulo : "IPAL", icono : "icon-edit"});
}
function cambiarEstadoIpal_Item()
{
  var pNumeral = $(this).attr("Numeral");
  if ($(this).is(":checked"))
  {
    ipalItemsEvaluados[pNumeral] = "Si";
  } else
  {
    ipalItemsEvaluados[pNumeral] = "No";
  }
}
function btnIpal_AgregarCuadrillero_Click(evento)
{
  evento.preventDefault();
  var tds = '<tr class="">';
      tds += '<td><input class="span12 txtIpal_CuadrilleroCedula"></td>';
      tds += '<td><input class="span12 txtIpal_CuadrilleroNombre"></td>';
      tds += '<td><input class="span12 txtIpal_CuadrilleroCargo"></td>';
      tds += '<td><a class="delete" href="javascript:;">Borrar</a></td>';
      tds += '</tr>';
  $("#tableIpalCuadrilla tbody").append(tds);
}
function txtIpal_Nombre_change()
{
  var pSelected = $(this).val();
  
    $("#txtIpal_OtraEmpresa").val("");
    if (pSelected == "1")
    {
      $("#txtIpal_OtraEmpresa").show();
    } else
    {
      $("#txtIpal_OtraEmpresa").hide();
    }
}