function SGD()
{

  $("#frmSGD").on("submit", function(evento)
    {
      evento.preventDefault();
      SGD_CargarComercial();
    });

  $(".lblLinkArchivo").live("click", lblLinkArchivo_Click);
}

 function SGD_CargarComercial()
{
  $(".icoCargando").show();
  $.post("php/cargarDocumental.php", 
    {
      Desde: $("#txtSGD_Desde").val(), 
      Hasta : $("#txtSGD_Hasta").val(),
      Consecutivo : $("#txtSGD_Consecutivo").val(),
      Descripcion : $("#txtSGD_Descripcion").val(),
      Nombre : $("#txtSGD_Nombre").val()
    }, 
      function(data)
      {
          $("#tblSGD").dataTable().fnDestroy();
          $("#tblSGD tbody tr").remove();
        if (data == 0)
        {
          Mensaje("Hey", "No se encontraron resultados");
        } else
        {
          var tds = "";
          var icono = "";
          var extension = "";


          $.each(data, function(index, value)
            {
              extension = value.Ruta.substring(value.Ruta.lastIndexOf(".")).toLowerCase();
              console.log(value.Consecutivo + ": " + extension);
              switch (extension)
              {
                case ".xls":
                  icono = "xls";
                  break;
                case ".xlsx":
                  icono = "xls";
                  break;
                case ".doc":
                  icono = "doc";
                  break;
                case ".docx":
                  icono = "doc";
                  break;
                case ".gif":
                  icono = "jpg";
                  break;
                case ".jpg":
                  icono = "jpg";
                  break;
                case ".jpeg":
                  icono = "jpg";
                  break;
                case ".png":
                  icono = "jpg";
                  break;
                case ".pdf":
                  icono = "pdf";
                  break;
                case ".ppt":
                  icono = "ppt";
                  break;
                case ".pptx":
                  icono = "ppt";
                  break;
                case ".zip":
                  icono = "zip";
                  break;
                case ".rar":
                  icono = "zip";
                  break;
                default:
                  icono = "eps";
              }

              tds += '<tr>';
              tds += '<td>' + value.Fecha + '</td>';
              tds += '<td>' + value.Tipo + '</td>';
              tds += '<td>' + value.Consecutivo + '</td>';
              tds += '<td>' + value.Nombre + '</td>';
              tds += '<td>' + value.Descripcion + '</td>';
              tds += '<td>' + value.Usuario + '</td>';
              tds += '<td><a href="#" class="lblLinkArchivo" Ruta="' + value.Ruta + '"><img src="img/file-search/' + icono + '.png" width="20"> Abrir</a></td>'
              tds += '</tr>';

            });
          $("#tblSGD tbody").append(tds);
          $("#tblSGD").crearTabla1({lblMenu : "Documentos por página"});
        }
      }, 'json').always(function() 
      {
        //Cuando Finaliza
        $(".icoCargando").hide();
      }).fail(function() {
        Mensaje("Error", "No fue posible conectar con el Servidor");
      });
}
function lblLinkArchivo_Click()
{
  popupWin = window.open($(this).attr("Ruta"), 'open_window');
}
