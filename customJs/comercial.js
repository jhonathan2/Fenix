function Comercial()
{
  var f = new Date();
  pPrefijo = $("#txtPrefijo").val();
  $("#comercialSubir").attr("src" ,"subir/index.html?Prefijo=Comercial/" + pPrefijo);

  verContratos();

  $(".toggle").toggleButtons({
              width: 200,
              label: {
                  enabled: "Si",
                  disabled: "No"
              }
          });

  $("#btnComercial_TomarCoordenadas").on("click", function(event) 
  {
    event.preventDefault();
    Mensaje("Ubicar", "Se han tomado coordenadas de: Carrera 11A # 93-2 a 93-100");
  });
  
  //$("#txtComercial_Ejecutor").val("admin");
  comercialValoresDefecto();

  $("#txtComercial_Nombre").on("change", cargarOrden);
  $("#frmComercial .chosen").chosen();
  $("#grpComercialFechaInicio input, #grpComercialFechaTerminacion input").datepicker("destroy");
    $("#grpComercialFechaInicio input, #grpComercialFechaTerminacion input").datepicker(
          {
            changeMonth: true,
            changeYear: true
          });

          $("#grpComercialFechaInicio .add-on").on("click", function()
          {
            $("#grpComercialFechaInicio input").datepicker("show");
          });
          $("#grpComercialFechaTerminacion .add-on").on("click", function()
          {
            $("#grpComercialFechaTerminacion input").datepicker("show");
          });

        $("#btnComercial_Reset").on("click", comercialValoresDefecto);
          $("#frmComercial").on("submit", function(evento)
            {
              evento.preventDefault();
              checkValor("txtComercial_Cumple");
              $.post("php/comercial/crearComercial.php",
              {
                Prefijo : pPrefijo,
                idLogin : Usuario.id,
                OT : $("#txtComercial_Nombre").val(),
                FechaIngreso : $("#txtComercial_FechaInicio").val(),
                HoraInicio : $("#txtComercial_HoraIni").val(),
                Direccion  : $("#txtComercial_Direccion").val(),
                Atendio : $("#txtComercial_Atendio").val(),
                Telefono : $("#txtComercial_Telefono").val(),
                TipoInterventoria : $("#txtComercial_TipoInterventoria").val(),
                SubProceso : $("#txtComercial_SubProceso").val(),
                Medida : $("#txtComercial_Medida").val(),
                SE : $("#txtComercial_Se").val(),
                Medida2 : $("#txtComercial_Medida2").val(),
                ActividadEconomica : $("#txtComercial_ActividadEconomica").val(),
                Factor : $("#txtComercial_Factor").val(),
                MedActiva : $("#txtComercial_MedActiva").val(),
                LecActiva : $("#txtComercial_LecActiva").val(),
                TipoFase : $("#txtComercial_TipoFase").val(),
                Marca : $("#txtComercial_Marca").val(),
                Red : $("#txtComercial_Red").val(),
                AccesoMedidor : checkValor("txtComercial_AccesoMedido"),
                TipoAcometida : $("#txtComercial_TipoAcometida").val(),
                AcometidaCompartida : checkValor("txtComercial_AcometidaCompartida"),
                LocalizacionMedidor : $("#txtComercial_LocalizacionMedidor").val(),
                MedidaReferencia : $("#txtComercial_MedidaReferencia").val(),
                Sellos : $("#txtComercial_Sellos").val(),
                CapaTrafo : $("#txtComercial_CapaTrafo").val(),
                CD : $("#txtComercial_CD").val(),
                PuntoFisico : $("#txtComercial_PuntoFisico").val(),
                Observaciones : $("#txtComercial_Observaciones").val(),
                pFormatoSupervision : checkValor("txtComercial_FormatoSupervision"),
                Cuadrillas : $("#txtComercial_Cuadrillas").val(),
                InspeccionesRealizadas : checkValor("txtComercial_InspeccionesRealizadas"),
                PlanSupervision : $("#txtComercial_PlanSupervision").val(),
                ObservacionesCliente : $("#txtComercial_ObservacionesCliente").val(),
                ObservacionesPredio : $("#txtComercial_ObservacionesPredio").val(),
                Cumple : checkValor("txtComercial_Cumple"),
                CodIncumplimiento : $("#txtComercial_CodIncumplimiento").val(),
                CruadrillaTipo :  $('#txtComercial_CruadrillaTipo').val(),
                CuadrillaNum :  $('#txtComercial_CuadrillaNum').val(),
                CuadrillaSupervisor :  $('#txtComercial_CuadrillaSupervisor').val(),
                CedulaSupervisor :  $('#txtComercial_CedulaSupervisor').val(),
                SupervisorCelular :  $('#txtComercial_SupervisorCelular').val(),
                CuadrillaAux1 :  $('#txtComercial_CuadrillaAux1').val(),
                CuadrillaAux2 :  $('#txtComercial_CuadrillaAux2').val(),
                CuadrillaTec1 :  $('#txtComercial_CuadrillaTec1').val(),
                CuadrillaCelulares :  $('#txtComercial_CuadrillaCelulares').val(),
                CuadrillaCedulas :  $('#txtComercial_CuadrillaCedulas').val(),
                NoOrden :  $('#txtComercial_NoOrden').val(),
                DetalleActividad :  $('#txtComercial_DetalleActividad').val()
              }, function(data, textStatus, xhr)
              {
                if (data > 0)
                {
                  Mensaje("Ok", "La Ejecución ha sido almacenada, por favor diligencie la segunda parte.");    
                  $("#frmComercial")[0].reset();
                  if ($("#txtComercial_idContrato").val() != "")
                  {
                    $("#trContrato_" + $("#txtComercial_idContrato").val()).remove();
                  }
                  $("#modulo_comercial").remove();
                  $(this).cargarModulo({pagina : "CT_Int", titulo : "Interventoría" + data, icono : "icon-copy"}, function()
                    {
                      $("#frmCTInt").attr("idInspeccion", data);
                    });
                  
                } else
                {
                  Mensaje("Error", data);    
                }
              }).always(function() 
              {
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No fue posible almacenar La Ejecución, por favor intenta nuevamente.");
              });
              
            });
}
function comercialValoresDefecto()
{
  var f = new Date();
  $("#grpComercialFechaInicio input").val(f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2));
  $("#txtComercial_HoraIni").val( CompletarConCero(f.getHours(), 2) + ":" + CompletarConCero(f.getMinutes(), 2));
}

function cargarOrden()
{
  var pOt = $(this).val();
  if (pOt.length > 4)
  {
    $.post("php/buscarContratos.php", 
      {parametros: "Nombre", criterio : pOt, ejecutados : "true"}, 
      function(data)
      {
        var idx = 0;
        $.each(data, function(index, value)
          {
            if (value.Estado == "Ejecutado")
            {
              Mensaje("Hey", "Esa orden ya fue ejecutada.");      
            }
              $("#txtComercial_idContrato").val(value.idContrato);
              $("#txtComercial_Nombre").val(value.Nombre);
              $("#txtComercial_Direccion").val(value.Contratista);
              $("#txtComercial_Atendio").val(value.Contratante);
              idx++;
          });
        if (idx == 0)
        {
          Mensaje("Hey", "El Numero de Orden no está en la bolsa de Pendientes."); 
        }

      }, "json").fail(function()
      {
        Mensaje("Error", "No hay conexión con el servidor.");
      });
  }
}