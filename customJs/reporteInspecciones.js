function reporteInspecciones()
{
  
  $("#frmReporteInspecciones").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteInspecciones_CargarInspecciones();
    });
  
}
function reporteInspecciones_CargarInspecciones()
 {
  $(".icoCargando").show();
  $.post("php/cargarInspecciones.php", {Desde: $("#txtReporteInspecciones_Desde").val(), Hasta : $("#txtReporteInspecciones_Hasta").val()}, 
        function(data)
        {
            $("#tblInspecciones").dataTable().fnDestroy();
            $("#tblInspecciones tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";

            $.each(data, function(index, value)
              {
                tds += '<tr>';
                tds += '<td></td>';
                tds += '<td>' + value.idInspeccion + '</td>';
                tds += '<td>' + value.fechaIngreso + '</td>';
                tds += '<td>' + value.Actividad + '</td>';
                tds += '<td>' + value.Veredal + '</td>';
                tds += '<td>' + value.Adjudicado2015 + '</td>';
                tds += '<td>' + value.Total + '</td>';
                tds += '<td>' + value.Nombre + '</td>';
                tds += '<td>' + value.Tipo + '</td>';
                tds += '</tr>';

              });
            $("#tblInspecciones tbody").append(tds);
            $("#tblInspecciones").crearTabla1({lblMenu : "Inspecciones por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }
