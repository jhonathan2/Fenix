function reporteParaFactura()
{
  $("#btnReporteParaFactura_Descargar").on("click", btnReporteParaFactura_Descargar_Click);

  $("#frmReporteParaFactura").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteParaFactura_Cargar();
    });
  
}
function reporteParaFactura_Cargar()
 {
  $(".icoCargando").show();
  $.post("php/cargarParaFactura.php", {Desde: $("#txtReporteParaFactura_Desde").val(), Hasta : $("#txtReporteParaFactura_Hasta").val()}, 
        function(data)
        {
            //$("#tblParaFactura").dataTable().fnDestroy();
            $("#tblParaFactura tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";
            var SubZona = "";
            var granTotal = 0;
         
            $.each(data, function(index, value)
              {
                tds += '<tr>';
                tds += '<td>' + value.SubZona + '</td>';
                tds += '<td>' + value.Municipio + '</td>';
                tds += '<td>$ ' + separadorMiles(value.Total) + '</td>';
                tds += '</tr>';
                granTotal += parseFloat(value.Total);

              });

            tds += "<tr><td colspan = '3'></td><tr>";
            tds += "<tr><td colspan='2' >SUBTOTAL FACTURA</td><td>" + separadorMiles(granTotal) + "</td></tr>";
            tds += "<tr><td></td><td>Iva</td><td>" + separadorMiles(granTotal*0.16)+ "</td></tr>";
            tds += "<tr><td colspan='2' >TOTAL FACTURA</td><td>" + separadorMiles(granTotal * 1.16) + "</td></tr>";
            $("#tblParaFactura tbody").append(tds);
            //$("#tblParaFactura").crearTabla1({lblMenu : "Grupos de Facturación por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }
function btnReporteParaFactura_Descargar_Click(evento)
{
  evento.preventDefault();
  window.open("php/cargarParaFacturaXLS.php?Desde=" + $("#txtReporteParaFactura_Desde").val() +"&Hasta=" + $("#txtReporteParaFactura_Hasta").val());

}