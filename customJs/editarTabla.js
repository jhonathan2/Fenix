function editarTabla()
{
  cargarEncabezado("Baremo");
  cargarDatos("Baremo");
}

function cargarEncabezado(nomTabla)
{
  $.post("php/editarTablaEncabezado.php", {tabla :  nomTabla},
    function(data)
    {
      $("#tblEditarTabla thead th").remove();
      var tds = "";
      $.each(data, function(index, val) 
      {
        tds += "<th>" + val.Campo + "</th>";
      });
      $("#tblEditarTabla thead").append(tds);
    }, "json");
}

function cargarDatos(nomTabla)
{
  $.post("php/editarTablaDatos.php", {tabla :  nomTabla},
    function(data)
    {
      $("#tblEditarTabla tbody tr").remove();
      var tds = "";
      $.each(data, function(index, val) 
      {
        tds += "<tr>";
        $.each(val, function(index2, valor) 
        {
          tds += "<td>" + valor + "</td>";
        });
        tds += "</tr>";
      });
      $("#tblEditarTabla tbody").append(tds);
      $("#tblEditarTabla").crearTabla1({lblMenu : "Resultados por página"});
    }, "json");
}