
function editarIpal()
{
  $(".lblEditarIpal_BorrarItem").live("click", lblEditarIpal_BorrarItem_click);

  $.post("php/ipal/cargarIpalEd.php", {Prefijo: $("#txtPrefijo").val()}, 
    function(data)
    {
      var resultado = data[0];
      console.log(resultado);
      $("#txtEditarIpal_Nombre").val(resultado.Empresa);
      $("#txtEditarIpal_Direccion").val(resultado.Direccion);
      $("#txtEditarIpal_Celular").val(resultado.Celular);
      $("#txtEditarIpal_TipoVehiculo").val(resultado.VehiculoTipo);
      $("#txtEditarIpal_PlacaVehiculo").val(resultado.VehiculoPlaca);
      $("#txtEditarIpal_PlacaGrua").val(resultado.GruaPlaca);
      $("#txtEditarIpal_PlacaCanasta").val(resultado.CanastaPlaca);
      $("#txtEditarIpal_PlacaMoto").val(resultado.MotoPlaca);
      $("#txtEditarIpal_NoContrato").val(resultado.NoContrato);
      $("#txtEditarIpal_Trabajo").val(resultado.Trabajo);
      $("#txtEditarIpal_Proceso").val(resultado.Proceso);
      $("#txtEditarIpal_Zona").val(resultado.Zona);

      var Cuadrilla = resultado.Cuadrilla.split("_._");
      var objCuadrilla = {};
      var tds = "";
      $("#tableEditarIpalCuadrilla tbody tr").remove();

      $.each(Cuadrilla, function(index, value)
        { 
          objCuadrilla = value.split(";");
          
          if (objCuadrilla.length > 1)
          {
            tds += "<tr>";
            tds += "<td>";
            tds += "<input value='" + objCuadrilla[0] + "'>";
            tds += "</td>";
            tds += "<td>";
            tds += "<input value='" + objCuadrilla[1] + "'>";
            tds += "</td>";
            tds += "<td>";
            tds += "<input value='" + objCuadrilla[2] + "'>";
            tds += "</td>";
            tds += "<td></td>";
            tds += "</tr>";
          }
        });
          $("#tableEditarIpalCuadrilla tbody").append(tds);

      $("#txtEditarIpal_Observaciones").val(resultado.Observaciones);
      $("#txtEditarIpal_Recibio").val(resultado.Cudrillero);
      $("#txtEditarIpal_RecibioCargo").val(resultado.Cuadrillero2);
      $("#txtEditarIpal_Resultado").val(resultado.resultado);

      $("#tableEditarIpalResultados tbody tr").remove();
      tds = "";
      $.each(resultado.itemsEvaluados, function(index, val) 
      { 
          tds += "<tr>";
          tds += "<td>" + val.Numeral + "</td>";
          tds += "<td>" + val.Descripcion + "</td>";
          tds += "<td>" + val.Resultado + "</td>";
          tds += "<td>" + val.Observaciones + "</td>";
          tds += "<td><a idResultado='" + val.idResultado + "' class='lblEditarIpal_BorrarItem'><i class='icon-trash'></i> </a></td>";
          tds += "</tr>";
      });

      $("#tableEditarIpalResultados tbody").append(tds);
    }, "json");
  
  var f = new Date();
  
  var pPrefijo = $("#txtPrefijo").val();
  $("#ipalSubir").attr("src" ,"subir/index.html?Prefijo=Ipales/" + pPrefijo);
  
  
  $("#frmEditarIpal").on("submit", function(evento)
  {
    evento.preventDefault();
    
    var f = new Date();

    $.post("php/ipal/editarIpal.php", {
         Empresa : $("#txtEditarIpal_Nombre").val(),
         Direccion : $("#txtEditarIpal_Direccion").val(),
         Celular : $("#txtEditarIpal_Celular").val(),
         vehiculoTipo : $("#txtEditarIpal_TipoVehiculo").val(),
         vehiculoPlaca : $("#txtEditarIpal_PlacaVehiculo").val(),
         gruaPlaca : $("#txtEditarIpal_PlacaGrua").val(),
         canastaPlaca : $("#txtEditarIpal_PlacaCanasta").val(),
         motoPlaca : $("#txtEditarIpal_PlacaMoto").val(),
         noContrato : $("#txtEditarIpal_NoContrato").val(),
         Trabajo : $("#txtEditarIpal_Trabajo").val(),
         Proceso : $("#txtEditarIpal_Proceso").val(),
         Zona : $("#txtEditarIpal_Zona").val(),
         jefeCuadrilla : $("#txtEditarIpal_Recibio").val(),
         cargoEmpresa : $("#txtEditarIpal_RecibioCargo").val(),
         Observaciones : $("#txtEditarIpal_Observaciones").val(),
         resultadoFinal : $("#txtEditarIpal_Resultado").val(),
         Prefijo : pPrefijo
    },
      function(data)
      {
        if (!isNaN(data))
        {
          Mensaje("Ok", "El Ipal ha sido almacenado.");    
          nuevoIpal();
        } else
        {
          Mensaje("Error", data);
        }
      }).always(function() 
        {
          //Cuando Finaliza
        }).fail(function() {
          Mensaje("Error", "No fue posible almacenar el Ipal, por favor intenta nuevamente.");
        });
  })
}

function lblEditarIpal_BorrarItem_click()
{
  var pIdResultado = $(this).attr("idResultado");
  var objFila = $(this).parent("td").parent("tr");
  $.post("php/ipal/borrarItem.php", {idResultado : pIdResultado}, function()
    {
      $(objFila).remove();
    });
}