function reporteAlumbrado()
{
  $("#frmReporteAlumbradoCompleto").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteAlumbrado_CargarIpalCompleto();
    });
    $("#btnReporteAlumbradoCompleto_Descargar").on("click", function(evento)
        {
            evento.preventDefault();
            var url = "http://fenix.wspcolombia.com/php/alumbrado/crearCSVAlumbradoCompleto.php?Desde=" + $("#txtReporteAlumbradoCompleto_Desde").val() + "&Hasta=" + $("#txtReporteAlumbradoCompleto_Hasta").val();
            window.open(url, "nuevo", "directories=no, location=no, menubar=no, scrollbars=no, statusbar=no, tittlebar=no, width=400, height=400");
        });

    $(".lblArchivosAlumbrado").live("click", btnAlumbradoDetalle_Archivos_Click);
}
 
 function reporteAlumbrado_CargarIpalCompleto()
 {
    $(".icoCargando").show();
  $.post("php/alumbrado/cargarAlumbradoCompleto.php", {Desde: $("#txtReporteAlumbradoCompleto_Desde").val(), Hasta : $("#txtReporteAlumbradoCompleto_Hasta").val()}, 
        function(data)
        {
            $("#tblAlumbradoCompleto").dataTable().fnDestroy();
            $("#tblAlumbradoCompleto tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";

            $.each(data, function(index, value)
              {
                var Estado = "success";
                if (value.Resultado != "Cumple")
                {
                  Estado = "error";
                } 
                tds += '<tr>';
                tds += '<td><a class="lblArchivosAlumbrado" Prefijo ="' + value.Prefijo + '" Consecutivo="' + value.ID + '">' + value.ID + '</a></td>';
                tds += '<td>' + value.OT + '</td>';
                tds += '<td>' + value.TIPO_OT + '</td>';
                tds += '<td>' + value.FECHA_EJEC + '</td>';
                tds += '<td>' + value.EJECUTOR + '</td>';
                tds += '<td>' + value.MUNICIPIO + '</td>';
                tds += '<td>' + value.CD + '</td>';
                tds += '<td>' + value.Trafo_nox + '</td>';
                tds += '<td>' + value.Trafo_noy + '</td>';
                tds += '<td>' + value.trafo_capa + '</td>';
                tds += '<td>' + value.trafo_fase + '</td>';
                tds += '<td>' + value.trafo_ubic + '</td>';
                tds += '<td>' + value.MUNICIPIO + '</td>';
                tds += '<td>' + value.trafo_barr + '</td>';
                tds += '<td>' + value.trafo_dir + '</td>';
                tds += '<td>' + value.clase_red + '</td>';
                tds += '<td>' + value.uso_red + '</td>';
                tds += '<td>' + value.punto_sig + '</td>';
                tds += '<td>' + value.LUMINARIA + '</td>';
                tds += '<td>' + value.ILU_DIR + '</td>';
                tds += '<td>' + value.ilu_nox + '</td>';
                tds += '<td>' + value.ilu_noy + '</td>';
                tds += '<td>' + value.ilu_tipoco + '</td>';
                tds += '<td>' + value.ilu_cant + '</td>';
                tds += '<td>' + value.ilu_tiplam + '</td>';
                tds += '<td>' + value.ilu_ilumin + '</td>';
                tds += '<td>' + value.ilu_propie + '</td>';
                tds += '<td>' + value.ilu_tipvia + '</td>';
                tds += '<td>' + value.rele_tipo + '</td>';
                tds += '<td>' + value.Rele_nox + '</td>';
                tds += '<td>' + value.Rele_noy + '</td>';
                tds += '<td>' + value.red_tipseg + '</td>';
                tds += '<td>' + value.red_mat + '</td>';
                tds += '<td>' + value.red_estado + '</td>';
                tds += '<td>' + value.tiempo_op + '</td>';
                tds += '<td>' + value.tipo_red + '</td>';
                tds += '<td>' + value.observacio + '</td>';
                tds += '<td>' + value.estado + '</td>';
                tds += '</tr>';

/*
                tds += '<td><a href="#" class="lblLinkIpal" idIpal="' + value.idIpal + '">' + value.idIpal + '</a></td>';
                tds += '<td>' + value.Fecha + '</td>';
                tds += '<td>' + value.Empresa + '</td>';
                tds += '<td>' + value.Direccion + '</td>';
                tds += '<td>' + value.Celular + '</td>';
                tds += '<td>' + value.VehiculoTipo + '</td>';
                tds += '<td>' + value.VehiculoPlaca + '</td>';
                tds += '<td>' + value.GruaPlaca + '</td>';
                tds += '<td>' + value.CanastaPlaca + '</td>';
                tds += '<td>' + value.MotoPlaca + '</td>';
                tds += '<td>' + value.NoContrato + '</td>';
                tds += '<td>' + value.Trabajo + '</td>';
                tds += '<td>' + value.Proceso + '</td>';
                tds += '<td><a href="#" class="lblLinkUsuario" idLogin="' + value.idLogin + '">' + value.Usuario + '</a></td>'
                tds += '<td>' + value.Observaciones + '</td>';
                tds += '<td>' + value.Zona + '</td>';
                tds += '<td>' + value.Recibio + '</td>';
                tds += '<td>' + value.CargoEmpresa + '</td>';
                tds += '<td class="alert alert-block alert-'+ Estado +'">' + value.Resultado + '</td>';
                tds += '</tr>';*/

              });
            $("#tblAlumbradoCompleto tbody").append(tds);
            $("#tblAlumbradoCompleto").crearTabla1({lblMenu : "Alumbrado por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }

function btnAlumbradoDetalle_Archivos_Click()
{
  var pPrefijo = $(this).attr("Prefijo");
  var pConsecutivo = $(this).attr("Consecutivo");
  cargarArchivos("Alumbrado: " + pConsecutivo, "Alumbrado\/" + pPrefijo); 
}
