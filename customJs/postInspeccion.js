var ActividadesInterventoria = {};
function postInspeccion()
{
	$("#frmPostInspeccion").on("submit", frmPostInspeccion_Submit);

	//Ipal
	ActividadesInterventoria[1] = "<option value='12'>REALIZACIÓN DE INSPECCIÓN DE SEGURIDAD ( IPAL ) SITIO</option>";
	//Comercial
	ActividadesInterventoria[2] = "<option value='1'>INTERVENTORIA DE ACTIVIDAD PROCESO DE SCR FRIO</option>";
	ActividadesInterventoria[2] += "<option value='2'>INTERVENTORIA DE ACTIVIDAD PROCESO DE SCR SITIO</option>";
	ActividadesInterventoria[2] += "<option value='11'>INTERVENTORIA A LA ACTIVIDAD DE VCT POR TRANSFORMADOR FRIO</option>";
	ActividadesInterventoria[2] += "<option value='13'>INSPECCIONES OPERACIONALES (RECLAMACIÓN DE USUARIOS) EN FRIO</option>";
	ActividadesInterventoria[2] += "<option value='3'>INTERVENTORIA DE ACTIVIDAD DE INSPECCIONES CUADRILLA TIPO I FRIO</option>";
	ActividadesInterventoria[2] += "<option value='4'>INTERVENTORIA DE ACTIVIDAD DE INSPECCIONES CUADRILLA TIPO I SITIO</option>";
	ActividadesInterventoria[2] += "<option value='5'>INTERVENTORIA DE ACTIVIDAD DE INSPECCIONES II FRIO</option>";
	ActividadesInterventoria[2] += "<option value='6'>INTERVENTORIA DE ACTIVIDAD DE INSPECCIONES II SITIO</option>";
	ActividadesInterventoria[2] += "<option value='7'>INTERVENTORIA DE ACTIVIDAD DE INSPECCIONES III FRIO</option>";
	ActividadesInterventoria[2] += "<option value='8'>INTERVENTORIA DE ACTIVIDAD DE INSPECCIONES III SITIO</option>";
	//Alumbrado
	ActividadesInterventoria[3] = "<option value='18'>LEVANTAMIENTO DE LUMINARIA PÚBLICA CONCENTRADA SITIO</option>";
	ActividadesInterventoria[3] += "<option value='19'>LEVANTAMIENTO DE LUMINARIA PÚBLICA DISPERSA SITIO</option>";
	ActividadesInterventoria[3] += "<option value='20'>VERIFICACIÓN DE LUMINARIA PÚBLICA CONCENTRADA EN FRIO</option>";
	ActividadesInterventoria[3] += "<option value='21'>VERIFICACIÓN DE LUMINARIA PÚBLICA DISPERSA EN FRIO</option>";
	//Facturacion
	ActividadesInterventoria[4] = "<option value='9'>INTERVENTORIA A GRUPO DE FACTURACION FRIO</option>";
	ActividadesInterventoria[4] += "<option value='10'>INTERVENTORIA A GRUPO DE FACTURACION SITIO</option>";
	//Tecnica
	ActividadesInterventoria[6] = "<option value='22'>RED DE BAJA TENSIÓN - AEREO T0 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='23'>RED DE BAJA TENSIÓN - AEREO T1 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='24'>RED DE BAJA TENSIÓN - AEREO T2 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='25'>RED DE BAJA TENSIÓN - AEREO T3 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='26'>RED DE MEDIA TENSIÓN - AEREO T0 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='27'>RED DE MEDIA TENSIÓN - AEREO T1 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='28'>RED DE MEDIA TENSIÓN - AEREO T2 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='29'>RED DE MEDIA TENSIÓN - AEREO T3 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='30'>RED DE MEDIA TENSIÓN - SUBTERRÁNEOS T0 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='31'>RED DE MEDIA TENSIÓN - SUBTERRÁNEOS T1 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='32'>RED DE MEDIA TENSIÓN - SUBTERRÁNEOS T2 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='33'>RED DE MEDIA TENSÓN - SUBTERRÁNEOS T3 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='34'>VCR MASIVO RED AÉREA  T1 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='35'>VCR MASIVO RED AÉREA  T2 SITIO/EN FRIO</option>";
	ActividadesInterventoria[6] += "<option value='36'>VCR MASIVO RED AÉREA  T3 SITIO/EN FRIO</option>";

	ActividadesInterventoria[8] = "<option value='14'>INTERVENTORIA A OPERACIÓN FALLIDA EN FRIO</option>";
	ActividadesInterventoria[8] += "<option value='15'>VALOR ADICIONAL POR DESPLAZAMIENTO VEREDAL MUNICIPIOS DISPERSOS TODOS</option>";
	ActividadesInterventoria[8] += "<option value='16'>HORA DE ACTIVIDAD DE MONITOREO CON AYUDAS TECNOLÓGICAS SITIO</option>";
	ActividadesInterventoria[8] += "<option value='17'>ACTIVIDAD DE DESEMBARCO A CUADRILLA TODOS</option>";
	ActividadesInterventoria[8] += "<option value='37'>HORA CUADRILLA INTERVENTOR MOVILIZADO TODOS</option>";
	ActividadesInterventoria[8] += "<option value='38'>VIÁTICOS Y PERNOCTADAS TODOS</option>";
	ActividadesInterventoria[8] += "<option value='39'>HORA EXTRA TODOS</option>";


	$.each(ActividadesInterventoria, function(index, value)
		{
			if (index < 7)
			{
				$("#txtPostInspeccion_Actividad").append(value);
			}
		});

	$("#frmPostInspeccion .chosen").chosen();
	$(".radPostInspeccion").buttonset();

	$('#txtPostInspeccion_Hora').timepicker({
      minuteStep: 5,
      showSeconds: false,
      showMeridian: false,
      defaultTime:false,
      disableMousewheel : true,
      disableFocus: true
  });
	$("#txtPostInspeccion_Desplazamiento input").on("click", txtPostInspeccion_Desplazamiento_Click);
}


function txtPostInspeccion_Desplazamiento_Click()
{
	if ($("#radPostInspeccion_Desplazamiento_Si").is(':checked'))
	{
		$("#grpPostInspeccion_Hora").slideDown();
	} else
	{
		$("#grpPostInspeccion_Hora").slideUp();
		$("#txtPostInspeccion_Hora").val("");
	}
}
function frmPostInspeccion_Submit(evento)
{
	evento.preventDefault();

	var f = new Date();
  	if ($("#txtPostInspeccion_Sucursal").val() > 0)
  	{
	  	var pPrefijo = CompletarConCero(Usuario.id, 3) + f.getFullYear() + CompletarConCero(f.getMonth() +1, 2) + CompletarConCero(f.getDate(), 2) + CompletarConCero(f.getHours(), 2) + CompletarConCero(f.getMinutes(), 2) + CompletarConCero(f.getSeconds(), 2);
		$.post("php/crearInspeccion.php", 
		{
			idBaremo : $("#txtPostInspeccion_Actividad").val(),
			idLogin : Usuario.id,
			Prefijo : pPrefijo,
			Desplazamiento : $('input:radio[name=radPostInspeccion_Desplazamiento]:checked').val(),
			Tiempo : $("#txtPostInspeccion_Hora").val(),
			idMunicipio : $("#txtPostInspeccion_Municipio").val(),
			Veredal : $('input:radio[name=radPostInspeccion_Veredal]:checked').val(),
			Viaticos : $('input:radio[name=radPostInspeccion_Viaticos]:checked').val(),
			sucursal : $("txtPostInspeccion_Sucursal").val()
		}, function(data)
		{
			if (data.idInspeccion > 0)
			{
				abrirInspeccion(data.idInspeccionTipo);
				$("#modulo_postInspeccion").hide();
				$("#txtPrefijo").val(pPrefijo);
			} else
			{
				Mensaje("Error", data.Mensaje);
			}
		}, "json").fail(function()
		{
			Mensaje("Error", "No hay conexión con el Servidor");
		});
  	} else
  	{
  		Mensaje("Error", "Debe Seleccionar una sucursal");
  	}
}
function abrirInspeccion(idTipoInspeccion)
{
	tipoInspecciones = {};
	tipoInspecciones[1] = "ipal";
	tipoInspecciones[2] = "comercial";
	tipoInspecciones[3] = "alumbrado";
	tipoInspecciones[4] = "facturacion";

	$(this).cargarModulo({pagina : tipoInspecciones[idTipoInspeccion], titulo : "Formulario de " + tipoInspecciones[idTipoInspeccion], icono : "icon-edit"});

}