function reporteComercial()
{
  $(".lblLinkComercial").live("click", lblLinkComercial_Click);
  $(".lblEditarComercial").live("click", lblEditarComercial_Click);
  $(".lblEliminarComercial").live("click", lblEliminarComercial_Click);
  
  $("#btnComercialDetalle_Archivos").live("click", btnComercialDetalle_Archivos_Click);
  $("#frmReporteComercial").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteComercial_CargarComercial();
    });
}
function reporteComercialCompleto()
{
  $(".lblLinkComercial").unbind("click");
  $(".lblLinkComercial").live("click", lblLinkComercial_Click);

  $("#btnComercialDetalle_Archivos").unbind("click");
  $("#btnComercialDetalle_Archivos").live("click", btnComercialDetalle_Archivos_Click);
  $("#frmReporteComercialCompleto").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteComercial_CargarComercialCompleto();
    });
}
 function lblLinkComercial_Click(evento)
 {
  evento.preventDefault();
    var pIdComercial = $(this).attr("idComercial");
    $(this).cargarModulo({pagina: "reporteComercialDetalle", titulo : "Detalles de Comercial " + $(this).text(), icono : "icon-eye-open"},
      function()
      {
        $.post("php/comercial/cargarDetalle.php", {idComercial : pIdComercial },
          function(data)
          {
            $("#btnComercialDetalle_Archivos").attr("Prefijo", data.Prefijo);
            $("#btnComercialDetalle_Archivos").attr("Consecutivo", data.idComercial);
            
            $("#txtComercialDetalle_Resultado").attr("src", "img/" + data.Resultado + ".png")

            $("#txtComercialDetalle_Consecutivo").text(data.Consecutivo);
            $("#txtComercialDetalle_idComercial").text(data.idComercial);
            $("#txtComercialDetalle_Fecha").text(data.Fecha);
            $("#txtComercialDetalle_Usuario").html('<a href="#" class="lblLinkUsuario" idLogin="' + data.idLogin + '">' + data.Usuario + '</a>');
            $("#txtComercialDetalle_Atendio").text(data.Atendio);
            $("#txtComercialDetalle_Direccion").text(data.Direccion);
            $("#txtComercialDetalle_Telefono").text(data.Telefono);
            $("#txtComercialDetalle_OT").text(data.OT);
            $("#txtComercialDetalle_TipoInterventoria").text(data.TipoInterventoria);
            $("#txtComercialDetalle_SubProceso").text(data.SubProceso);
            $("#txtComercialDetalle_Medida").text(data.Medida);
            $("#txtComercialDetalle_Se").text(data.SE);
            $("#txtComercialDetalle_Medida2").text(data.Medida2);
            $("#txtComercialDetalle_ActividadEconomica").text(data.ActividadEconomica);
            $("#txtComercialDetalle_Factor").text(data.Factor);
            $("#txtComercialDetalle_MedActiva").text(data.MedActiva);
            $("#txtComercialDetalle_LecActiva").text(data.LecActiva);
            $("#txtComercialDetalle_TipoFase").text(data.TipoFase);
            $("#txtComercialDetalle_Marca").text(data.Marca);
            $("#txtComercialDetalle_Red").text(data.Red);
            $("#txtComercialDetalle_AccesoMedidor").text(data.AccesoMedidor);
            $("#txtComercialDetalle_TipoAcometida").text(data.TipoAcometida);
            $("#txtComercialDetalle_AcometidaCompartida").text(data.AcometidaCompartida);
            $("#txtComercialDetalle_LocalizacionMedidor").text(data.LocalizacionMedidor);
            $("#txtComercialDetalle_MedidaReferencia").text(data.MedidaReferencia);
            $("#txtComercialDetalle_Sellos").text(data.Sellos);
            $("#txtComercialDetalle_CapaTrafo").text(data.CapaTrafo);
            $("#txtComercialDetalle_CD").text(data.CD);
            $("#txtComercialDetalle_PuntoFisico").text(data.PuntoFisico);

          }, 'json').always(function() 
        {
          //Cuando Finaliza
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
      })
 }
 function reporteComercial_CargarComercial()
 {
  $(".icoCargando").show();
  $.post("php/comercial/cargarComercial.php", {Desde: $("#txtReporteComercial_Desde").val(), Hasta : $("#txtReporteComercial_Hasta").val()}, 
        function(data)
        {
            $("#tblComercial").dataTable().fnDestroy();
            $("#tblComercial tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";

            $.each(data, function(index, value)
              {
                var Estado = "success";
                if (value.Resultado != "Cumple")
                {
                  Estado = "error";
                } 
                tds += '<tr>';
                tds += '<td></td>';
                tds += '<td><a href="#" class="lblLinkComercial" idComercial="' + value.idComercial + '">' + value.Consecutivo + '</a></td>';
                tds += '<td>' + value.Fecha + '</td>';
                tds += '<td>' + value.HoraInicio + '</td>';
                tds += '<td>' + value.OT + '</td>';
                tds += '<td>' + value.Direccion + '</td>';
                tds += '<td>' + value.TipoInterventoria + '</td>';
                tds += '<td>' + value.SubProceso + '</td>';
                tds += '<td><a href="#" class="lblLinkUsuario" idLogin="' + value.idLogin + '">' + value.Usuario + '</a></td>'
                tds += '<td><a class="lblEditarComercial" Prefijo="' + value.Prefijo + '"><i class="icon-edit"></i> Editar </a></td>';
                tds += '<td><a class="lblEliminarComercial" Prefijo="' + value.Prefijo + '"><i class="icon-trash"></i> Eliminar </a></td>';
                tds += '</tr>';

              });
            $("#tblComercial tbody").append(tds);
            $("#tblComercial").crearTabla1({lblMenu : "Ordenes por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }

function btnComercialDetalle_Archivos_Click()
{
  var pPrefijo = $(this).attr("Prefijo");
  var pConsecutivo = $(this).attr("Consecutivo");
  cargarArchivos("Comercial: " + pConsecutivo, "Comercial\/" + pPrefijo); 
}
function reporteComercial_CargarComercialCompleto()
 {
  $(".icoCargando").show();
  $.post("php/comercial/cargarComercialCompleto.php", {Desde: $("#txtReporteComercialCompleto_Desde").val(), Hasta : $("#txtReporteComercialCompleto_Hasta").val()}, 
        function(data)
        {
            $("#tblComercialCompleto").dataTable().fnDestroy();
            $("#tblComercialCompleto tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";

            $.each(data, function(index, value)
              {
                var Estado = "success";
                if (value.Resultado != "Cumple")
                {
                  Estado = "error";
                } 
                tds += '<tr>';
                tds += '<td><a href="#" class="lblLinkComercial" idComercial="' + value.idComercial + '">' + value.idInspeccion + '</a></td>';
                tds += '<td>' + value.Prefijo + '</td>';
                tds += '<td>' + value.idInspeccion + '</td>';
                tds += '<td>' + value.OT + '</td>';
                tds += '<td>' + value.FechaIngreso + '</td>';
                tds += '<td>' + value.HoraInicio + '</td>';
                tds += '<td>' + value.Direccion + '</td>';
                tds += '<td>' + value.Atendio + '</td>';
                tds += '<td>' + value.Telefono + '</td>';
                tds += '<td>' + value.TipoInterventoria + '</td>';
                tds += '<td>' + value.SubProceso + '</td>';
                tds += '<td>' + value.Medida + '</td>';
                tds += '<td>' + value.SE + '</td>';
                tds += '<td>' + value.Medida2 + '</td>';
                tds += '<td>' + value.ActividadEconomica + '</td>';
                tds += '<td>' + value.Factor + '</td>';
                tds += '<td>' + value.MedActiva + '</td>';
                tds += '<td>' + value.LecActiva + '</td>';
                tds += '<td>' + value.TipoFase + '</td>';
                tds += '<td>' + value.Marca + '</td>';
                tds += '<td>' + value.Red + '</td>';
                tds += '<td>' + value.AccesoMedidor + '</td>';
                tds += '<td>' + value.TipoAcometida + '</td>';
                tds += '<td>' + value.AcometidaCompartida + '</td>';
                tds += '<td>' + value.LocalizacionMedidor + '</td>';
                tds += '<td>' + value.MedidaReferencia + '</td>';
                tds += '<td>' + value.Sellos + '</td>';
                tds += '<td>' + value.CapaTrafo + '</td>';
                tds += '<td>' + value.CD + '</td>';
                tds += '<td>' + value.PuntoFisico + '</td>';
                tds += '<td>' + value.Observaciones + '</td>';
                tds += '<td>' + value.FormatoSupervision + '</td>';
                tds += '<td>' + value.Cuadrillas + '</td>';
                tds += '<td>' + value.InspeccionesRealizadas + '</td>';
                tds += '<td>' + value.PlanSupervision + '</td>';
                tds += '<td>' + value.ObservacionesCliente + '</td>';
                tds += '<td>' + value.ObservacionesPredio + '</td>';
                tds += '<td>' + value.Cumple + '</td>';
                tds += '<td>' + value.CodIncumplimiento + '</td>';
                tds += '<td>' + value.FRIO + '</td>';
                tds += '<td>' + value.Nombre + '</td>';
                tds += '</tr>';

              });
            $("#tblComercialCompleto tbody").append(tds);
            $("#tblComercialCompleto").crearTabla1({lblMenu : "Ordenes por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }
 function lblEditarComercial_Click(evento)
 {
  evento.preventDefault();
  var pPrefijo = $(this).attr("Prefijo");
  $("#txtPrefijo").val(pPrefijo);
  $(this).cargarModulo({pagina: "editarComercial", titulo : "Editar Inspeccion " + $(this).text(), icono : "icon-edit"});
 }

 function lblEliminarComercial_Click(evento)
 {
  evento.preventDefault();
  var pPrefijo = $(this).attr("Prefijo");
  var obFila = $(this).parent("td").parent("tr");
  $.post("php/comercial/eliminarComercial.php", {idLogin : Usuario.id, Prefijo : pPrefijo}, function(data)
    {
      if (data == 1)
      {
        Mensaje("Ok", "Inspección Borrada");
        $(obFila).remove();

      } else
      {
        Mensaje("Error", data);
      }
    });
 }