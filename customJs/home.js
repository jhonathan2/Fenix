var Usuario = null;

$(document).on("ready", appReady);

function appReady()
{

  $("#btnCoordenadas").on("click", function()
    {
      ObtenerCoordenadas("txtCoordenadas", false);
    });
  Usuario = JSON.parse(localStorage.getItem('hesPu'));  
  cagarBarraSuperior(Usuario);

  $("#btnEnviarCorreo").live('click', btnEnviarCorreo_Click);

  $("#tablaMensajes tr").live('click', verCorreo);

  //$("#modulo").load("Inicio.html");
  $(this).cargarModulo({pagina : "Inicio", titulo : "Inicio", icono : "icon-dashboard"}, cargarInicio);

  //setInterval(hacerPush, 120000);

  $(".lblLinkUsuario").live("click", cargarPerfilUsuario);
  $(".lblMaterialNombre").live("click", cargarDetalleMaterial);
  
  $(".cargarArchivos_Usuario").live("click", cargarArchivos_Usuario_Click);
  $(".cargarMateriales_Usuario").live("click", cargarMateriales_Usuario_Click);
  $("#mnuVerMateriales").live("click", mnuVerMateriales_Click);

  $(".cargarIpal").live("click", cargarItemsIpal);
  $(".chkIpalEvaluado").live("change", cambiarEstadoIpal_Item);
  
  //$("#tituloDelModulo h4 span").text("Inicio");
}

function cagarBarraSuperior(pUsuario)
{
  $("#header").load("header.html", function()
    {
      $(".username").text(pUsuario.nombre);
      $("#lblHeaderUsuario a").attr("idLogin", pUsuario.id);
      $("#FotoPerfil").attr("src", pUsuario.Foto);
    });
  $("#sideMenu").load("menu.html", function()
    {
      App.init();
      $("#sideMenu li").on("click", sideMenu);
    });

  hacerPush();

}

$.fn.cargarCorreos = function(options, callback)
      {
        var defaults =
        {
          idUsuario : "0",
          Estado: 'Pendiente',
          Tipo: ''
        }
        var options = $.extend(defaults, options);
        if (callback === undefined)
        {callback = function(){};}

      /*plugin*/
      
        $.post("php/cargarCorreos.php", 
          {pUsuario : options.idUsuario, pEstado: options.Estado, pTipo : options.Tipo},
          function(data, textStatus, xhr)
          {
            callback(data);
          },"json");
      /*plugin*/

      //Averigua si el parámetro contiene una función de ser así llamarla
        if($.isFunction(options.onComplete)) 
        {options.onComplete.call();}
      };
function sideMenu(event)
{
  if (!($(this).hasClass('has-sub')))
  {
    if ($(this).text() == "Inicio")
    {
      $(this).cargarModulo({pagina : $(this).attr("pagina"), titulo : $(this).text(), icono : $(this).attr("icono")}, cargarInicio);  
    } else
    {
      $(this).cargarModulo({pagina : $(this).attr("pagina"), titulo : $(this).text(), icono : $(this).attr("icono")});
    }
    
    
  }  
}
function hacerPush (argument) 
{
  comprobarActualizacion();
  ObtenerCoordenadas("txtCoordenadas", false);
  $("#desplegableCorreos").cargarCorreos({idUsuario: Usuario.id, Estado: 'Pendiente', Tipo : 'Recibidos'}, 
    function(Correos)
    {
      $("#desplegableCorreos li").remove();
      $(".lblContadorMensajes").hide();
      var s_ = "";

      var tds = "<li><p>Tienes <span class='lblContadorMensajes'>0</span> nuevo"+ s_ + " mensaje" + s_ +"</p></li>";
      $.each(Correos, function(index, value)
        {
          if (Correos.length > 1)
          { s_ = "s"; }

          if (index < 5)
          {
            tds += "<li><a href='#'>";
            //tds += '<span class="photo"><i class="icon-envelope"></i></span>';
            //tds += '<span class="photo"><img src="./img/avatar-mini.png" alt="avatar"></span>';
            
            tds += '<span class="subject">';
            tds += '  <span class="from">' + value.nombreRemitente + '</span><br />';

            tds += '  <span class="time">' + value.Fecha + '</span><br />';
            tds += '</span>';
            tds += '<span class="message">' + value.Asunto + '</span>';
            tds += '</a></li>';
          }
        });
      //tds += "<li><a href='inbox.html'>Ver todos los mensajes</a></li>";
      $("#desplegableCorreos").append(tds);
      if (Correos.length > 0)
      {
        $(".lblContadorMensajes").text(Correos.length);  
        $(".lblContadorMensajes").show();
      }
      
    });
}

$.fn.crearTabla1 = function(options, callback)
{
  var idTabla = $(this).attr("id");
  var defaults =
  {
    lblMenu : "Registros por página"
  }
  var options = $.extend(defaults, options);
  if (callback === undefined)
  {callback = function(){};}

/*plugin*/
  
  $('#' + idTabla).dataTable().fnDestroy();
//"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",  
  tabla = $('#' + idTabla).dataTable({
            "sDom": 'CTW<"clear">lfrtip',
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ " + options.lblMenu + "",
                "oPaginate": {
                    "sPrevious": "Ant",
                    "sNext": "Sig"
                }
            },
            "oTableTools": 
            {
          "sSwfPath": "assets/data-tables/media/swf/copy_csv_xls_pdf.swf",
          "aButtons": [
              {
                "sExtends": "copy",
                "sButtonText": "Copiar"
              },
              {
                "sExtends": "csv",
                "sButtonText": "CSV"
              },
              {
                "sExtends": "xls",
                "sButtonText": "Excel"
              }
            ]
            }
        });

        jQuery('#' + idTabla + '_wrapper .DTTT').addClass("span6 offset10"); // modify table search input
        jQuery('#' + idTabla + '_wrapper .dataTables_filter input').addClass("input-medium"); // modify table search input
        jQuery('#' + options.idTabla + '_wrapper .dataTables_length select').addClass("input-mini"); // modify table per page dropdown
/*plugin*/

//Averigua si el parámetro contiene una función de ser así llamarla
  if($.isFunction(options.onComplete)) 
  {options.onComplete.call();}
};
function btnEnviarCorreo_Click(event) 
{
  event.preventDefault();
}
function verCorreo()
{
  var pIdMensaje = $(this).attr('idMensaje');
  $("#txtIdMensaje").val(pIdMensaje);
}
function Mensaje(Titulo, Mensaje)
{
  $.gritter.add({
        title: Titulo,
        text: Mensaje
      });
}
$.fn.cargarModulo = function(options, callback)
{
  var defaults =
  {
    pagina : "404",
    titulo : "Funcion no Encontrada",
    icono : "icon-ban-circle"
  };
  var options = $.extend(defaults, options);
  if (callback === undefined)
  {callback = function(){};}

/*plugin*/
    var nonModulo = options.pagina;
  nomModulo = options.pagina;
  nomModulo = "modulo_" + nomModulo.replace(/\s/g, "_");
  nomModulo = nomModulo.replace(/\./g, "_");
  nomModulo = nomModulo.replace(/\//g, "_");
  var tds = "";
  $(".widget-body").slideUp();
  $(".icon-chevron-up").removeClass("icon-chevron-up").addClass("icon-chevron-down");

  if ($("#main_menu_trigger").is(":visible"))
  {
    $("#sideMenu").slideUp();
  } 

  if ($('#' + nomModulo).length)
  {
     $('#' + nomModulo).show('slide');
     $("#" + nomModulo + " .widget-body").slideDown();
     $("#" + nomModulo + " .icon-chevron-down").removeClass("icon-chevron-down").addClass("icon-chevron-up");
     $("#" + nomModulo + " .widget-title h4 span").text(options.titulo);
     callback();
  } else
  {
    tds += '<div id="' + nomModulo + '" class="">';
    tds += '  <div class="widget">';
    tds += '        <div class="widget-title">';
    tds += '           <h4><i class="' + options.icono + '"></i><span>Cargando...</span></h4>';
    tds += '           <span class="tools">';
    tds += '           <a href="javascript:;" class="icon-chevron-up btnMinimizar"></a>';
    tds += '           <a class="icon-remove btnCerrar" style="font-size:2em;" href="javascript:;"></a>';
    tds += '           </span>';
    tds += '        </div>';
    tds += '        <div class="widget-body">';
    tds += '        </div>';
    tds += '  </div>';
    tds += '</div>';
    
    $("#contenedorModulos").append(tds);  
     var n = options.pagina.search(".html");
     if (n < 1)
     {
        options.pagina = options.pagina + ".html";
     }
    $.post("cargarFrame.php", {archivo: options.pagina}, 
      function(data)
      {
        $("#" + nomModulo + " .widget-body").html(data);  
        
        //$("#" + nomModulo + " .widget-body").html("<h1>Listo</h1>");
      }, "html").always(function()
      {
          $("#" + nomModulo + " .widget-title h4 span").text(options.titulo);
          /*$("#" + nomModulo + " .widget-title h4 i").removeClass('*');
          $("#" + nomModulo + " .widget-title h4 i").addClass(icono);        
          */
          minimizarPorlet(nomModulo);
          cerrarPorlet(nomModulo);
          callback();
      });
  }

/*plugin*/

//Averigua si el parámetro contiene una función de ser así llamarla
  if($.isFunction(options.onComplete)) 
  {options.onComplete.call();}
};

function minimizarPorlet(id)
{
  //jQuery('#' + id + ' .widget .tools .icon-chevron-down, #' + id + '.widget .tools .icon-chevron-up').on("click", function () {
  /*jQuery('#' + id + ' .btnMinimizar').on("click", function () {
            var el = $("#" + id + " .widget-body");
            var objTitulo = $("#" + id + " .btnMinimizar");
            if (jQuery(objTitulo).hasClass("icon-chevron-down")) {
                jQuery(objTitulo).removeClass("icon-chevron-down").addClass("icon-chevron-up");
                el.slideDown(200);
            } else {
                jQuery(objTitulo).removeClass("icon-chevron-up").addClass("icon-chevron-down");
                el.slideUp(200);
            }
        });*/
  $("#" + id + " .widget-title").on("click", function(event) 
  {
      var el = $("#" + id + " .widget-body");
            var objTitulo = $("#" + id + " .btnMinimizar");
            if (jQuery(objTitulo).hasClass("icon-chevron-down")) {
                jQuery(objTitulo).removeClass("icon-chevron-down").addClass("icon-chevron-up");
                el.slideDown(200);
            } else {
                jQuery(objTitulo).removeClass("icon-chevron-up").addClass("icon-chevron-down");
                el.slideUp(200);
            }
  });
}
function cerrarPorlet(id)
{
  jQuery('#' + id + ' .btnCerrar').on("click", function () 
    {
        jQuery('#' + id).hide('slide');
    });
}
function cargarInicio()
{
  $.post('php/cargarEstadisticas_Inicio.php', {idLogin : Usuario.id}, function(data, textStatus, xhr) 
            {
              $("#txtEstadisticas_Fecha span").text(data.fecha);
                $("#txtEstadisticas").slideDown();
                if (data.total == 0)
                {
                  $(".circle-stat").slideUp();
                  $("#divJhonathan").slideUp();
                  $("#txtEstadisticas h4").text("No hay actividades programadas.");

                } else
                {
                  $(".circle-stat").slideDown();
                  $("#divJhonathan").slideDown();
                  $("#txtEstadisticas h4").text("");
                }
                
                var totalRealizadas = parseInt(data.comercialRealizadas) + parseInt(data.alumbradoRealizadas) ;
                $("#txtThick_Usuarios").val(((totalRealizadas * 100)/data.total).toFixed(2));
                $("#txtThick_Empresas").val(((data.alumbradoRealizadas * 100)/data.alumbrado).toFixed(2));
                $("#txtThick_Pruebas").val(0);//data.comercial);
                $("#txtThick_Contratos").val(0);
                $("#txtThick_Ipales").val(0);

                $("#txtNum_Usuarios").text("+" + totalRealizadas + " de " + data.total + " " + ((totalRealizadas * 100)/data.total).toFixed(2) + "%");
                if (data.alumbrado > 0)
                {
                  $("#txtNum_Empresas").text("+" + data.alumbradoRealizadas + " de " + data.alumbrado + " " + ((data.alumbradoRealizadas * 100)/data.alumbrado).toFixed(2) + "%");  
                } else
                {
                  $("#txtNum_Empresas").text("+" + data.alumbradoRealizadas  + " de 0");  
                }
                if (data.comercial > 0)
                {
                  $("#txtNum_Pruebas").text("+" + data.comercialRealizadas + " de " + data.comercial + " " + ((data.comercialRealizadas * 100)/data.comercial).toFixed(2) + "%");
                } else
                {
                  $("#txtNum_Pruebas").text("+1 de 0");    
                }
                if (data.tecnica > 0)
                {
                  $("#txtNum_Contratos").text("+0 de " + data.tecnica + " 0%");  
                } else
                {
                  $("#txtNum_Contratos").text("+0 de 0");  
                }
                
                $("#txtNum_Ipales").text("+" + data.ipales);
                

            }, 'json').always(function() 
              {
                $(".knob").knob();
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No hay conexión con el servidor");
                $("#txtEstadisticas").slideUp();
                $("#txtError").slideDown();
              });
}

HTTP_GET_VARS=new Array();
strGET=document.location.search.substr(1,document.location.search.length);
if(strGET!='')
    {
    gArr=strGET.split('&');
    for(i=0;i<gArr.length;++i)
        {
        v='';vArr=gArr[i].split('=');
        if(vArr.length>1){v=vArr[1];}
        HTTP_GET_VARS[unescape(vArr[0])]=unescape(v);
        }
    }

function GET(v) {
  if(!HTTP_GET_VARS[v]){return 'undefined';}
  return HTTP_GET_VARS[v];
}

function cargarPerfilUsuario()
{
  var pIdLogin = $(this).attr("idLogin");
  var pTexto = $(this).text();;
 $(this).cargarModulo({pagina : "profile", titulo : "Perfil de " + pTexto, icono : "icon-user"}, function()
 {
    cargarProfile(pIdLogin);
 }); 
}
function cargarArchivos(titulo, idCarpeta)
{
  $(this).cargarModulo({pagina : "verArchivos", titulo : "Archivos de " + titulo, icono : "icon-copy"}, function()
      {
        if($('#verArchivos').elfinder('instance'))
        { $('#verArchivos').elfinder('destroy');}
       
        var elf = $('#verArchivos').elfinder({
              url : 'Archivos/php/connector.php?Contrato=' + idCarpeta,
              lang: 'es',  
              handlers:
              {
                upload : function(event) 
                    { 
                      
                    },
                open: function(event)
                    { 
                      
                    },
                rm : function(event)
                    {
                      
                    }
              }           
        }).elfinder('instance');
      });
}
function cargarArchivos_Usuario_Click()
{
  cargarArchivos($(this).attr("Nombre"), "Usuarios\/" + $(this).attr("idLogin"));
}
function cargarDetalleMaterial()
{
  var pIdMaterial = $(this).attr("idMaterial");
  var pTexto = $(this).text();
 $(this).cargarModulo({pagina : "materialesDetalle", titulo : "Detalles de " + pTexto, icono : "icon-shopping-cart"}, function()
 {
    $.post("php/cargarDetalleMaterial.php", {idMaterial : pIdMaterial}, function(data)
      {
        $("#txtMaterialesDetalle_Imagen").attr("src", data.Foto)
        $("#txtMaterialesDetalle_Id").text(data.idMaterial);
        $("#txtMaterialesDetalle_Nombre").text(data.Nombre);
        $("#txtMaterialesDetalle_Marca").text(data.Marca);
        $("#txtMaterialesDetalle_Referencia").text(data.Referencia);
        $("#txtMaterialesDetalle_FechaCompra").text(data.FechaCompra);
        $("#txtMaterialesDetalle_FechaAsignacion").text(data.FechaAsignacion);
        $("#txtMaterialesDetalle_Usuario").html('<a href="#" class="lblLinkUsuario" idLogin="' + data.idLogin + '">' + data.Usuario + '</a>');
        $("#txtMaterialesDetalle_Valor").text(data.Valor);
        $("#txtMaterialesDetalle_Descripcion").text(data.Descripcion);
        $("#txtMaterialesDetalle_Accesorios").text(data.Accesorios);
        $("#txtMaterialesDetalle_Observaciones").text(data.Observaciones);
        $("#txtMaterialesDetalle_Estado").text(data.Estado);
        if (data.Estado == "Asignado")
        {
          if ($("#frmVerMateriales").attr("Habilitado") == 1)
          {
            $("#frmEntregarMaterial").show();
            $("#frmEntregarMaterial").attr("idMaterial", data.idMaterial);
            $("#frmMaterialesDetalle_Entrega").slideUp();
          } else
          {
            $("#frmEntregarMaterial").hide();
          }

          $("#txtMaterialesDetalle_Estado").removeClass("label-important");
          $("#txtMaterialesDetalle_Estado").addClass("label-success");
        } else
        {
          $("#frmEntregarMaterial").hide();

          $("#frmMaterialesDetalle_Entrega").slideDown();
          $("#txtMaterialesDetalle_ImagenEntrega").attr("src", data.imagenRecibe);
          $("#txtMaterialesDetalle_Recibio").text(data.Recibio);
          $("#txtMaterialesDetalle_Recibio").html('<a href="#" class="lblLinkUsuario" idLogin="' + data.idLoginRecibe + '">' + data.Recibio + '</a>');
          $("#txtMaterialesDetalle_FechaEntrega").text(data.fechaEntrega);
          $("#txtMaterialesDetalle_DetalleEntrega").text(data.detalleEntrega);

          $("#txtMaterialesDetalle_Estado").removeClass("label-success");
          $("#txtMaterialesDetalle_Estado").addClass("label-important");
        }
      }, "json");
 }); 
}function cargarMateriales_Usuario_Click()
{
  var nomUsuario = $(this).attr("Usuario");
  $(this).cargarModulo({pagina : "verMateriales", titulo : "Materiales de " + nomUsuario, icono : "icon-search"}, function()
    {
      $("#txtVerMateriales_Parametros").val("DatosUsuarios.Nombre");
      $("#txtVerMateriales_Criterio").val(nomUsuario);
      btnVerMateriales_Buscar_Click();
      $("#frmVerMateriales").slideUp();
      $("#frmVerMateriales").attr("Habilitado", 0);
      $("#txtVerMateriales_Parametros").val("");
      $("#txtVerMateriales_Criterio").val("");


    });
}
function mnuVerMateriales_Click()
{
  $("#frmVerMateriales").slideDown();
  $("#frmVerMateriales").attr("Habilitado", 1);
}
function CompletarConCero(n, length)
{
   n = n.toString();
   while(n.length < length) n = "0" + n;
   return n;
}
function fechasRango(desde, hasta)
{
  $( desde ).datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2,
          onClose: function( selectedDate ) {
            $( hasta ).datepicker( "option", "minDate", selectedDate );
          }
        });
        $( hasta ).datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 2,
          onClose: function( selectedDate ) {
            $( desde ).datepicker( "option", "maxDate", selectedDate );
          }
        });
}
function checkValor(idObjeto)
{
  return $("#"+ idObjeto).is(":checked");
}
function ObtenerCoordenadas(idControl, pMensaje)
{
  var objCoordenadas ="";
  $("#" + idControl).val(objCoordenadas); 
  navigator.geolocation.getCurrentPosition(
    function(datos)
    {
      var lat = datos.coords.latitude;
      var lon = datos.coords.longitude;
      var accu = datos.coords.accuracy;

      objCoordenadas =  lat + "," + lon + "#" + accu;
        if (pMensaje != false)
        {
            Mensaje("Ubicación", objCoordenadas);
        }
      $("#" + idControl).val(objCoordenadas);
    }, 
    function ()
    {
      objCoordenadas ="No hay precision en el dato";
      if (pMensaje != false)
      {
        Mensaje("Ubicación", objCoordenadas);
      }
    });
  return objCoordenadas;
}
function comprobarActualizacion()
{
  $.post("php/actualizacion.php", {version : 1, idLogin : Usuario.id}, function(data)
    {
      if (data != "")
      {
        Mensaje("Hey", data);
      }
    });
}

function cargarConvenciones(datos2, idContenedor, idGrafica)
{
  var tmpObj = $("#" + idGrafica + " .flotr-legend-color-box");
        $("#" + idContenedor + " article").remove();
        $.each(tmpObj, function(index, value)
        {
          var objHtml = $(value).html();
                  
          var strObj = objHtml.substring(parseInt(objHtml.indexOf("(") + 1 ), parseInt(objHtml.indexOf(")")) );
          var strObj_ = strObj.split(",");
          $("#" + idContenedor + "").append("<article><div style='width:1em; height:1em;background:#"+ rgbToHex(strObj_[0], strObj_[1], strObj_[2]) + ";'></div><span> " + datos2[index].label+"</span></article>");

        });
        $(".flotr-legend").hide();
}
function cargarGraficaBarras(datos2, titulo, idContenedor)
{
  var Contenedor = document.getElementById(idContenedor);
  graph = Flotr.draw(Contenedor,
          datos2
          , {
            title: titulo,
              bars : {
                show : true,
                horizontal : false,
                shadowSize : 5,
                barWidth: 1
              },
              legend: 
              {
                backgroundOpacity: 0,
                position: 'ne'
              },
              grid : 
              {
                verticalLines : false,
                horizontalLines : true
              },
              mouse:
              {
                relative: true,
                track: true,
                tackAll: true,
                trackDecimals: 0,
                trackFormatter: function (x) 
                {
                    return x.series.label +": " + x.series.data[0][1];
                  }
              },
               xaxis: 
              {   autoscale: true,
                showLabels: false   },
              yaxis:
              {   
                autoscale:true,
                autoscaleMargin : 1,
                min: 0,
                showLabels: true,
                tickDecimals: 0
              }
            });
}


function cargarGraficaBarras2(datos2, titulo, idContenedor)
{
  var Contenedor = document.getElementById(idContenedor);
  graph = Flotr.draw(Contenedor,
          datos2
          , {
            title: titulo,
              bars : {
                show : true,
                horizontal : false,
                shadowSize : 5,
                barWidth: 1
              },
              legend: 
              {
                backgroundOpacity: 0,
                position: 'ne'
              },
              grid : 
              {
                verticalLines : false,
                horizontalLines : true
              },
              mouse:
              {
                relative: true,
                track: true,
                tackAll: true,
                trackDecimals: 0,
                trackFormatter: function (x) 
                {
                    return x.series.label +": " + x.series.data[0][1];
                  }
              },
               xaxis: 
              {   autoscale: true,
                showLabels: false   },
              yaxis:
              {   
                autoscale:true,
                autoscaleMargin : 1,
                min: 0,
                showLabels: true,
                tickDecimals: 0
              }
            });
}

function rgbToHex(R,G,B) 
{
  return toHex(R)+toHex(G)+toHex(B);
}
function toHex(n) 
{
   n = parseInt(n,10);
   if (isNaN(n)) return "00";
   n = Math.max(0,Math.min(n,255));
   return "0123456789ABCDEF".charAt((n-n%16)/16) + "0123456789ABCDEF".charAt(n%16);
}