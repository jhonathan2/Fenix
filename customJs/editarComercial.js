function editarComercial()
{
	$.post("php/comercial/cargarDetalle.php", {idComercial : $("#txtPrefijo").val()}, 
		function(data)
		{
			if (data.PlanSupervision == "on")
			{
				data.PlanSupervision = "true";
			} else
			{
				data.PlanSupervision = "false";
			}

			$("#txtEditarComercial_idContrato").val(data.Consecutivo);
			$("#txtEditarComercial_Nombre").val(data.OT);
			$("#txtEditarComercial_FechaInicio").val(data.Fecha);
			$("#txtEditarComercial_HoraIni").val(data.HoraInicio);
			$("#txtEditarComercial_Direccion").val(data.Direccion);
			$("#txtEditarComercial_Atendio").val(data.Atendio);
			$("#txtEditarComercial_Telefono").val(data.Telefono);
			$("#txtEditarComercial_TipoInterventoria").val(data.TipoInterventoria);
			$("#txtEditarComercial_SubProceso").val(data.SubProceso);
			
			$("#txtEditarComercial_CruadrillaTipo").val(data.CruadrillaTipo);
			$("#txtEditarComercial_CuadrillaNum").val(data.CuadrillaNum);
			$("#txtEditarComercial_CuadrillaSupervisor").val(data.CuadrillaSupervisor);
			$("#txtEditarComercial_CedulaSupervisor").val(data.CedulaSupervisor);
			$("#txtEditarComercial_SupervisorCelular").val(data.SupervisorCelular);
			$("#txtEditarComercial_CuadrillaAux1").val(data.CuadrillaAux1);
			$("#txtEditarComercial_CuadrillaAux2").val(data.CuadrillaAux2);
			$("#txtEditarComercial_CuadrillaTec1").val(data.CuadrillaTec1);
			$("#txtEditarComercial_CuadrillaCelulares").val(data.CuadrillaCelulares);
			$("#txtEditarComercial_CuadrillaCedulas").val(data.CuadrillaCedulas);
			$("#txtEditarComercial_NoOrden").val(data.NoOrden);
			$("#txtEditarComercial_DetalleActividad").val(data.DetalleActividad);
			
			$("#txtEditarComercial_Medida").val(data.Medida);
			$("#txtEditarComercial_Se").val(data.SE);
			$("#txtEditarComercial_Medida2").val(data.Medida2);
			$("#txtEditarComercial_ActividadEconomica").val(data.ActividadEconomica);
			$("#txtEditarComercial_Factor").val(data.Factor);
			$("#txtEditarComercial_MedActiva").val(data.MedActiva);
			$("#txtEditarComercial_LecReactiva").val(data.LecActiva);
			$("#txtEditarComercial_TipoFase").val(data.TipoFase);
			$("#txtEditarComercial_Marca").val(data.Marca);
			$("#txtEditarComercial_Red").val(data.Red);
			if (data.AccesoMedidor == "true")
			{
				$("#txtEditarComercial_AccesoMedido").attr("checked", data.AccesoMedidor);
			}
			$("#txtEditarComercial_TipoAcometida").val(data.TipoAcometida);
			if (data.AcometidaCompartida == "true")
			{
				$("#txtEditarComercial_AcometidaCompartida").attr("checked", data.AcometidaCompartida);
			}
			$("#txtEditarComercial_LocalizacionMedidor").val(data.LocalizacionMedidor);
			$("#txtEditarComercial_MedidaReferencia").val(data.MedidaReferencia);
			$("#txtEditarComercial_Sellos").val(data.Sellos);
			$("#txtEditarComercial_CapaTrafo").val(data.CapaTrafo);
			$("#txtEditarComercial_CD").val(data.CD);
			$("#txtEditarComercial_PuntoFisico").val(data.PuntoFisico);
			
			if (data.FormatoSupervision == "true")
			{
				$("#txtEditarComercial_FormatoSupervision").attr("checked", data.FormatoSupervision);
			}
			$("#txtEditarComercial_Cuadrillas").val(data.Cuadrillas);
			$("#txtEditarComercial_InspeccionesRealizadas").val(data.InspeccionesRealizadas);
			if (data.PlanSupervision == "true")
			{
				$("#txtEditarComercial_PlanSupervision").attr("checked", data.PlanSupervision);
			}
			$("#txtEditarComercial_Observaciones").val(data.Observaciones);
			$("#txtEditarComercial_ObservacionesCliente").val(data.ObservacionesCliente);
			$("#txtEditarComercial_ObservacionesPredio").val(data.ObservacionesPredio);
			if (data.Cumple == "true")
			{
				$("#txtEditarComercial_Cumple").attr("checked", data.Cumple);
			}
			$("#txtEditarComercial_CodIncumplimiento").val(data.CodIncumplimiento);

			$("#frmEditarComercial").on("submit", function(evento)
            {
              evento.preventDefault();
              $.post("php/comercial/editarComercial.php",
              {
                Prefijo : $("#txtPrefijo").val(),
                idLogin : Usuario.id,
                OT : $("#txtEditarComercial_Nombre").val(),
                FechaIngreso : $("#txtEditarComercial_FechaInicio").val(),
                HoraInicio : $("#txtEditarComercial_HoraIni").val(),
                Direccion  : $("#txtEditarComercial_Direccion").val(),
                Atendio : $("#txtEditarComercial_Atendio").val(),
                Telefono : $("#txtEditarComercial_Telefono").val(),
                TipoInterventoria : $("#txtEditarComercial_TipoInterventoria").val(),
                SubProceso : $("#txtEditarComercial_SubProceso").val(),
                Medida : $("#txtEditarComercial_Medida").val(),
                SE : $("#txtEditarComercial_Se").val(),
                Medida2 : $("#txtEditarComercial_Medida2").val(),
                ActividadEconomica : $("#txtEditarComercial_ActividadEconomica").val(),
                Factor : $("#txtEditarComercial_Factor").val(),
                MedActiva : $("#txtEditarComercial_MedActiva").val(),
                LecActiva : $("#txtEditarComercial_LecActiva").val(),
                TipoFase : $("#txtEditarComercial_TipoFase").val(),
                Marca : $("#txtEditarComercial_Marca").val(),
                Red : $("#txtEditarComercial_Red").val(),
                AccesoMedidor : checkValor("txtEditarComercial_AccesoMedido"),
                TipoAcometida : $("#txtEditarComercial_TipoAcometida").val(),
                AcometidaCompartida : checkValor("txtEditarComercial_AcometidaCompartida"),
                LocalizacionMedidor : $("#txtEditarComercial_LocalizacionMedidor").val(),
                MedidaReferencia : $("#txtEditarComercial_MedidaReferencia").val(),
                Sellos : $("#txtEditarComercial_Sellos").val(),
                CapaTrafo : $("#txtEditarComercial_CapaTrafo").val(),
                CD : $("#txtEditarComercial_CD").val(),
                PuntoFisico : $("#txtEditarComercial_PuntoFisico").val(),
                Observaciones : $("#txtEditarComercial_Observaciones").val(),
                pFormatoSupervision : checkValor("txtEditarComercial_FormatoSupervision"),
                Cuadrillas : $("#txtEditarComercial_Cuadrillas").val(),
                InspeccionesRealizadas : checkValor("txtEditarComercial_InspeccionesRealizadas"),
                PlanSupervision : $("#txtEditarComercial_PlanSupervision").val(),
                ObservacionesCliente : $("#txtEditarComercial_ObservacionesCliente").val(),
                ObservacionesPredio : $("#txtEditarComercial_ObservacionesPredio").val(),
                Cumple : checkValor("txtEditarComercial_Cumple"),
                CodIncumplimiento : $("#txtEditarComercial_CodIncumplimiento").val(),
                CruadrillaTipo :  $('#txtEditarComercial_CruadrillaTipo').val(),
                CuadrillaNum :  $('#txtEditarComercial_CuadrillaNum').val(),
                CuadrillaSupervisor :  $('#txtEditarComercial_CuadrillaSupervisor').val(),
                CedulaSupervisor :  $('#txtEditarComercial_CedulaSupervisor').val(),
                SupervisorCelular :  $('#txtEditarComercial_SupervisorCelular').val(),
                CuadrillaAux1 :  $('#txtEditarComercial_CuadrillaAux1').val(),
                CuadrillaAux2 :  $('#txtEditarComercial_CuadrillaAux2').val(),
                CuadrillaTec1 :  $('#txtEditarComercial_CuadrillaTec1').val(),
                CuadrillaCelulares :  $('#txtEditarComercial_CuadrillaCelulares').val(),
                CuadrillaCedulas :  $('#txtEditarComercial_CuadrillaCedulas').val(),
                NoOrden :  $('#txtEditarComercial_NoOrden').val(),
                DetalleActividad :  $('#txtEditarComercial_DetalleActividad').val()
              }, function(data, textStatus, xhr)
              {
                if (data > 0)
                {
                  Mensaje("Ok", "La Ejecución ha sido actualizada.");    
                } else
                {
                  Mensaje("Error", data);    
                }
              }).always(function() 
              {
                //Cuando Finaliza
              }).fail(function() {
                Mensaje("Error", "No fue posible almacenar La Ejecución, por favor intenta nuevamente.");
              });
              
            });
			
		}, "json");
}