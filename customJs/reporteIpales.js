function reporteIpales()
{
  $( ".lblLinkIpal").unbind( "click" );
  $( ".lblEditarIpal").unbind( "click" );
  $( ".lblEliminarIpal").unbind( "click" );
  $(".lblEditarIpal").live("click", lblEditarIpal_Click);
  $(".lblEliminarIpal").live("click", lblEliminarIpal_Click);
  $(".lblLinkIpal").live("click", lblLinkIpal_Click);
  
  $("#btnIpalDetalle_Archivos").unbind( "click" );
  $("#btnIpalDetalle_Archivos").live("click", btnIpalDetalle_Archivos_Click);
  
  $("#frmReporteIpales").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteIpales_CargarIpal();
    });
  $("#frmReporteIpalesCompleto").on("submit", function(evento)
    {
      evento.preventDefault();
      reporteIpales_CargarIpalCompleto();
    });
  
}
 function lblLinkIpal_Click(evento)
 {
  evento.preventDefault();
    var pIdIpal = $(this).attr("idIpal");
    $(this).cargarModulo({pagina: "reporteIpalesDetalle", titulo : "Detalles de Ipal " + $(this).text(), icono : "icon-eye-open"},
      function()
      {
        $.post("php/ipal/cargarDetalle.php", {idIpal : pIdIpal },
          function(data)
          {
            $("#btnIpalDetalle_Archivos").attr("Prefijo", data.Prefijo);
            $("#btnIpalDetalle_Archivos").attr("Consecutivo", data.idIpal);
            
            $("#txtIpalDetalle_Resultado").attr("src", "img/" + data.Resultado + ".png")

            $("#txtIpalDetalle_Consecutivo").text(data.Consecutivo);
            $("#txtIpalDetalle_idIpal").text(data.idIpal);
            $("#txtIpalDetalle_Fecha").text(data.Fecha);
            $("#txtIpalDetalle_Empresa").text(data.Empresa);
            $("#txtIpalDetalle_Direccion").text(data.Direccion);
            $("#txtIpalDetalle_Usuario").html('<a href="#" class="lblLinkUsuario" idLogin="' + data.idLogin + '">' + data.Usuario + '</a>');
            $("#txtIpalDetalle_NoContrato").text(data.NoContrato);
            $("#txtIpalDetalle_Observaciones").text(data.Observaciones);
            $("#txtIpalDetalle_Celular").text(data.Celular);
            $("#txtIpalDetalle_VehiculoTipo").text(data.VehiculoTipo);
            $("#txtIpalDetalle_VehiculoPlaca").text(data.VehiculoPlaca);
            $("#txtIpalDetalle_GruaPlaca").text(data.GruaPlaca);
            $("#txtIpalDetalle_CanastaPlaca").text(data.CanastaPlaca);
            $("#txtIpalDetalle_MotoPlaca").text(data.MotoPlaca);
            $("#txtIpalDetalle_Trabajo").text(data.Trabajo);
            $("#txtIpalDetalle_Proceso").text(data.Proceso);
            $("#txtIpalDetalle_Zona").text(data.Zona);
            $("#txtIpalDetalle_Recibio").text(data.Recibio);
            $("#txtIpalDetalle_CargoEmpresa").text(data.CargoEmpresa);

            $("#tblIpalesDetalle").dataTable().fnDestroy();
            $("#tblIpalesDetalle tbody tr").remove();

            var tds = "";
            $.each(data.Detalle, function(index, value)
              {
                var Estado = "success";
                if (value.Resultado == "No")
                {
                  Estado = "error";
                }
                if (value.Resultado == "NA")
                {
                  Estado = "info";
                }

                tds += '<tr><td></td>';
                tds += '<td>' + value.Numeral+ '</td>';
                tds += '<td>' + value.Descripcion+ '</td>';
                tds += '<td class="alert alert-block alert-'+ Estado +'">' + value.Resultado + '</td>';
                tds += '</tr>';
              });
            $("#tblIpalesDetalle tbody").append(tds);
            $("#tblIpalesDetalle").crearTabla1({lblMenu : "Resultados por página"});
          }, 'json').always(function() 
        {
          //Cuando Finaliza
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
      })
 }
 function reporteIpales_CargarIpalCompleto()
 {
  $(".icoCargando").show();
  $.post("php/ipal/cargarIpalCompleto.php", {Desde: $("#txtReporteIpalesCompleto_Desde").val(), Hasta : $("#txtReporteIpalesCompleto_Hasta").val()}, 
        function(data)
        {
            $("#tblIpalesCompleto").dataTable().fnDestroy();
            $("#tblIpalesCompleto tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";

            $.each(data, function(index, value)
              {
                var Estado = "success";
                if (value.Resultado != "Cumple")
                {
                  Estado = "error";
                } 
                tds += '<tr>';
                tds += '<td></td>';
                tds += '<td><a href="#" class="lblLinkIpal" idIpal="' + value.idIpal + '">' + value.Consecutivo + '</a></td>';
                tds += '<td>' + value.Fecha + '</td>';
                tds += '<td>' + value.Empresa + '</td>';
                tds += '<td>' + value.Direccion + '</td>';
                tds += '<td>' + value.Celular + '</td>';
                tds += '<td>' + value.VehiculoTipo + '</td>';
                tds += '<td>' + value.VehiculoPlaca + '</td>';
                tds += '<td>' + value.GruaPlaca + '</td>';
                tds += '<td>' + value.CanastaPlaca + '</td>';
                tds += '<td>' + value.MotoPlaca + '</td>';
                tds += '<td>' + value.NoContrato + '</td>';
                tds += '<td>' + value.Trabajo + '</td>';
                tds += '<td>' + value.Proceso + '</td>';
                tds += '<td><a href="#" class="lblLinkUsuario" idLogin="' + value.idLogin + '">' + value.Usuario + '</a></td>'
                tds += '<td>' + value.Observaciones + '</td>';
                tds += '<td>' + value.Zona + '</td>';
                tds += '<td>' + value.Recibio + '</td>';
                tds += '<td>' + value.CargoEmpresa + '</td>';
                tds += '<td class="alert alert-block alert-'+ Estado +'">' + value.Resultado + '</td>';
                tds += '</tr>';

              });
            $("#tblIpalesCompleto tbody").append(tds);
            $("#tblIpalesCompleto").crearTabla1({lblMenu : "Ipales por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }

 function reporteIpales_CargarIpal()
 {
  $(".icoCargando").show();
  $.post("php/ipal/cargarIpal.php", {Desde: $("#txtReporteIpales_Desde").val(), Hasta : $("#txtReporteIpales_Hasta").val()}, 
        function(data)
        {
            $("#tblIpales").dataTable().fnDestroy();
            $("#tblIpales tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";

            $.each(data, function(index, value)
              {
                var Estado = "success";
                if (value.Resultado != "Cumple")
                {
                  Estado = "error";
                } 
                tds += '<tr>';
                tds += '<td></td>';
                tds += '<td><a href="#" class="lblLinkIpal" idIpal="' + value.idIpal + '">' + value.Consecutivo + '</a></td>';
                tds += '<td>' + value.Fecha + '</td>';
                tds += '<td>' + value.Empresa + '</td>';
                tds += '<td>' + value.Direccion + '</td>';
                tds += '<td>' + value.NoContrato + '</td>';
                tds += '<td><a href="#" class="lblLinkUsuario" idLogin="' + value.idLogin + '">' + value.Usuario + '</a></td>'
                tds += '<td>' + value.Observaciones + '</td>';
                tds += '<td>' + value.Zona + '</td>';
                tds += '<td>' + value.Recibio + '</td>';
                tds += '<td>' + value.CargoEmpresa + '</td>';
                tds += '<td class="alert alert-block alert-'+ Estado +'">' + value.Resultado + '</td>';
                tds += '<td><a Prefijo="' + value.Prefijo + ' " class="lblEditarIpal"><i class="icon-edit"></i> </a></td>';
                tds += '<td><a Prefijo="' + value.Prefijo + ' " class="lblEliminarIpal"><i class="icon-trash"></i> </a></td>';
                tds += '</tr>';

              });
            $("#tblIpales tbody").append(tds);
            $("#tblIpales").crearTabla1({lblMenu : "Ipales por página"});
          }
        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }

function btnIpalDetalle_Archivos_Click()
{
  var pPrefijo = $(this).attr("prefijo");
  var pConsecutivo = $(this).attr("Consecutivo");
  cargarArchivos("Ipal: " + pConsecutivo, "Ipales\/" + pPrefijo); 
}
function lblEditarIpal_Click()
{
  $("#txtPrefijo").val($(this).attr("Prefijo"));
  $(this).cargarModulo({pagina: "editarIpal", titulo : "Editar Inspeccion " + $(this).text(), icono : "icon-edit"});
}
function lblEliminarIpal_Click(evento)
{
  evento.preventDefault();
  var pPrefijo = $(this).attr("Prefijo");
  var obFila = $(this).parent("td").parent("tr");
  $.post("php/comercial/eliminarComercial.php", {idLogin : Usuario.id, Prefijo : pPrefijo}, function(data)
    {
      if (data == 1)
      {
        Mensaje("Ok", "Inspección Borrada");
        $(obFila).remove();

      } else
      {
        Mensaje("Error", data);
      }
    });
}