function factura()
{
  $("#frmFactura").on("submit", function(evento)
    {
      evento.preventDefault();
      frmFactura_Submit();
    });
}

 function frmFactura_Submit()
 {
  $(".icoCargando").show();
  $.post("php/cargarFactura.php", {Desde: $("#txtFactura_Desde").val(), Hasta : $("#txtFactura_Hasta").val()}, 
        function(data)
        {
            $("#tblFactura").dataTable().fnDestroy();
            $("#tblFactura tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {
            var tds = "";
            var totalInspecciones = 0;
            var totalDinero = 0.0;

            $.each(data, function(index, value)
              {
                tds += '<tr>';
                tds += '<td>' + (index + 1) + '</td>';
                tds += '<td>' + value.Tipo + '</td>';
                tds += '<td>' + value.Actividad + '</td>';
                tds += '<td>' + value.Zona + '</td>';
                tds += '<td>' + value.SubZona + '</td>';
                tds += '<td>' + value.Municipio + '</td>';
                tds += '<td>$ ' + separadorMiles(value.Adjudicado) + '</td>';
                tds += '<td>' + value.Cantidad + '</td>';
                tds += '<td>$ ' + separadorMiles(value.Total) + '</td>';
                tds += '</tr>';

                //totalDinero += parseFloat(value.Total.replace(".", "").replace(",", "."));
                totalDinero += parseFloat(value.Total);
                if (value.Actividad != "VALOR ADICIONAL POR DESPLAZAMIENTO VEREDAL MUNICIPIOS DISPERSOS" && value.Actividad != "VIÁTICOS Y PERNOCTADAS" && value.Actividad != "HORA DE ACTIVIDAD DE MONITOREO CON AYUDAS TECNOLÓGICAS")
                {
                  totalInspecciones += parseInt(value.Cantidad);
                }
              });

            $("#lblFactura_TotalInspecciones span").text(totalInspecciones);
            $("#lblFactura_SubTotal span").text("$ " + separadorMiles(totalDinero));
            $("#lblFactura_IVA span").text("$ " + separadorMiles((totalDinero * 0.16)));
            $("#lblFactura_Total span").text("$ " + separadorMiles((totalDinero * 1.16)));
            $("#tblFactura tbody").append(tds);
            $("#tblFactura").crearTabla1({lblMenu : "Ordenes por página"});
          }
        }, 'json').always(function() 
        {
          $(".icoCargando").hide();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
 }

 function separadorMiles(Numero)
{
  var arrNumero = String(Numero).split(".");
  var number = new String(arrNumero[0]);
  var decimals = 0;
  if (arrNumero.length > 1)
  {
    decimals = arrNumero[1].substring(0, 2);
  }

  var result = '';
  while( number.length > 3 )
  {
   result = '.' + number.substr(number.length - 3) + result;
   number = number.substring(0, number.length - 3);
  }
  result = number + result;
  return result + "," + decimals;
};