function verMateriales()
{       
         // initiate layout and plugins
         $("#frmVerMateriales .chosen").chosen();

         $("#txtVerMateriales_BuscarPor").chosen().change(txtVerMateriales_BuscarPor_Click);

         $("#btnVerMateriales_Buscar").on("click", btnVerMateriales_Buscar_Click);
}

function txtVerMateriales_BuscarPor_Click(evento, Objeto)
{
$.each(Objeto, function(index, value)
  {
    var str = value + ", ";
    if (index == "selected")
    {
      $("#txtVerMateriales_Parametros").val($("#txtVerMateriales_Parametros").val() + str)
    } else
    {
      $("#txtVerMateriales_Parametros").val($("#txtVerMateriales_Parametros").val().replace(str, ""));
    }
  });
}

function btnVerMateriales_Buscar_Click()
 {
    var pParametros = $("#txtVerMateriales_Parametros").val();
    if (pParametros != "")
    {
      $(".icoCargando").show();
        $.post("php/buscarMateriales.php", {parametros: pParametros, criterio : $("#txtVerMateriales_Criterio").val()}, function(data)
        {
            $("#tblMateriales").dataTable().fnDestroy();
            $("#tblMateriales tbody tr").remove();
          if (data == 0)
          {
            Mensaje("Hey", "No se encontraron resultados");
          } else
          {

            var tds = "";

            $.each(data, function(index, value)
              {
                var Estado = "success";
                    if (value.Estado != "Entregado")
                    {
                      Estado = "important";
                    } 
                tds += '<tr>';
                tds += '<td class="span2"><center><div class="thumbnail">';
                  tds += '<div class="item">';
                    tds += '<a class="fancybox-button" data-rel="fancybox-button" title="' + value.Nombre + '" href="' + value.Foto + '">';
                      tds += '<div class="zoom">';
                        tds += ' <img width="50" height="50" src="' + value.Foto + '" alt="' + value.Nombre + '" />';
                        tds += '<div class="zoom-icon"></div>';
                      tds += '</div>';
                    tds += '</a>';
                  tds += '</div>'
                tds += '</div></center></td>';
                //tds += '<td><center><img width="40" height="40" src="' + value.Foto + '"/></center></td>'
                tds += '<td><a href="javascript:void(0)" class="lblMaterialNombre" idMaterial="' + value.idMaterial + '">' + value.Nombre + '</a></td>';
                tds += '<td>' + value.Marca + '</td>';
                tds += '<td>' + value.Referencia + '</td>';
                tds += '<td>' + value.FechaAsignacion + '</td>';
                tds += '<td><a href="#" class="lblLinkUsuario" idLogin="' + value.idLogin + '">' + value.Usuario + '</a></td>'
                tds += '<td><span class="label label-'+ Estado +'">' + value.Estado + '</span></td>';
                tds += '</tr>';

              });
            $("#tblMateriales tbody").append(tds);
            $("#tblMateriales").crearTabla1({lblMenu : "Equipos por página"});
            fancybox();

            /*
            $("#txtVerMateriales_ContenedorResultados").append(tds);
            PortletDraggable.init();                    
            cerrarPorlet_();
            */
          }

        }, 'json').always(function() 
        {
          //Cuando Finaliza
          $(".icoCargando").show();
        }).fail(function() {
          Mensaje("Error", "No fue posible conectar con el Servidor");
        });
    } else
    {
      Mensaje("Hey", "Tienes que seleccionar por lo menos un parámetro");
    }
      
}
function fancybox()
{
  if (!jQuery().fancybox) {
            alert(obj);
        }

        if (jQuery("#tblMateriales tbody .fancybox-button").size() > 0) {
            jQuery("#tblMateriales tbody .fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });
        }
}
function verMaterialesDetalle()
{
  $("#frmEntregarMaterial").on("submit", function(evento)
    {
      evento.preventDefault();
      
      $.post("php/entregarMaterial.php", 
        {pUsuario : Usuario.id, 
          idMaterial : $("#frmEntregarMaterial").attr("idMaterial"),
          Descripcion : $("#txtMaterialesDetalle_ObservacionesEntrega").val(), 
          Foto : $("#frmEntregarMaterial .fileupload-preview img").attr("src")}, function(data)
          {
            if (data == 1)
            {
              $("#txtMaterialesDetalle_ImagenEntrega").attr("src", $("#frmEntregarMaterial .fileupload-preview img").attr("src"));
              $("#txtMaterialesDetalle_Recibio").html('<a href="#" class="lblLinkUsuario" idLogin="' + Usuario.id + '">' + Usuario.Nombre + '</a>');
              
              $("#txtMaterialesDetalle_FechaEntrega").text("Hoy");
              $("#txtMaterialesDetalle_DetalleEntrega").text($("#txtMaterialesDetalle_ObservacionesEntrega").val());

              $("#txtMaterialesDetalle_Estado").text("Entregado")
              $("#txtMaterialesDetalle_Estado").removeClass("*");
              $("#txtMaterialesDetalle_Estado").addClass("label label-important");

              $("#frmEntregarMaterial").slideUp();
              $("#frmEntregarMaterial")[0].reset();

              Mensaje("Ok", "El Equipo ha sido entregado.");    
            } else
            {
              Mensaje("Ok", "Hubo un error de comunicación.");    
            }
          }).always(function() 
              {

              }).fail(function() {
                Mensaje("Error", "No hay conexión con el servidor");
              });
    });
}