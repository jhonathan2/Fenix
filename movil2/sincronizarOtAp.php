<?php
  include("../php/conectar.php");

  date_default_timezone_set('America/Bogota');

  $link = Conectar();
  $Fecha = $_POST['fecha'];
  
   $sql = "SELECT DISTINCT
            municipio,
            trafo_plac AS cd,
            ot,
            CONCAT(trafo_nox, ',', trafo_noy) AS trafo_no,
            trafo_mun,
            trafo_capa,
            trafo_fase,
            trafo_ubic,
            trafo_barr,
            trafo_dir,
            clase_red,
            uso_red,
            Fecha
          FROM EEC_ApConsolidado_In
          WHERE Fecha >= '$Fecha';";

  $result = $link->query($sql);

  if ( $result->num_rows > 0)
  {
    
    $idx = 0;
    $Resultado = array('Trafos' => array(), 'Luminarias' => array());

     while ($row = mysqli_fetch_assoc($result))
     { 
        $Resultado['Trafos'][$idx] = array();

        foreach ($row as $key => $value) 
        {
          $Resultado['Trafos'][$idx][$key] = utf8_encode($value);
          if ($key == "Fecha")
            {
              $Resultado['Trafos'][$idx][$key] = strtotime($value);
            }
        }
        $idx++;
     }

     $sql = "SELECT DISTINCT
            municipio,
            trafo_plac AS cd,
            luminaria,
            punto_sig,
            ilu_dir,
            CONCAT(ilu_nox, ',', ilu_noy) AS ilu_no,
            ilu_tipoco,
            ilu_cant,
            ilu_tiplam,
            ilu_ilumin,
            ilu_propie,
            ilu_tipvia,
            rele_tipo,
            CONCAT(rele_nox, ',', rele_noy) AS rele_no,
            red_tipseg,
            red_mat,
            red_estado,
            tiempo_op,
            tipo_red,
            estado,
            Fecha         
          FROM EEC_ApConsolidado_In
          WHERE Fecha >= '$Fecha';";
      $idx = 0;

      $result = $link->query($sql);

      while ($row = mysqli_fetch_assoc($result))
       { 
          $Resultado['Luminarias'][$idx] = array();

          foreach ($row as $key => $value) 
          {
            $Resultado['Luminarias'][$idx][$key] = utf8_encode($value);
            if ($key == "Fecha")
            {
              $Resultado['Luminarias'][$idx][$key] = strtotime($value);
            }
          }
          $idx++;
       }
     
        mysqli_free_result($result);  
        echo json_encode($Resultado);
  } else
  {
    echo 0;
  }   

?>
