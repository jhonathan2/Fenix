<?php
   include("../php/conectar.php"); 
   include("../php/Inspecciones.php"); 
   include ("funciones.php");
   include ("php.ini");
   
   $link = Conectar();

   date_default_timezone_set("America/Bogota");
   $fecha = date("Y-m-d H:i:s");

      $datos = $_POST['datos'];

   $values = "";
   if ($datos <> "")
   {
      foreach ($datos as $key => $value) 
         {
            $Prefijo =  $value['Prefijo'];
            $idLogin =  $value['idInspeccion'];

            $OT =  $value['OT'];
            $FechaIngreso =  $value['FechaIngreso'];
            $HoraInicio =  $value['HoraInicio'];
            $Direccion  =  $value['Direccion'];
            $Atendio =  $value['Atendio'];
            $Telefono =  $value['Telefono'];
            $TipoInterventoria =  $value['TipoInterventoria'];
            $SubProceso =  $value['SubProceso'];
            $Medida =  $value['Medida'];
            $SE =  $value['SE'];
            $Medida2 =  $value['Medida2'];
            $ActividadEconomica =  $value['ActividadEconomica'];
            $Factor =  $value['Factor'];
            $MedActiva =  $value['MedActiva'];
            $LecActiva = $value['LecActiva'];
            $TipoFase =  $value['TipoFase'];
            $Marca =  $value['Marca'];
            $Red =  $value['Red'];

            $AccesoMedidor =  $value['AccesoMedidor'];
            $TipoAcometida =  $value['TipoAcometida'];
            $AcometidaCompartida =  $value['AcometidaCompartida'];
            $LocalizacionMedidor =  $value['LocalizacionMedidor'];
            $MedidaReferencia =  $value['MedidaReferencia'];
            $Sellos =  $value['Sellos'];
            $CapaTrafo =  $value['CapaTrafo'];
            $CD =  $value['CD'];
            $PuntoFisico =  $value['PuntoFisico'];
            $Observaciones =  $value['Observaciones'];

            $pFormatoSupervision =  $value['FormatoSupervision'];
            $pCuadrillas =  $value['Cuadrillas'];
            $pInspeccionesRealizadas =  $value['InspeccionesRealizadas'];
            $pPlanSupervision =  $value['PlanSupervision'];
            $pObservacionesCliente =  $value['ObservacionesCliente'];
            $pObservacionesPredio =  $value['ObservacionesPredio'];
            $pCumple =  $value['Cumple'];
            $pCodIncumplimiento =  $value['CodIncumplimiento'];

            $pCruadrillaTipo =  $value['CruadrillaTipo'];
            $pCuadrillaNum =  $value['CuadrillaNum'];
            $pCuadrillaSupervisor =  $value['CuadrillaSupervisor'];
            $pCedulaSupervisor =  $value['CedulaSupervisor'];
            $pSupervisorCelular =  $value['SupervisorCelular'];
            $pCuadrillaAux1 =  $value['CuadrillaAux1'];
            $pCuadrillaAux2 =  $value['CuadrillaAux2'];
            $pCuadrillaTec1 =  $value['CuadrillaTec1'];
            $pCuadrillaCelulares =  $value['CuadrillaCelulares'];
            $pCuadrillaCedulas =  $value['CuadrillaCedulas'];
            $pNoOrden =  $value['NoOrden'];
            $pDetalleActividad =  $value['DetalleActividad'];

            $idInspeccion = devolverId($Prefijo);
            if (is_numeric($idInspeccion))
            {
               $sql = "INSERT INTO Comercial_1 (Prefijo, idInspeccion, OT, FechaIngreso, HoraInicio, Direccion, Atendio, Telefono, 
               TipoInterventoria, SubProceso, Medida, SE, Medida2, ActividadEconomica, Factor, MedActiva, LecActiva, TipoFase, Marca, Red, 
               AccesoMedidor, TipoAcometida, AcometidaCompartida, LocalizacionMedidor, MedidaReferencia, Sellos, CapaTrafo, CD, PuntoFisico, 
               Observaciones, FormatoSupervision, Cuadrillas, InspeccionesRealizadas, PlanSupervision, ObservacionesCliente, ObservacionesPredio, 
               Cumple, CodIncumplimiento, CruadrillaTipo, CuadrillaNum, CuadrillaSupervisor, CedulaSupervisor, SupervisorCelular, CuadrillaAux1, 
               CuadrillaAux2, CuadrillaTec1, CuadrillaCelulares, CuadrillaCedulas, NoOrden, DetalleActividad) 
                  VALUES (
                     '$Prefijo', '$idInspeccion', '$OT', '$FechaIngreso', '$HoraInicio', '$Direccion', '$Atendio', '$Telefono', '$TipoInterventoria', 
                     '$SubProceso', '$Medida', '$SE', '$Medida2', '$ActividadEconomica', '$Factor', '$MedActiva', '$LecActiva', '$TipoFase', 
                     '$Marca', '$Red', '$AccesoMedidor', '$TipoAcometida', '$AcometidaCompartida', '$LocalizacionMedidor', '$MedidaReferencia', 
                     '$Sellos', '$CapaTrafo', '$CD', '$PuntoFisico', '$Observaciones', '$pFormatoSupervision','$pCuadrillas', '$pInspeccionesRealizadas', '$pPlanSupervision', 
                     '$pObservacionesCliente', '$pObservacionesPredio', '$pCumple', '$pCodIncumplimiento', '$pCruadrillaTipo', '$pCuadrillaNum', 
                     '$pCuadrillaSupervisor', '$pCedulaSupervisor', '$pSupervisorCelular', '$pCuadrillaAux1', '$pCuadrillaAux2', '$pCuadrillaTec1', 
                     '$pCuadrillaCelulares', '$pCuadrillaCedulas', '$pNoOrden', '$pDetalleActividad')
                  ON DUPLICATE KEY UPDATE 
                     OT = VALUES(OT), 
                     FechaIngreso = VALUES(FechaIngreso), 
                     HoraInicio = VALUES(HoraInicio), 
                     Direccion = VALUES(Direccion), 
                     Atendio = VALUES(Atendio), 
                     Telefono = VALUES(Telefono), 
                     TipoInterventoria = VALUES(TipoInterventoria), 
                     SubProceso = VALUES(SubProceso), 
                     Medida = VALUES(Medida), 
                     SE = VALUES(SE), 
                     Medida2 = VALUES(Medida2), 
                     ActividadEconomica = VALUES(ActividadEconomica), 
                     Factor = VALUES(Factor), 
                     MedActiva = VALUES(MedActiva), 
                     LecActiva = VALUES(LecActiva), 
                     TipoFase = VALUES(TipoFase), 
                     Marca = VALUES(Marca), 
                     Red = VALUES(Red), 
                     AccesoMedidor = VALUES(AccesoMedidor), 
                     TipoAcometida = VALUES(TipoAcometida), 
                     AcometidaCompartida = VALUES(AcometidaCompartida), 
                     LocalizacionMedidor = VALUES(LocalizacionMedidor), 
                     MedidaReferencia = VALUES(MedidaReferencia), 
                     Sellos = VALUES(Sellos), 
                     CapaTrafo = VALUES(CapaTrafo), 
                     CD = VALUES(CD), 
                     PuntoFisico = VALUES(PuntoFisico), 
                     Observaciones = VALUES(Observaciones), 
                     FormatoSupervision = VALUES(FormatoSupervision), 
                     Cuadrillas = VALUES(Cuadrillas), 
                     InspeccionesRealizadas = VALUES(InspeccionesRealizadas), 
                     PlanSupervision = VALUES(PlanSupervision), 
                     ObservacionesCliente = VALUES(ObservacionesCliente), 
                     ObservacionesPredio = VALUES(ObservacionesPredio), 
                     Cumple = VALUES(Cumple), 
                     CodIncumplimiento = VALUES(CodIncumplimiento), 
                     CruadrillaTipo = VALUES(CruadrillaTipo), 
                     CuadrillaNum = VALUES(CuadrillaNum), 
                     CuadrillaSupervisor = VALUES(CuadrillaSupervisor), 
                     CedulaSupervisor = VALUES(CedulaSupervisor), 
                     SupervisorCelular = VALUES(SupervisorCelular), 
                     CuadrillaAux1 = VALUES(CuadrillaAux1), 
                     CuadrillaAux2 = VALUES(CuadrillaAux2), 
                     CuadrillaTec1 = VALUES(CuadrillaTec1), 
                     CuadrillaCelulares = VALUES(CuadrillaCelulares), 
                     CuadrillaCedulas = VALUES(CuadrillaCedulas), 
                     NoOrden = VALUES(NoOrden), 
                     DetalleActividad = VALUES(DetalleActividad);";

                  $link->query(utf8_decode($sql));
                  if ( $link->affected_rows == 0)
                        {
                           $sql .= "<br>" . implode("|", $datos);
                           //mensajeError($sql, $link->error);
                        }

                  /*$fp = fopen($Prefijo + '.txt', 'w');
                  fwrite($fp, $sql);
                  fclose($fp);*/
                  cerrarInspeccion($Prefijo);
            } else
            {
               echo $idInspeccion;
            }
         } 
         echo 0;//Aqui iba un 1  
   } else
   {
      echo 0;
   }
   mysqli_free_result($result);
?>