<?php
   include("../php/conectar.php"); 
   include("../php/Inspecciones.php"); 
   include ("funciones.php");
   include ("php.ini");
   
   $link = Conectar();

   date_default_timezone_set("America/Bogota");
   $fecha = date("Y-m-d H:i:s");

      $datos = $_POST['datos'];

   $values = "";
   if ($datos <> "")
   {
      foreach ($datos as $key => $value) 
         {
            $Prefijo = addslashes($value['Prefijo']);
            $idLogin = addslashes($value['idLogin']);

            $OT = addslashes($value['OT']);
            $Tipo = addslashes($value['Tipo']);
            $FechaIngreso = addslashes($value['FechaIngreso']);
            $Ejecutor  = addslashes($value['Ejecutor']);
            $Municipio = addslashes($value['Municipio']);
            $CD = addslashes($value['CD']);
            $CoorTransformador = addslashes($value['CoorTransformador']);
            $CapaTrafo = addslashes($value['CapaTrafo']);
            $Fase = addslashes($value['Fase']);
            $Ubicacion = addslashes($value['Ubicacion']);
            $Barrio = addslashes($value['Barrio']);
            $Direccion = addslashes($value['Direccion']);
            $Red = addslashes($value['Red']);
            $CodPoste = addslashes($value['CodPoste']);
            $TipoControl = $value['TipoControl'];
            $CantLuminarias = addslashes($value['CantLuminarias']);
            $TipoLuminarias = addslashes($value['TipoLuminarias']);
            $TipoIluminaria = addslashes($value['TipoIluminaria']);
            $Propietario = addslashes($value['Propietario']);
            $Via = addslashes($value['Via']);
            $TipoControl2 = addslashes($value['TipoControl2']);
            $TipoRed = addslashes($value['TipoRed']);
            $Materiales = addslashes($value['Materiales']);
            $Estado = addslashes($value['Estado']);
            $TiempoOperacion = addslashes($value['TiempoOperacion']);
            $Red2 = addslashes($value['Red2']);
            $Observaciones = addslashes($value['Observaciones']);
            $Estado2 = addslashes($value['Estado2']);

            $DireccionLuminaria = addslashes($value['DireccionLuminaria']);
            $UsoRed = addslashes($value['UsoRed']);
            $Luminaria = addslashes($value['Luminaria']);
            $CoordenadasIluminaria = addslashes($value['CoordenadasIluminaria']);
            $CoordenadasRele = addslashes($value['CoordenadasRele']);
            $TrafoDireccion = $Direccion;

            $idInspeccion = devolverId($Prefijo);
            if (is_numeric($idInspeccion))
            {
               $sql = "SELECT * FROM Alumbrado_1 WHERE Prefijo = '$Prefijo'";
               $result = $link->query($sql);

               if ( $result->num_rows == 0)
               {
                  $sql = "INSERT INTO Alumbrado_1 
                        (idInspeccion, Prefijo, idLogin, OT, Tipo, FechaIngreso, Ejecutor, Municipio, CD, CoorTransformador, 
                        CapaTrafo, Fase, Ubicacion, Barrio, Direccion, TrafoDireccion, Red, UsoRed, CodPoste, Luminaria, 
                        DireccionLuminaria, CoordenadasIluminaria, TipoControl, CantLuminarias, TipoIluminarias, 
                        TipoLuminaria, Propietario, Via, TipoControl2, CoordenadasRele, TipoRed, Materiales, Estado, 
                        TiempoOperacion, Red2, Observaciones, Estado2) 
                     VALUES 
                        ('$idInspeccion', '$Prefijo', '$idLogin', '$OT', '$Tipo', '$FechaIngreso', '$Ejecutor', '$Municipio', '$CD', '$CoorTransformador', 
                        '$CapaTrafo', '$Fase', '$Ubicacion', '$Barrio', '$Direccion', '$TrafoDireccion', '$Red', '$UsoRed', '$CodPoste', '$Luminaria', 
                        '$DireccionLuminaria', '$CoordenadasIluminaria', '$TipoControl', '$CantLuminarias', '$TipoLuminarias', 
                        '$TipoIluminaria', '$Propietario', '$Via', '$TipoControl2', '$CoordenadasRele', '$TipoRed', '$Materiales', '$Estado', 
                        '$TiempoOperacion', '$Red2', '$Observaciones', '$Estado2')
                           ON DUPLICATE KEY UPDATE 
                              Prefijo = VALUES(Prefijo), 
                              idLogin = VALUES(idLogin), 
                              OT = VALUES(OT), 
                              Tipo = VALUES(Tipo), 
                              FechaIngreso = VALUES(FechaIngreso), 
                              Ejecutor = VALUES(Ejecutor), 
                              Municipio = VALUES(Municipio), 
                              CD = VALUES(CD), 
                              CoorTransformador = VALUES(CoorTransformador), 
                              CapaTrafo = VALUES(CapaTrafo), 
                              Fase = VALUES(Fase), 
                              Ubicacion = VALUES(Ubicacion), 
                              Barrio = VALUES(Barrio), 
                              Direccion = VALUES(Direccion), 
                              TrafoDireccion = VALUES(TrafoDireccion), 
                              Red = VALUES(Red), 
                              UsoRed = VALUES(UsoRed), 
                              CodPoste = VALUES(CodPoste), 
                              Luminaria = VALUES(Luminaria), 
                              DireccionLuminaria = VALUES(DireccionLuminaria), 
                              CoordenadasIluminaria = VALUES(CoordenadasIluminaria), 
                              TipoControl = VALUES(TipoControl), 
                              CantLuminarias = VALUES(CantLuminarias), 
                              TipoIluminarias = VALUES(TipoIluminarias), 
                              TipoLuminaria = VALUES(TipoLuminaria), 
                              Propietario = VALUES(Propietario), 
                              Via = VALUES(Via), 
                              TipoControl2 = VALUES(TipoControl2), 
                              CoordenadasRele = VALUES(CoordenadasRele), 
                              TipoRed = VALUES(TipoRed), 
                              Materiales = VALUES(Materiales), 
                              Estado = VALUES(Estado), 
                              TiempoOperacion = VALUES(TiempoOperacion), 
                              Red2 = VALUES(Red2), 
                              Observaciones = VALUES(Observaciones), 
                              Estado2 = VALUES(Estado2);";
         
                           $link->query(utf8_decode($sql));
                           if ( $link->affected_rows == 0)
                                 {
                                    $sql .= "<br>" . implode("|", $datos);
                                    //mensajeError($sql, $link->error);
                                 }
         
                           cerrarInspeccion($Prefijo);
               }
            } else
            {
               echo $idInspeccion;
            }
         } 
         echo 0;//Aqui iba un 1  
   } else
   {
      echo 0;
   }
   mysqli_free_result($result);  
?>