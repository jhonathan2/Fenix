<?php
	include("../php/conectar.php"); 
   
   $link = Conectar();

   date_default_timezone_set("America/Bogota");
   $fecha = date("Y-m-d H:i:s");

      $datos = $_POST['datos'];

   $values = "";
   if ($datos <> "")
   {
      foreach ($datos as $key => $value) 
         {
               $sql = "SELECT idInspeccionTipo AS 'idInspeccionTipo' FROM Baremo WHERE idBaremo = '" . $value['idBaremo'] . "';";
               $result = $link->query($sql);
   
               $fila =  $result->fetch_array(MYSQLI_ASSOC);
               $inspeccionTipo = $fila['idInspeccionTipo'];
   
               if ($Veredal == 0)
               {
                  $sql = "SELECT * FROM Municipios WHERE idMunicipio = '$idMunicipio' AND Disperso = 'VERDADERO';";
                  $result = $link->query($sql);
   
                  if ( $result->num_rows > 0)
                  {
                     $Veredal = 1;
                  } 
               }
               if ($Tiempo <> "")
               {
                  $tmpTiempo = explode(":", $Tiempo);
   
                  if (count($tmpTiempo)  == 2)
                  {
                     $Tiempo = ($tmpTiempo[0] * 60) + ($tmpTiempo[1]);
                  } else
                  {
                        $Tiempo = 0;
                  }
               } else
               {
                     $Tiempo = 0;
               }
   
               $values .= "(
                              '$inspeccionTipo', 
                              '" . $value['fechaIngreso'] ."', 
                              '" . $value['idLogin'] . "',
                              '" . $value['Prefijo'] . "',
                              '" . $value['idBaremo'] . "',
                              '" . $value['Desplazamiento'] . "',
                              '" . $value['Tiempo'] . "',
                              '" . $value['Veredal'] . "',
                              '" . $value['Viaticos'] . "',
                              '" . $value['idMunicipio'] . "',
                              '0', 
                              '" . $value['Coordenadas'] . "',
                              '$fecha',
                              '" . $value['fechaFin'] . "',
                              '" . $value['Sucursal'] . "'), ";
         }   
   
         $values = substr($values, 0, -2);
   
         $sql = "INSERT INTO Inspecciones 
                           (idInspeccionTipo, fechaIngreso, idLogin, Prefijo, idBaremo, Desplazamiento, Tiempo, Veredal, Viaticos, idMunicipio, Estado, Coordenadas, fechaCargue, fechaFin, Sucursal) 
                        VALUES ";
   
         $sql .= $values;
   
         /*$sql .= " ON DUPLICATE KEY UPDATE 
                  idInspeccionTipo = VALUES(idInspeccionTipo), 
                  fechaIngreso = VALUES(fechaIngreso), 
                  idLogin = VALUES(idLogin), 
                  Prefijo = VALUES(Prefijo), 
                  idBaremo = VALUES(idBaremo), 
                  Desplazamiento = VALUES(Desplazamiento), 
                  Tiempo = VALUES(Tiempo), 
                  Veredal = VALUES(Veredal), 
                  Viaticos = VALUES(Viaticos), 
                  idMunicipio = VALUES(idMunicipio), 
                  Estado = VALUES(Estado), 
                  Coordenadas = VALUES(Coordenadas), 
                  fechaCargue = VALUES(fechaCargue), 
                  fechaFin = VALUES(fechaFin);";*/
            $sql .= " ON DUPLICATE KEY UPDATE 
                  idInspeccionTipo = VALUES(idInspeccionTipo), 
                  fechaIngreso = VALUES(fechaIngreso), 
                  idLogin = VALUES(idLogin), 
                  Prefijo = VALUES(Prefijo), 
                  idBaremo = VALUES(idBaremo), 
                  Desplazamiento = VALUES(Desplazamiento), 
                  Tiempo = VALUES(Tiempo), 
                  Veredal = VALUES(Veredal), 
                  Viaticos = VALUES(Viaticos), 
                  idMunicipio = VALUES(idMunicipio), 
                  Coordenadas = VALUES(Coordenadas), 
                  fechaFin = VALUES(fechaFin);";

      $link->query($sql);
   
         if ( $link->affected_rows > 0)
         {
           echo 0; //Aqui iba un 1
         } else
         {
           echo 0;
         }
   } else
   {
      echo 0;
   }
   mysqli_free_result($result);
?>