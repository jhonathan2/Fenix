<?php
   include("../php/conectar.php"); 
   include("../php/Inspecciones.php"); 
   include ("funciones.php");
   include ("php.ini");
   
   $link = Conectar();

   date_default_timezone_set("America/Bogota");
   $fecha = date("Y-m-d H:i:s");

      $datos = $_POST['datos'];

   $values = "";
   if ($datos <> "")
   {
      foreach ($datos as $key => $value) 
         {
            $Empresa = $value['Empresa'];
            $Direccion = $value['Direccion'];
            $Celular = $value['Celular'];
            $vehiculoTipo = $value['vehiculoTipo'];
            $vehiculoPlaca  = $value['vehiculoPlaca'];
            $gruaPlaca = $value['gruaPlaca'];
            $canastaPlaca = $value['canastaPlaca'];
            $motoPlaca = $value['motoPlaca'];
            $noContrato = $value['noContrato'];
            $Trabajo = $value['Trabajo'];
            $Proceso = $value['Proceso'];
            $Zona = $value['Zona'];
            $Cuadrilla = $value['Cuadrilla'];
            $jefeCuadrilla = $value['jefeCuadrilla'];
            $cargoEmpresa = $value['cargoEmpresa'];
            $Resultados = $value['Resultados'];
            $Prefijo = $value['Prefijo'];
            $idLogin = $value['idLogin'];
            $Observaciones = $value['Observaciones'];

               $idInspeccion = devolverId($Prefijo);
               if (is_numeric($idInspeccion))
               {
                  $sql = "INSERT INTO Ipal 
                           (idInspeccion, idLogin, Empresa, Direccion, Celular, 
                              VehiculoTipo, VehiculoPlaca, GruaPlaca, CanastaPlaca, MotoPlaca, 
                              NoContrato, Trabajo, Proceso, Zona, Cuadrilla, resultado, Prefijo, Observaciones, Cudrillero, Cuadrillero2) 
                           VALUES (
                              '$idInspeccion', 
                              '$idLogin', 
                              '$Empresa', 
                              '$Direccion', 
                              '$Celular', 
                              '$vehiculoTipo', 
                              '$vehiculoPlaca', 
                              '$gruaPlaca', 
                              '$canastaPlaca', 
                              '$motoPlaca', 
                              '$noContrato', 
                              '$Trabajo', 
                              '$Proceso',
                              '$Zona', 
                              '$Cuadrilla',
                              'Cumple',
                              '$Prefijo',
                              '$Observaciones',
                              '$jefeCuadrilla',
                              '$cargoEmpresa') ";
                  $sql .= " ON DUPLICATE KEY UPDATE 
                           idInspeccion = VALUES(idInspeccion), 
                           idLogin = VALUES(idLogin), 
                           Empresa = VALUES(Empresa), 
                           Direccion = VALUES(Direccion), 
                           Celular = VALUES(Celular), 
                           VehiculoTipo = VALUES(VehiculoTipo), 
                           VehiculoPlaca = VALUES(VehiculoPlaca), 
                           GruaPlaca = VALUES(GruaPlaca), 
                           CanastaPlaca = VALUES(CanastaPlaca), 
                           MotoPlaca = VALUES(MotoPlaca), 
                           NoContrato = VALUES(NoContrato), 
                           Trabajo = VALUES(Trabajo), 
                           Proceso = VALUES(Proceso), 
                           Zona = VALUES(Zona), 
                           Cuadrilla = VALUES(Cuadrilla), 
                           resultado = VALUES(resultado), 
                           Observaciones = VALUES(Observaciones), 
                           Cudrillero = VALUES(Cudrillero), 
                           Cuadrillero2 = VALUES(Cuadrillero2);";

                     $link->query(utf8_decode($sql));
                        if ( $link->affected_rows > 0)
                        {
                           $nuevoId = $link->insert_id;
                           if (is_numeric($nuevoId))
                           {
                              $resultadoF = FALSE;  

                              $tmpResultados = explode("#F#", $Resultados);
                              $values = "";
                              foreach ($tmpResultados as $idxIpal => $resultado) 
                                 {

                                    if ($resultado <>"")
                                    {
                                       $tmpResultado = explode("#C#", $resultado);
                                       $numeral = $tmpResultado[0];
                                       $valor = $tmpResultado[1];
                                       $observacion = $tmpResultado[2];
                                       $tmpIncumplidores = explode("->", $tmpResultado[3]);
                                       if (count($tmpIncumplidores) == 2)
                                       {
                                          $incumplidores = $tmpIncumplidores[0];
                                          $macroCategoria = $tmpIncumplidores[1];
                                       } else
                                       {
                                          $incumplidores = "";
                                          $macroCategoria = "";
                                       }

                                       if ($valor == "No")
                                       {
                                          $resultadoF = true;
                                       }
                                       $values .= "('$nuevoId', '$numeral', '$valor', '$observacion', '$incumplidores', '$macroCategoria'), ";
                                    }
                                 }
                                 $values = substr($values, 0, -2);

                              if ($resultadoF)
                              {
                                 $sql = "UPDATE Ipal set resultado = 'Rechazada' WHERE idIpal = '$nuevoId';";
                                 $link->query(utf8_decode($sql));
                              }

                              
                              if ($values <> "")
                              {
                                 $sql = "INSERT INTO IpalResultados (idIpal, Numeral, Resultado, Observaciones, Incumplidores, macrocategoria) 
                                 VALUES $values;";

                                    $link->query(utf8_decode($sql));
                                    cerrarInspeccion($Prefijo);
                              }
                           } else
                           {
                              echo 0;
                           }
                        } else
                        {
                           $sql .= "<br>" . implode("|", $datos);
                           //mensajeError($sql, $link->error);
                           echo 0;
                        }
               } else
               {
                  echo 0;
               }
         } 
         echo 0;   //Cambiar por 1
   } else
   {
      echo 0;
   }
   mysqli_free_result($result);
?>