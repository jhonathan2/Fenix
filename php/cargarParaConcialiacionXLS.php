<?php
	include("conectar.php"); 
  include("phpExcel/PHPExcel.php"); 

   $link = Conectar();

   $Desde = $_GET['Desde'] . " 00:00:00";
   $Hasta = $_GET['Hasta'] . " 23:59:59";
   $Anio = substr($Desde, 0, 4);
   $Fecha = substr($Desde, 0, 7);

   $sql = "SELECT * FROM Conciliacion_Inspecciones WHERE fechaIngreso BETWEEN '$Desde' AND '$Hasta';";
   $result = $link->query($sql);

  $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'idInspeccion')
                ->setCellValue('B1', 'fechaIngreso')
                ->setCellValue('C1', 'Actividad')
                ->setCellValue('D1', 'Veredal')
                ->setCellValue('E1', 'Cantidad')
                ->setCellValue('F1', 'Adjudicado')
                ->setCellValue('G1', 'Total')
                ->setCellValue('H1', 'Nombre')
                ->setCellValue('I1', 'Tipo');
      $idx = 2;
      
      
         while ($row = mysqli_fetch_assoc($result))
         { 
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $idx  , utf8_encode($row['idInspeccion']))
                ->setCellValue('B'. $idx , $row['fechaIngreso'])
                ->setCellValue('C'. $idx , utf8_encode($row['Actividad']))
                ->setCellValue('D'. $idx , $row['Veredal'])
                ->setCellValue('E'. $idx , $row['Cantidad'])
                ->setCellValue('F'. $idx , $row['Adjudicado2015'])
                ->setCellValue('G'. $idx , $row['Total'])
                ->setCellValue('H'. $idx , utf8_encode($row['Nombre']))
                ->setCellValue('I'. $idx , utf8_encode($row['Tipo']));
            $idx++;
         }
        mysqli_free_result($result);  
         $objPHPExcel->getActiveSheet()->setTitle('Inspecciones');


    $sql = "SELECT * FROM Conciliacion_Comercial WHERE Fecha BETWEEN '$Desde' AND '$Hasta';";
   $result = $link->query($sql);

  $objWorkSheet = $objPHPExcel->createSheet("Comercial", 1); 
  $objPHPExcel->addSheet($objWorkSheet);

    $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Prefijo')
                ->setCellValue('B1', 'Consecutivo Interno')
                ->setCellValue('C1', 'OT')
                ->setCellValue('D1', 'Baremo')
                ->setCellValue('E1', 'Adjudicado')
                ->setCellValue('F1', 'Disperso')
                ->setCellValue('G1', 'Total')
                ->setCellValue('H1', 'Fecha')
                ->setCellValue('I1', 'Hora Inicio')
                ->setCellValue('J1', 'Dirección')
                ->setCellValue('K1', 'Atendió')
                ->setCellValue('L1', 'Telefono')
                ->setCellValue('M1', 'Tipo')
                ->setCellValue('N1', 'Sub Proceso')
                ->setCellValue('O1', 'Medida')
                ->setCellValue('P1', 'SE')
                ->setCellValue('Q1', 'Medida2')
                ->setCellValue('R1', 'Act Economica')
                ->setCellValue('S1', 'Factor')
                ->setCellValue('T1', 'Med Activa')
                ->setCellValue('U1', 'Lec Activa')
                ->setCellValue('V1', 'Tipo Fase')
                ->setCellValue('W1', 'Marca')
                ->setCellValue('X1', 'Red')
                ->setCellValue('Y1', 'Acceso Medidor')
                ->setCellValue('Z1', 'Tipo Acometida')
                ->setCellValue('AA1', 'Acometida Compartida')
                ->setCellValue('AB1', 'Localización Medidor')
                ->setCellValue('AC1', 'Med Referencia')
                ->setCellValue('AD1', 'Sellos')
                ->setCellValue('AE1', 'Capa Trafo')
                ->setCellValue('AF1', 'CD')
                ->setCellValue('AG1', 'Punto Fisico')
                ->setCellValue('AH1', 'Observaciones')
                ->setCellValue('AI1', 'Obs Cliente')
                ->setCellValue('AJ1', 'Obs Predio')
                ->setCellValue('AK1', 'Cumple')
                ->setCellValue('AL1', 'Cod Incumplimiento')
                ->setCellValue('AM1', 'Frio/Sitio')
                ->setCellValue('AN1', 'Ejecutado Por');
      $idx = 2;

      
      
         while ($row = mysqli_fetch_assoc($result))
         { 
            $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A' . $idx  , utf8_encode($row['Prefijo']))
                ->setCellValue('B' . $idx  , utf8_encode($row['Consecutivo Interno']))
                ->setCellValue('C' . $idx  , utf8_encode($row['OT']))
                ->setCellValue('D' . $idx  , utf8_encode($row['Baremo']))
                ->setCellValue('E' . $idx  , $row['Adjudicado'])
                ->setCellValue('F' . $idx  , $row['Disperso'])
                ->setCellValue('G' . $idx  , $row['Total'])
                ->setCellValue('H' . $idx  , $row['Fecha'])
                ->setCellValue('I' . $idx  , $row['Hora Inicio'])
                ->setCellValue('J' . $idx  , utf8_encode($row['Dirección']))
                ->setCellValue('K' . $idx  , utf8_encode($row['Atendió']))
                ->setCellValue('L' . $idx  , utf8_encode($row['Telefono']))
                ->setCellValue('M' . $idx  , utf8_encode($row['Tipo']))
                ->setCellValue('N' . $idx  , utf8_encode($row['Sub Proceso']))
                ->setCellValue('O' . $idx  , utf8_encode($row['Medida']))
                ->setCellValue('P' . $idx  , utf8_encode($row['SE']))
                ->setCellValue('Q' . $idx  , utf8_encode($row['Medida2']))
                ->setCellValue('R' . $idx  , utf8_encode($row['Act Economica']))
                ->setCellValue('S' . $idx  , utf8_encode($row['Factor']))
                ->setCellValue('T' . $idx  , utf8_encode($row['Med Activa']))
                ->setCellValue('U' . $idx  , utf8_encode($row['Lec Activa']))
                ->setCellValue('V' . $idx  , utf8_encode($row['Tipo Fase']))
                ->setCellValue('W' . $idx  , utf8_encode($row['Marca']))
                ->setCellValue('X' . $idx  , utf8_encode($row['Red']))
                ->setCellValue('Y' . $idx  , utf8_encode($row['Acceso Medidor']))
                ->setCellValue('Z' . $idx  , utf8_encode($row['Tipo Acometida']))
                ->setCellValue('AA' . $idx  , utf8_encode($row['Acometida Compartida']))
                ->setCellValue('AB' . $idx  , utf8_encode($row['Localización Medidor']))
                ->setCellValue('AC' . $idx  , utf8_encode($row['Med Referencia']))
                ->setCellValue('AD' . $idx  , utf8_encode($row['Sellos']))
                ->setCellValue('AE' . $idx  , utf8_encode($row['Capa Trafo']))
                ->setCellValue('AF' . $idx  , utf8_encode($row['CD']))
                ->setCellValue('AG' . $idx  , utf8_encode($row['Punto Fisico']))
                ->setCellValue('AH' . $idx  , utf8_encode($row['Observaciones']))
                ->setCellValue('AI' . $idx  , utf8_encode($row['Obs Cliente']))
                ->setCellValue('AJ' . $idx  , utf8_encode($row['Obs Predio']))
                ->setCellValue('AK' . $idx  , utf8_encode($row['Cumple']))
                ->setCellValue('AL' . $idx  , utf8_encode($row['Cod Incumplimiento']))
                ->setCellValue('AM' . $idx  , utf8_encode($row['Frio/Sitio']))
                ->setCellValue('AN' . $idx  , utf8_encode($row['Ejecutado Por']));
              $idx++;
         }

         $objPHPExcel->getActiveSheet()->setTitle('Comercial');



                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="ParaConciliacion' . $Fecha . '.xlsx"');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');

                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0


          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
          //$objWriter->save();
          $objWriter->save('php://output');

            mysqli_free_result($result);  
?>