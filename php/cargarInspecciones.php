<?php
	include("conectar.php"); 
   $link = Conectar();

   $Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";

   $sql = "SELECT 
               * 
            FROM 
               Conciliacion_Inspecciones
            WHERE
               Conciliacion_Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta' AND Conciliacion_Inspecciones.Sucursal = 6000;";

   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
        public $idInspeccion;
        public $fechaIngreso;
        public $Actividad;
        public $Veredal;
        public $Cantidad;
        public $Adjudicado2015;
        public $Total;
        public $Nombre;
        public $Tipo;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->idInspeccion = utf8_encode($row['idInspeccion']);
            $Descripciones[$idx]->fechaIngreso = utf8_encode($row['fechaIngreso']);
            $Descripciones[$idx]->Actividad = utf8_encode($row['Actividad']);
            $Descripciones[$idx]->Veredal = utf8_encode($row['Veredal']);
            $Descripciones[$idx]->Cantidad = utf8_encode($row['Cantidad']);
            $Descripciones[$idx]->Adjudicado2015 = utf8_encode($row['Adjudicado2015']);
            $Descripciones[$idx]->Total = utf8_encode($row['Total']);
            $Descripciones[$idx]->Nombre = utf8_encode($row['Nombre']);
            $Descripciones[$idx]->Tipo = utf8_encode($row['Tipo']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>