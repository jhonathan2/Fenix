<?php
	include("conectar.php"); 
   $link = Conectar();

   $Fecha_Desde = addslashes($_POST['Desde']);
   $Fecha_Hasta = addslashes($_POST['Hasta']);
   
   $Nombre = addslashes($_POST['Nombre']);
   $Consecutivo = addslashes($_POST['Consecutivo']);
   $Descripcion = addslashes($_POST['Descripcion']);
   
   
   $sql = "SELECT 
            sisDocumental.idDocumento,
            sisDocumental.Fecha,
            sisDocumental.Tipo,
            sisDocumental.Consecutivo,
            sisDocumental.Descripcion,
            sisDocumental.Nombre,
            sisDocumental.ruta,
            DatosUsuarios.Nombre AS 'Usuario'
 FROM 
   sisDocumental, DatosUsuarios 
 WHERE
   sisDocumental.idLogin = DatosUsuarios.idLogin ";

   if ($Fecha_Desde <> "")
   {
      $sql .= " AND Fecha >= '$Fecha_Desde 00:00:00'";
   }
   if ($Fecha_Hasta <> "")
   {
      $sql .= " AND Fecha <= '$Fecha_Hasta 23:59:59'";
   }
   if ($Nombre <> "")
   {
      $sql .= " AND Nombre LIKE '%$Nombre%'";
   }
   if ($Consecutivo <> "")
   {
      $sql .= " AND Consecutivo LIKE '%$Consecutivo%'";
   }
   if ($Descripcion <> "")
   {
      $sql .= " AND Descripcion LIKE '%$Descripcion%'";
   }
   $sql .=";";
   
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Documento
      {
         public $idDocumento;
         public $Fecha;
         public $Tipo;
         public $Consecutivo;
         public $Descripcion;
         public $Nombre;
         public $Ruta;
         public $Usuario;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Documentos[$idx] = new Documento();
            $Documentos[$idx]->idDocumento = utf8_encode($row['idDocumento']);
            $Documentos[$idx]->Fecha = utf8_encode($row['Fecha']);
            $Documentos[$idx]->Tipo = utf8_encode($row['Tipo']);
            $Documentos[$idx]->Consecutivo = utf8_encode($row['Consecutivo']);
            $Documentos[$idx]->Descripcion = utf8_encode($row['Descripcion']);
            $Documentos[$idx]->Nombre = utf8_encode($row['Nombre']);
            $Documentos[$idx]->Ruta = utf8_encode($row['ruta']);
            $Documentos[$idx]->Usuario = utf8_encode($row['Usuario']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Documentos);   
   } else
   {
      echo 0;
   }
?>