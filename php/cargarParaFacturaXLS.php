<?php
	include("conectar.php"); 
  include("phpExcel/PHPExcel.php"); 

   $link = Conectar();

   $Desde = $_GET['Desde'] . " 00:00:00";
   $Hasta = $_GET['Hasta'] . " 23:59:59";
   $Anio = substr($Desde, 0, 4);
   $Fecha = substr($Desde, 0, 7);

   $sql = "SELECT * FROM (SELECT 
            SubZonas.Sede AS 'SubZona',
            Municipios.Nombre AS 'Municipio',
            (COUNT(*)*Baremo.Adjudicado$Anio) AS 'Total'
          FROM 
            Inspecciones 
            INNER JOIN Baremo ON Inspecciones.idBaremo = Baremo.idBaremo 
            LEFT JOIN Municipios ON Inspecciones.idMunicipio = Municipios.idMunicipio 
            LEFT JOIN SubZonas ON SubZonas.idSubZona = Municipios.idSubZona
          WHERE
            Inspecciones.Estado = 1
            AND Inspecciones.Sucursal = 6000
            AND Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
          GROUP BY 
              SubZonas.Sede,
              Municipios.Nombre
        UNION ALL
          SELECT 
            SubZonas.Sede AS 'SubZona',
            Municipios.Nombre AS 'Municipio',
            (COUNT(Inspecciones.idInspeccion) * Baremo.Adjudicado$Anio) AS 'Total'
          FROM 
            Inspecciones 
            LEFT JOIN Municipios ON Inspecciones.idMunicipio = Municipios.idMunicipio 
            LEFT JOIN SubZonas ON SubZonas.idSubZona = Municipios.idSubZona,
            Baremo 
          WHERE
            Inspecciones.Estado = 1
            AND Inspecciones.Sucursal = 6000
            AND Inspecciones.Veredal = 1
            AND Baremo.idBaremo = 15
            AND Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
          GROUP BY 
              SubZonas.Zona,
              SubZonas.Sede,
              Municipios.Nombre) AS Datos ORDER BY 1;";



            
//echo $sql;
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'SubZona')
                ->setCellValue('B1', 'Municipio')
                ->setCellValue('C1', 'Total');
      $idx = 2;
      
      $granTotal = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $idx  , utf8_encode($row['SubZona']))
                ->setCellValue('B'. $idx , utf8_encode($row['Municipio']))
                ->setCellValue('C'. $idx , $row['Total']);
            
            $granTotal = $granTotal + $row['Total'];

            $idx++;
         }
          $idx++;

          $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $idx  , 'SUBTOTAL FACTURA')
                ->setCellValue('C'. $idx , $granTotal);

            $idx++;

          $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B' . $idx  , 'IVA')
                ->setCellValue('C'. $idx , ($granTotal * 0.16));
          $idx++;

          $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $idx  , 'TOTAL FACTURA')
                ->setCellValue('C'. $idx , ($granTotal * 1.16));

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="ParaFactura' . $Fecha . '.xlsx"');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');

                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0

          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
          //$objWriter->save();
          $objWriter->save('php://output');

            mysqli_free_result($result);  
              
   } else
   {
      echo 0;
   }
?>