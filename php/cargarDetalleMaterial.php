<?php
	include("conectar.php"); 
   $link = Conectar();

   $idMaterial = addslashes($_POST['idMaterial']);

   $sql = "SELECT
                Materiales.idMaterial AS 'idMaterial',
                Materiales.Foto AS 'Foto',
                Materiales.Nombre AS 'Nombre',
                Materiales.Marca AS 'Marca',
                Materiales.Referencia AS 'Referencia',
                Materiales.FechaCompra AS 'FechaCompra',
                Materiales.FechaAsignacion AS 'FechaAsignacion',
                DatosUsuarios.idLogin AS 'idLogin',
                DatosUsuarios.Nombre AS 'Usuario',
                Materiales.Valor AS 'Valor',
                Materiales.Descripcion AS 'Descripcion',
                Materiales.Accesorios AS 'Accesorios',
                Materiales.Observaciones AS 'Observaciones',
                Materiales.Estado AS 'Estado',
                Materiales.idLoginRecibe AS 'idLoginRecibe',
                Materiales.ObservacionesRecibe AS 'detalleEntrega',
                Materiales.FechaRecibe AS 'fechaEntrega',
                Materiales.FotoRecibe AS 'imagenRecibe'
            FROM 
               Materiales,
               DatosUsuarios 
            WHERE
               Materiales.idLogin = DatosUsuarios.idLogin AND 
               Materiales.idMaterial = '$idMaterial';";

   $result = $link->query(utf8_decode($sql));

   if ( $result->num_rows > 0)
   {
      class Material
      {
         public $idMaterial;
         public $Foto;
         public $Nombre;
         public $Marca;
         public $Referencia;
         public $FechaCompra;
         public $FechaAsignacion;
         public $idLogin;
         public $Usuario;
         public $Valor;
         public $Descripcion;
         public $Accesorios;
         public $Observaciones;
         public $Estado;
         public $idLoginRecibe;
         public $Recibio;
         public $detalleEntrega;
         public $fechaEntrega;
         public $imagenRecibe;
      }
      
      $row =  $result->fetch_array(MYSQLI_ASSOC);

      $Materiales = new Material();
      $Materiales->idMaterial = utf8_encode($row['idMaterial']);
      $Materiales->Foto = utf8_encode($row['Foto']);
      $Materiales->Nombre = utf8_encode($row['Nombre']);
      $Materiales->Marca = utf8_encode($row['Marca']);
      $Materiales->Referencia = utf8_encode($row['Referencia']);
      $Materiales->FechaAsignacion = utf8_encode($row['FechaAsignacion']);
      $Materiales->FechaCompra = utf8_encode($row['FechaCompra']);
      $Materiales->idLogin = utf8_encode($row['idLogin']);
      $Materiales->Usuario = utf8_encode($row['Usuario']);
      $Materiales->Valor = utf8_encode($row['Valor']);
      $Materiales->Descripcion = utf8_encode($row['Descripcion']);
      $Materiales->Accesorios = utf8_encode($row['Accesorios']);
      $Materiales->Observaciones = utf8_encode($row['Observaciones']);
      $Materiales->Estado = utf8_encode($row['Estado']);
      $Materiales->detalleEntrega = utf8_encode($row['detalleEntrega']);
      $Materiales->fechaEntrega = utf8_encode($row['fechaEntrega']);
      $Materiales->imagenRecibe = utf8_encode($row['imagenRecibe']);
      $Materiales->idLoginRecibe = utf8_encode($row['idLoginRecibe']);

      $sql = "SELECT Nombre FROM DatosUsuarios WHERE idLogin = '" . $row['idLoginRecibe'] . "';";
      $result = $link->query(utf8_decode($sql));
      $row =  $result->fetch_array(MYSQLI_ASSOC);

      $Materiales->Recibio = utf8_encode($row['Nombre']);
        
            mysqli_free_result($result);  
            echo json_encode($Materiales);   
   } else
   {
      echo 0;
   }
?>