<?php
	include("conectar.php"); 
   $link = Conectar();

   $parametros = addslashes($_POST['parametros']);
   $criterio = addslashes($_POST['criterio']);

   $criterio = str_replace(" ", "%", $criterio);

   if ($parametros == "")
   {
      $condicion = "1";
   } else
   {
      $condicion = "";
      $tmpParametros = explode(", ", $parametros);
      foreach ($tmpParametros as $key => $value) 
      {
         if ($value != "")
         {
            if ($key > 0)
            {
               $condicion .= "OR $value LIKE '%$criterio%'";
            } else
            {
               $condicion = "$value LIKE '%$criterio%'";
            }
         }
      }
   }

   $sql = "SELECT
                Materiales.idMaterial AS 'idMaterial',
                Materiales.Foto AS 'Foto',
                Materiales.Nombre AS 'Nombre',
                Materiales.Marca AS 'Marca',
                Materiales.Referencia AS 'Referencia',
                Materiales.FechaAsignacion AS 'FechaAsignacion',
                Materiales.Estado AS 'Estado',
                DatosUsuarios.Nombre AS 'Usuario',
                DatosUsuarios.idLogin AS 'idLogin'
            FROM 
               Materiales,
               DatosUsuarios 
            WHERE
               Materiales.idLogin = DatosUsuarios.idLogin AND ($condicion)";
               
   $result = $link->query(utf8_decode($sql));

   if ( $result->num_rows > 0)
   {
      class Material
      {
         public $idMaterial;
         public $Foto;
         public $Nombre;
         public $Marca;
         public $Referencia;
         public $FechaAsignacion;
         public $Usuario;
         public $idLogin;
         public $Estado;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Materiales[$idx] = new Material();
            $Materiales[$idx]->idMaterial = utf8_encode($row['idMaterial']);
            $Materiales[$idx]->Foto = utf8_encode($row['Foto']);
            $Materiales[$idx]->Nombre = utf8_encode($row['Nombre']);
            $Materiales[$idx]->Marca = utf8_encode($row['Marca']);
            $Materiales[$idx]->Referencia = utf8_encode($row['Referencia']);
            $Materiales[$idx]->FechaAsignacion = utf8_encode($row['FechaAsignacion']);
            $Materiales[$idx]->Usuario = utf8_encode($row['Usuario']);
            $Materiales[$idx]->idLogin = utf8_encode($row['idLogin']);
            $Materiales[$idx]->Estado = utf8_encode($row['Estado']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Materiales);   
   } else
   {
      echo 0;
   }
?>