<?php
	include("../conectar.php"); 
   $link = Conectar();

   $idIpal = $_POST['idIpal'];

   $sql = "SELECT 
               Ipal.idInspeccion AS 'Consecutivo', 
               Ipal.idIpal AS 'idIpal', 
                Inspecciones.fechaIngreso AS 'Fecha',
                Ipal.Empresa AS 'Empresa', 
                Ipal.Direccion AS 'Direccion', 
                DatosUsuarios.idLogin AS 'idLogin', 
                DatosUsuarios.Nombre AS 'Usuario', 
                Ipal.NoContrato AS 'NoContrato', 
                Ipal.Observaciones AS 'Observaciones', 
                Ipal.resultado AS 'Resultado',
                Ipal.Celular AS 'Celular',
                Ipal.VehiculoTipo AS 'VehiculoTipo',
                Ipal.VehiculoPlaca AS 'VehiculoPlaca',
                Ipal.GruaPlaca AS 'GruaPlaca',
                Ipal.CanastaPlaca AS 'CanastaPlaca',
                Ipal.MotoPlaca AS 'MotoPlaca',
                Ipal.Trabajo AS 'Trabajo',
                Ipal.Proceso AS 'Proceso',
                Ipal.Zona AS 'Zona',
                Ipal.Cuadrilla AS 'Cuadrilla',
                Ipal.Cudrillero AS 'Cuadrillero',
                Ipal.Cuadrillero2 AS 'Cuadrillero2',
                Ipal.Prefijo AS 'Prefijo'
            FROM 
               Ipal,
                DatosUsuarios,
                Inspecciones
            WHERE
               Ipal.idLogin = DatosUsuarios.idLogin
                AND Ipal.idInspeccion = Inspecciones.idInspeccion
                AND Ipal.idIpal = '$idIpal';";

   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
         public $Consecutivo;
         public $idIpal;
         public $Fecha;
         public $Empresa;
         public $Direccion;
         public $idLogin;
         public $Usuario;
         public $NoContrato;
         public $Observaciones;
         public $Resultado;
         public $Celular;
         public $VehiculoTipo;
         public $VehiculoPlaca;
         public $GruaPlaca;
         public $CanastaPlaca;
         public $MotoPlaca;
         public $Trabajo;
         public $Proceso;
         public $Zona;
         public $Cuadrilla;
         public $Detalle;
         public $Recibio;
         public $CargoEmpresa;
         public $Prefijo;
      }

      class Detalle
      {
        public $Numeral;
        public $Descripcion;
        public $Resultado;
      }
        $row =  $result->fetch_array(MYSQLI_ASSOC);
      
            $Descripciones = new Descripcion();
            $Descripciones->Consecutivo = utf8_encode($row['Consecutivo']);
            $Descripciones->idIpal = utf8_encode($row['idIpal']);
            $Descripciones->Fecha = utf8_encode($row['Fecha']);
            $Descripciones->Empresa = utf8_encode($row['Empresa']);
            $Descripciones->Direccion = utf8_encode($row['Direccion']);
            $Descripciones->idLogin = utf8_encode($row['idLogin']);
            $Descripciones->Usuario = utf8_encode($row['Usuario']);
            $Descripciones->NoContrato = utf8_encode($row['NoContrato']);
            $Descripciones->Observaciones = utf8_encode($row['Observaciones']);
            $Descripciones->Resultado = utf8_encode($row['Resultado']);
            $Descripciones->Celular = utf8_encode($row['Celular']);
            $Descripciones->VehiculoTipo = utf8_encode($row['VehiculoTipo']);
            $Descripciones->VehiculoPlaca = utf8_encode($row['VehiculoPlaca']);
            $Descripciones->GruaPlaca = utf8_encode($row['GruaPlaca']);
            $Descripciones->CanastaPlaca = utf8_encode($row['CanastaPlaca']);
            $Descripciones->MotoPlaca = utf8_encode($row['MotoPlaca']);
            $Descripciones->Trabajo = utf8_encode($row['Trabajo']);
            $Descripciones->Proceso = utf8_encode($row['Proceso']);
            $Descripciones->Zona = utf8_encode($row['Zona']);
            $Descripciones->Cuadrilla = utf8_encode($row['Cuadrilla']);
            $Descripciones->Recibio = utf8_encode($row['Cuadrillero']);
            $Descripciones->CargoEmpresa = utf8_encode($row['Cuadrillero2']);
            $Descripciones->Prefijo = utf8_encode($row['Prefijo']);

            $idIpal =$Descripciones->idIpal;
        $sql = "SELECT IpalResultados.idResultado, IpalResultados.Numeral, IpalResultados.Resultado, Ipal_Items.Descripcion 
                  FROM IpalResultados
                  INNER JOIN Ipal_Items ON IpalResultados.Numeral = Ipal_Items.Numeral
                  WHERE  IpalResultados.idIpal = '$idIpal'
                  ORDER BY 
                    IpalResultados.Resultado ASC;";

       $result = $link->query(utf8_decode($sql));

       if ( $result->num_rows > 0)
       {
          $idx = 0;
             while ($row = mysqli_fetch_assoc($result))
             { 
                $Detalles[$idx] = new Detalle();
                $Detalles[$idx]->Numeral = utf8_encode($row['Numeral']);
                $Detalles[$idx]->Descripcion = utf8_encode($row['Descripcion']);
                $Detalles[$idx]->Resultado = utf8_encode($row['Resultado']);

                $idx++;
             }
             $Descripciones->Detalle = $Detalles;

       } else
       {
          $Descripciones->Detalle = 0;
       }
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
    }
    else
    {
      echo 0;
    }
?>
