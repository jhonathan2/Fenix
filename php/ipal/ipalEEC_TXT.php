<?php
  
    include("../conectar.php"); 
   $link = Conectar();

   /*$Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";*/

   $Desde = "2014-12-01 00:00:00";
   $Hasta = "2014-12-31 23:59:59";

   $sql = "SELECT 
               Ipal.idInspeccion AS 'Consecutivo', 
               Ipal.idIpal AS 'idIpal', 
                Inspecciones.fechaIngreso AS 'Fecha',
                Ipal.Empresa AS 'Empresa', 
                Ipal.Direccion AS 'Direccion', 
                Ipal.Celular AS 'Celular', 
                Ipal.VehiculoTipo AS 'VehiculoTipo', 
                Ipal.VehiculoPlaca AS 'VehiculoPlaca', 
                Ipal.GruaPlaca AS 'GruaPlaca', 
                Ipal.CanastaPlaca AS 'CanastaPlaca', 
                Ipal.MotoPlaca AS 'MotoPlaca', 
                Ipal.Trabajo AS 'Trabajo', 
                Ipal.Proceso AS 'Proceso', 
                DatosUsuarios.idLogin AS 'idLogin', 
                DatosUsuarios.Nombre AS 'Usuario', 
                Login.Usuario AS 'Cedula', 
                DatosUsuarios.IpalWeb AS 'IpalWeb', 
                Ipal.NoContrato AS 'NoContrato', 
                Ipal.Observaciones AS 'Observaciones', 
                Ipal.Zona AS 'Zona',
                Ipal.Cuadrilla AS 'Cuadrilla',
                Ipal.Cudrillero AS 'Recibio',
                Ipal.Cuadrillero2 AS 'CargoEmpresa',
                Ipal.resultado AS 'Resultado' 
            FROM 
               Ipal
                INNER JOIN DatosUsuarios ON Ipal.idLogin = DatosUsuarios.idLogin
                INNER JOIN Login ON Ipal.idLogin = Login.idLogin
                INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
            WHERE
               Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
               AND Inspecciones.Estado = 1
            ORDER BY
              Ipal.resultado DESC;";


   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Resultado
      {
        public $Numeral;
        public $Resultado;
        public $idPre;
      }

      $sql2 = "SELECT
                  IpalResultados.idIpal,
                  IpalResultados.Numeral,
                  IpalResultados.Resultado,
                  Ipal_Items.idPre
                FROM IpalResultados
                  INNER JOIN Ipal_Items ON IpalResultados.Numeral = Ipal_Items.Numeral
                  INNER JOIN Ipal ON Ipal.idIpal = IpalResultados.idIpal
                  INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
                WHERE
                  Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
                  AND Inspecciones.Estado = 1
                ORDER BY  
                  IpalResultados.idIpal,
                  Ipal_Items.idPre";

      $result2 = $link->query($sql2);

      $resultadosIpal = array();
      $tmpIpal = 0;
      $idx = 0;

      while ($row2 = mysqli_fetch_assoc($result2))
         {
            if ($tmpIpal <> $row2['idIpal'])
            {
              $idx = 0;
            } 
              $resultadosIpal[$row2['idIpal']][$idx] = new Resultado;

              $resultadosIpal[$row2['idIpal']][$idx]->Numeral = $row2['Numeral'];
              $resultadosIpal[$row2['idIpal']][$idx]->idPre = $row2['idPre'];
              $resultadosIpal[$row2['idIpal']][$idx]->Resultado = $row2['Resultado'];

              if ($row2['Resultado'] == "Si")
              {
                $resultadosIpal[$row2['idIpal']][$idx]->Resultado = "S";
              }

              if ($row2['Resultado'] == "No")
              {
                $resultadosIpal[$row2['idIpal']][$idx]->Resultado = "N";
              }

              $tmpIpal = $row2['idIpal'];
              $idx++;
         }


      
      $idx = 0;
      $tmpConsecutivo = 50;
      $tmpFecha = 0;

            $Salida = "IDDPT|IDFOR|IDFRM|ORDEN|IDPRE|IDREG|RESCAM1|RESCAM2|RESCAM3|FCHCAP|IDPER|CONSECUTIVO|SERIAL|IDOPE|FCHSIS|USRSIS|HOST|HLLPRC|EVALUACION|IDFORTER";

         while ($row = mysqli_fetch_assoc($result))
         { 
            if ($tmpFecha <> date('Y-m-d', strtotime($row['Fecha'])))
            {
              $tmpConsecutivo = 50;
            } else
            {
              $tmpConsecutivo++;
            }
            
            $guid = generar_guid() . "WSP";

            
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']). "|";
            $Salida .= 12 . "|";
            $Salida .= 0 . "|";
            $Salida .= 185 . "|";
            $Salida .= 0 . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha']))  . "|";
            $Salida .= "".  "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])). "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 1 . "|";
            $Salida .= 186 . "|";
            $Salida .= 0 . "|";
            $Salida .= date('H:i:s', strtotime($row['Fecha'])) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 2 . "|";
            $Salida .= 187 . "|";
            $Salida .= 0 . "|";
            $Salida .= $row['NoContrato'] . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 3 . "|";
            $Salida .= 188 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Direccion']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 4 . "|";
            $Salida .= 189 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Trabajo']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 5 . "|";
            $Salida .= 190 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['NoContrato']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 6 . "|";
            $Salida .= 191 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Recibio']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 7 . "|";
            $Salida .= 192 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['CargoEmpresa']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 8 . "|";
            $Salida .= 193 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Celular']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 9 . "|";
            $Salida .= 194 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['VehiculoTipo']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 10 . "|";
            $Salida .= 195 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['VehiculoPlaca']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 11 . "|";
            $Salida .= 196 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['GruaPlaca']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 12 . "|";
            $Salida .= 197 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['CanastaPlaca']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 13 . "|";
            $Salida .= 198 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['MotoPlaca']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= 14 . "|";
            $Salida .= 199 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Zona']) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $tmpCuadrilla = explode("_._", utf8_encode($row['Cuadrilla']));

            $idxTrabajador = 2;
            $idxOrden = 0;

            foreach ($tmpCuadrilla as $key => $value) 
            {
              if ($value <> "")
              {
                $tmpValue = explode(";", $value);

                $Salida .= 0 . "|";
                $Salida .= utf8_encode($row['Consecutivo']) . "|";
                $Salida .= 12 . "|";
                $Salida .= $idxOrden . "|";
                $Salida .= 200 . "|";
                $Salida .= $tmpTrabajador . "|";
                $Salida .= utf8_encode($tmpValue[0]) . "|";
                $Salida .= "" . "|";
                $Salida .= "" . "|";
                $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
                $Salida .= 0 . "|";
                $Salida .= 0 . "|";
                $Salida .= utf8_encode($row['IpalWeb']) . "|";
                $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
                $Salida .= $row['Fecha'] . "|";
                $Salida .= utf8_encode($row['IpalWeb']) . "|";
                $Salida .= "COL\COLEECAS10" . "|";
                $Salida .= 0 . "|";
                $Salida .= 0 . "|";
                $Salida .= $guid . "\n";

                $idxOrden++;

                $Salida .= 0 . "|";
                $Salida .= utf8_encode($row['Consecutivo']) . "|";
                $Salida .= 12 . "|";
                $Salida .= $idxOrden . "|";
                $Salida .= 201 . "|";
                $Salida .= $tmpTrabajador . "|";
                $Salida .= utf8_encode($tmpValue[1]) . "|";
                $Salida .= "" . "|";
                $Salida .= "" . "|";
                $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
                $Salida .= 0 . "|";
                $Salida .= 0 . "|";
                $Salida .= utf8_encode($row['IpalWeb']) . "|";
                $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
                $Salida .= $row['Fecha'] . "|";
                $Salida .= utf8_encode($row['IpalWeb']) . "|";
                $Salida .= "COL\COLEECAS10" . "|";
                $Salida .= 0 . "|";
                $Salida .= 0 . "|";
                $Salida .= $guid . "\n";


                $idxOrden++;

                $Salida .= 0 . "|";
                $Salida .= utf8_encode($row['Consecutivo']) . "|";
                $Salida .= 12 . "|";
                $Salida .= $idxOrden . "|";
                $Salida .= 202 . "|";
                $Salida .= $tmpTrabajador . "|";
                $Salida .= utf8_encode($tmpValue[2]) . "|";
                $Salida .= "" . "|";
                $Salida .= "" . "|";
                $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
                $Salida .= 0 . "|";
                $Salida .= 0 . "|";
                $Salida .= utf8_encode($row['IpalWeb']) . "|";
                $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
                $Salida .= $row['Fecha'] . "|";
                $Salida .= utf8_encode($row['IpalWeb']) . "|";
                $Salida .= "COL\COLEECAS10" . "|";
                $Salida .= 0 . "|";
                $Salida .= 0 . "|";
                $Salida .= $guid . "\n";

                $idxOrden++;
                $tmpTrabajador++;

              }
            }

            foreach ($resultadosIpal[$row['idIpal']] as $key => $value) 
            {
                $Salida .= 0 . "|";
                $Salida .= utf8_encode($row['Consecutivo']) . "|";
                $Salida .= 12 . "|";
                $Salida .= $idxOrden . "|";
                $Salida .= $value->idPre . "|";
                $Salida .= 0 . "|";
                $Salida .= $value->Resultado . "|";
                $Salida .= "" . "|";
                $Salida .= "" . "|";
                $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
                $Salida .= 0 . "|";
                $Salida .= 0 . "|";
                $Salida .= utf8_encode($row['IpalWeb']) . "|";
                $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
                $Salida .= $row['Fecha'] . "|";
                $Salida .= utf8_encode($row['IpalWeb']) . "|";
                $Salida .= "COL\COLEECAS10" . "|";
                $Salida .= 0 . "|";
                $Salida .= 1 . "|";
                $Salida .= $guid . "\n";


                $idx++;
                $idxOrden++;
            }

            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['Consecutivo']) . "|";
            $Salida .= 12 . "|";
            $Salida .= $idxOrden . "|";
            $Salida .= 624 . "|";
            $Salida .= 0 . "|";
            $Salida .= str_replace("\r", " ",str_replace("\n", " ", utf8_encode($row['Observaciones']))) . "|";
            $Salida .= "" . "|";
            $Salida .= "" . "|";
            $Salida .= date('Y-m-d', strtotime($row['Fecha'])) . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo . "|";
            $Salida .= $row['Fecha'] . "|";
            $Salida .= utf8_encode($row['IpalWeb']) . "|";
            $Salida .= "COL\COLEECAS10" . "|";
            $Salida .= 0 . "|";
            $Salida .= 0 . "|";
            $Salida .= $guid . "\n";

            $tmpFecha = date('Y-m-d', strtotime($row['Fecha']));

         }
         
            mysqli_free_result($result);  
            mysqli_free_result($result2);  
            
            header("Content-type: text/plain");
            header( "Content-disposition: attachment; filename=IpalWeb_".$Desde. "_" . $Hasta . ".txt");
            header("Pragma: no-cache");
            header("Expires: 0");
            print strtoupper($Salida);
            exit;
   } else
   {
      echo 0;
   }
  
  function generar_guid()
  {
    mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
          $charid = strtoupper(md5(uniqid(rand(), true)));
          $hyphen = chr(45);// "-"
          $uuid = substr($charid, 0, 8).$hyphen
                  .substr($charid, 8, 4).$hyphen
                  .substr($charid,12, 4).$hyphen
                  .substr($charid,16, 4).$hyphen
                  .substr($charid,20,12);
          return $uuid;
  }
?>