<?php
  
 function IpalTrabajador($Desde, $Hasta)
{
    //include("../../conectar.php"); 
   $link = Conectar();

   /*$Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";*/

/*   $Desde = "2015-02-01 00:00:00";
   $Hasta = "2015-02-26 23:59:59";*/

   $sql = "SELECT 
                Ipal.idInspeccion AS 'idInspeccion',
                Ipal.Cuadrilla AS 'Cuadrilla' 
            FROM 
               Ipal
                INNER JOIN DatosUsuarios ON Ipal.idLogin = DatosUsuarios.idLogin
                INNER JOIN Login ON Ipal.idLogin = Login.idLogin
                INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
                LEFT JOIN ipalesEnviados ON ipalesEnviados.Prefijo = Inspecciones.Prefijo
            WHERE
               Inspecciones.fechaIngreso <= '$Hasta'
               AND ipalesEnviados.Prefijo IS NULL
               AND Inspecciones.Estado = 1
               AND Inspecciones.Sucursal = 6000
               AND Ipal.resultado = 'Cumple'
               AND Ipal.Cuadrilla <> ''
            ORDER BY
              Ipal.resultado DESC,
              Ipal.idInspeccion;";

   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      $idx = 0;
            $Salida = "ID|CEDULA|NOMBRE_APELLIDO|FUNCION_CARGO|SECUENCIA\n";

         while ($row = mysqli_fetch_assoc($result))
         { 
            $tmpCuadrilla = explode("_._", utf8_encode($row['Cuadrilla']));

            $idxOrden = 1;

            foreach ($tmpCuadrilla as $key => $value) 
            {
              if ($value <> "")
              {
                $tmpValue = explode(";", $value);

                $Salida .= $row['idInspeccion'] . "|";
                $Salida .= utf8_decode($tmpValue[0]) . "|";
                $Salida .= utf8_decode($tmpValue[1]) . "|";
                $Salida .= utf8_decode($tmpValue[2]) . "|";
                $Salida .= $idxOrden . "\n";

                $idxOrden++;
              }
            }

         }
         
            mysqli_free_result($result);  
           /*
            header("Content-type: text/plain");
            header( "Content-disposition: attachment; filename=IpalTrabajador_".$Desde. "_" . $Hasta . ".txt");
            header("Pragma: no-cache");
            header("Expires: 0");
            print strtoupper($Salida);
            exit;
            */
            if (!file_exists("IpalesEnviados/" . date("Y")))
            {
              mkdir("IpalesEnviados/" . date("Y"));
            }
            if (!file_exists("IpalesEnviados/" . date("Y/m")))
            {
              mkdir("IpalesEnviados/" . date("Y/m"));
            }
            if (!file_exists("IpalesEnviados/" . date("Y/m/d")))
            {
              mkdir("IpalesEnviados/" . date("Y/m/d"));
            }
            $ruta = "IpalesEnviados/" . date("Y/m/d/");
            
            $nombre ="ipal_trabajador_t.txt";
            $fp = fopen($ruta . $nombre, 'w');
            fwrite($fp, $Salida);
            fclose($fp);

            $fecha = date ( 'Y-m-d H:i:s');

            $sql = "INSERT INTO ipalesEnviados (Prefijo, fechaEnvio) 
              SELECT 
               Ipal.Prefijo AS 'Consecutivo',
               '$fecha' AS fecha
            FROM 
               Ipal
                INNER JOIN DatosUsuarios ON Ipal.idLogin = DatosUsuarios.idLogin
                INNER JOIN Login ON Ipal.idLogin = Login.idLogin
                INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
                LEFT JOIN SubZonas ON Ipal.Zona = SubZonas.idSubZona
                LEFT JOIN ipalesEnviados ON ipalesEnviados.Prefijo = Inspecciones.Prefijo
            WHERE
               Inspecciones.fechaIngreso <= '$Hasta'
               AND ipalesEnviados.Prefijo IS NULL
               AND Inspecciones.Estado = 1
               AND Ipal.resultado = 'Cumple'
               AND Ipal.Cuadrilla <> ''
            ORDER BY
              Ipal.Prefijo;";
              $link->query($sql);

            return $ruta . $nombre;
   } else
   {
      return 0;
   }
}
?>