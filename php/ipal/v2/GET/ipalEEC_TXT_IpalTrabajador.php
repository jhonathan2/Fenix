<?php
  
    include("../../../conectar.php"); 
   $link = Conectar();

   $Desde = $_GET['Desde'] . " 00:00:00";
   $Hasta = $_GET['Hasta'] . " 23:59:59";


   $sql = "SELECT 
                Ipal.idInspeccion AS 'idInspeccion',
                Ipal.Cuadrilla AS 'Cuadrilla' 
            FROM 
               Ipal
                INNER JOIN DatosUsuarios ON Ipal.idLogin = DatosUsuarios.idLogin
                INNER JOIN Login ON Ipal.idLogin = Login.idLogin
                INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
            WHERE
               Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
               AND Inspecciones.Estado = 1
               AND Inspecciones.Sucursal = 6000 
               AND Ipal.resultado = 'Cumple'
               AND Ipal.Cuadrilla <> ''
            ORDER BY
              Ipal.resultado DESC,
              Ipal.idInspeccion;";


   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      $idx = 0;
            $Salida = "ID|CEDULA|NOMBRE_APELLIDO|FUNCION_CARGO|SECUENCIA\n";

         while ($row = mysqli_fetch_assoc($result))
         { 
            $tmpCuadrilla = explode("_._", utf8_encode($row['Cuadrilla']));

            $idxOrden = 1;

            foreach ($tmpCuadrilla as $key => $value) 
            {
              if ($value <> "")
              {
                $tmpValue = explode(";", $value);

                $Salida .= $row['idInspeccion'] . "|";
                $Salida .= utf8_decode($tmpValue[0]) . "|";
                $Salida .= utf8_decode($tmpValue[1]) . "|";
                $Salida .= utf8_decode($tmpValue[2]) . "|";
                $Salida .= $idxOrden . "\n";

                $idxOrden++;
              }
            }

         }
         
            mysqli_free_result($result);  
           /*
            header("Content-type: text/plain");
            header( "Content-disposition: attachment; filename=IpalTrabajador_".$Desde. "_" . $Hasta . ".txt");
            header("Pragma: no-cache");
            header("Expires: 0");
            print strtoupper($Salida);
            exit;
            */
            $nombre = "IpalesEnviados/IpalTrabajador_".$Desde. "_" . $Hasta . ".txt";
            $fp = fopen($nombre, 'w');
            fwrite($fp, $Salida);
            fclose($fp);
            return $nombre;
   } else
   {
      return 0;
   }
?>