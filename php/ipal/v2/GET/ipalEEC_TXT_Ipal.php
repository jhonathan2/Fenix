<?php

    $Zona = array();
    $Zona[10] = 'Bogotá';
    $Zona[8] = 'Chocontá';
    $Zona[6] = 'Cáqueza';
    $Zona[2] = 'Facatativá';
    $Zona[5] = 'Fusagasugá';
    $Zona[7] = 'Gachetá';
    $Zona[3] = 'Girardot';
    $Zona[4] = 'La Mesa';
    $Zona[1] = 'Pto Salgar';
    $Zona[9] = 'Ubaté';
    $Zona[0] = 'Villeta';
  
    include("../../../conectar.php"); 
   $link = Conectar();

   $Desde = $_GET['Desde'] . " 00:00:00";
   $Hasta = $_GET['Hasta'] . " 23:59:59";

   /*$Desde = "2015-02-01 00:00:00";
   $Hasta = "2015-02-09 23:59:59";*/
   /*$Desde = "2015-02-01 00:00:00";
   $Hasta = "2015-02-26 23:59:59";*/

   $sql = "SELECT 
               Ipal.idInspeccion AS 'Consecutivo', 
                DATE_FORMAT(Inspecciones.fechaIngreso, '%d/%m/%Y') AS 'Fecha',
                DATE_FORMAT(Inspecciones.fechaIngreso, '%T') AS 'Hora',
                Ipal.Empresa AS 'Empresa', 
                Ipal.Direccion AS 'Direccion', 
                Ipal.Trabajo AS 'Trabajo', 
                Ipal.NoContrato AS 'NoContrato', 
                Ipal.Cudrillero AS 'Jefe_Cuadrilla',
                Ipal.Proceso AS 'Proceso', 
                Ipal.Celular AS 'Celular', 
                Ipal.VehiculoTipo AS 'VehiculoTipo', 
                Ipal.VehiculoPlaca AS 'VehiculoPlaca', 
                Ipal.GruaPlaca AS 'GruaPlaca', 
                Ipal.CanastaPlaca AS 'CanastaPlaca', 
                Ipal.MotoPlaca AS 'MotoPlaca', 
                SubZonas.idSubZona AS 'Zona',
                Ipal.Observaciones AS 'Observaciones'
            FROM 
               Ipal
                INNER JOIN DatosUsuarios ON Ipal.idLogin = DatosUsuarios.idLogin
                INNER JOIN Login ON Ipal.idLogin = Login.idLogin
                INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
                LEFT JOIN SubZonas ON Ipal.Zona = SubZonas.idSubZona
            WHERE
               Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
               AND Inspecciones.Estado = 1
               AND Inspecciones.Sucursal = 6000
               AND Ipal.resultado = 'Cumple'
               AND Ipal.Cuadrilla <> ''
            ORDER BY
              Ipal.resultado DESC,
              Ipal.idIpal;";

   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
            $Salida = "ID|FECHA|HORA|EMPRESA|DIRECCION|TRABAJO_REALIZAR|NUMERO_CONTRACTO|JEFE_CUADRILLA|PROCESO|CELULAR|TIPO_VEHICULO|PLACA_VEHICULO|PLACA_GRUA|PLACA_CANASTA|PLACA_MOTO|ZONA_EEC|OBS_GENERAL\n";

         while ($row = mysqli_fetch_assoc($result))
         { 
            $NoContrato = "";
            foreach ($row as $key => $value) 
            {
                if ($value == "")
                {
                    $row[$key] = "NA";
                }
                    $row[$key] = str_replace("\n"," ",$row[$key]);
                    $row[$key] = str_replace("\r"," ",$row[$key]);
                if ($key == "Zona")
                {
                    $row[$key] = utf8_decode(ucfirst(strtolower($Zona[$row[$key]])));
                } else
                {
                    $row[$key] = strtoupper($row[$key]);
                }

                if ($key == "Empresa")
                {
                    if ($row[$key] == "CAM")
                    {
                        $NoContrato = "5600000320";
                    }
                    elseif ($row[$key] == "MEC")
                    {
                        $NoContrato = "5700005503";
                    }
                    elseif ($row[$key] == "MICOL")
                    {
                        $NoContrato = "5800005328";
                    }
                    else
                    {
                        $NoContrato = "5600000320";
                    }
                    $Salida .= $NoContrato;
                } else
                {
                    if ($key == "NoContrato")
                    {
                        $Salida .= $NoContrato;  
                    } else
                    {
                        $Salida .= $row[$key];
                    }
                }

                if ($key <> "Observaciones")
                {
                    $Salida .= "|";
                }
            }
            $Salida .= "\n";
         }
         
            mysqli_free_result($result);  
            
            /*
            header("Content-type: text/plain");
            header( "Content-disposition: attachment; filename=Ipal_".$Desde. "_" . $Hasta . ".txt");
            header("Pragma: no-cache");
            header("Expires: 0");
            print $Salida;
            exit;
            */
            $nombre = "IpalesEnviados/Ipal_".$Desde. "_" . $Hasta . ".txt";
            $fp = fopen($nombre, 'w');
            fwrite($fp, $Salida);
            fclose($fp);
            return $nombre;
   } else
   {
      return 0;
   }
 ?>