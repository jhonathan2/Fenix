<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Prefijo = $_POST['Prefijo'];

   $sql = "SELECT 
               Ipal.*
            FROM 
               Ipal
            WHERE
               Prefijo = '$Prefijo';";


   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
        public $idIpal;
        public $idInspeccion;
        public $idLogin;
        public $resultado;
        public $Empresa;
        public $Direccion;
        public $Celular;
        public $VehiculoTipo;
        public $VehiculoPlaca;
        public $GruaPlaca;
        public $CanastaPlaca;
        public $MotoPlaca;
        public $NoContrato;
        public $Trabajo;
        public $Proceso;
        public $Zona;
        public $Cuadrilla;
        public $Prefijo;
        public $Observaciones;
        public $Cudrillero;
        public $Cuadrillero2;
        public $itemsEvaluados;
      }
      
      $idx = 0;
      $idIpal = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $idIpal = utf8_encode($row['idIpal']);
            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->idIpal = utf8_encode($row['idIpal']);
            $Descripciones[$idx]->idInspeccion = utf8_encode($row['idInspeccion']);
            $Descripciones[$idx]->idLogin = utf8_encode($row['idLogin']);
            $Descripciones[$idx]->resultado = utf8_encode($row['resultado']);
            $Descripciones[$idx]->Empresa = utf8_encode($row['Empresa']);
            $Descripciones[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Descripciones[$idx]->Celular = utf8_encode($row['Celular']);
            $Descripciones[$idx]->VehiculoTipo = utf8_encode($row['VehiculoTipo']);
            $Descripciones[$idx]->VehiculoPlaca = utf8_encode($row['VehiculoPlaca']);
            $Descripciones[$idx]->GruaPlaca = utf8_encode($row['GruaPlaca']);
            $Descripciones[$idx]->CanastaPlaca = utf8_encode($row['CanastaPlaca']);
            $Descripciones[$idx]->MotoPlaca = utf8_encode($row['MotoPlaca']);
            $Descripciones[$idx]->NoContrato = utf8_encode($row['NoContrato']);
            $Descripciones[$idx]->Trabajo = utf8_encode($row['Trabajo']);
            $Descripciones[$idx]->Proceso = utf8_encode($row['Proceso']);
            $Descripciones[$idx]->Zona = utf8_encode($row['Zona']);
            $Descripciones[$idx]->Cuadrilla = utf8_encode($row['Cuadrilla']);
            $Descripciones[$idx]->Prefijo = utf8_encode($row['Prefijo']);
            $Descripciones[$idx]->Observaciones = utf8_encode($row['Observaciones']);
            $Descripciones[$idx]->Cudrillero = utf8_encode($row['Cudrillero']);
            $Descripciones[$idx]->Cuadrillero2 = utf8_encode($row['Cuadrillero2']);

            $idx++;
         }

         $sql = "SELECT 
                  IpalResultados.idResultado, 
                  IpalResultados.Numeral, 
                  IpalResultados.Resultado, 
                  IpalResultados.Observaciones, 
                  Ipal_Items.Descripcion 
                FROM 
                  IpalResultados 
                  INNER JOIN Ipal_Items ON Ipal_Items.Numeral = IpalResultados.Numeral
                WHERE IpalResultados.idIpal = '" . $idIpal . "';";

         class ipalResultados
         {
          public $idResultado;
          public $Numeral;
          public $Descripcion;
          public $Resultado;
          public $Observaciones;
         }
         $result = $link->query($sql);
         $idy = 0;
         
         while ($row = mysqli_fetch_assoc($result))
         { 
          $Resultados[$idy] = new ipalResultados();
          $Resultados[$idy]->idResultado = utf8_encode($row['idResultado']);
          $Resultados[$idy]->Numeral = utf8_encode($row['Numeral']);
          $Resultados[$idy]->Descripcion = utf8_encode($row['Descripcion']);
          $Resultados[$idy]->Resultado = utf8_encode($row['Resultado']);
          $Resultados[$idy]->Observaciones = utf8_encode($row['Observaciones']);
          $idy++;
         }

         $Descripciones[0]->itemsEvaluados = $Resultados;

            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>