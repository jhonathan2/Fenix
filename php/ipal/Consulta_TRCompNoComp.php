<?php
    include_once "../Include/VarGlobales.PHP";
    include_once "../Include/BdSqlClases.php";

     $q = new ConecteMysql($ServidorBD,$UsrBD,$ClaveBD,$NomBD);
    $Desde = $_POST['fechaDesde'];
    $Hasta = $_POST['fechaHasta'];
    $TipoGrafZona = $_POST['TipoGrafZona'];
    // $Directriz = $_POST['directriz'];
    // $incMaterial = $_POST['incMaterial'];

    $Condiciones = "";
    if ($Desde <> "")
    {
      $Condiciones .= " AND P.Acc_FechaAcc >= '$Desde'";
    }
    if ($Hasta <> "")
    {
      $Condiciones .= " AND P.Acc_FechaAcc <= '$Hasta'";
    }
     
    // if ($incMaterial <> "")
    // {
    //   $Condiciones .= " AND P.Acc_IncMaterial = '$incMaterial'";
    // }

  $sql="SELECT 
  TRIESGO,
  COALESCE ([COMPUTABLE], 0) Computable,
  COALESCE ([NO COMPUTABLE], 0) NoComputable
FROM (
    SELECT       
      TR.Acc_Descripcion_Tipo_Riesgo 'TRIESGO',
      P.Acc_Directriz  'DIRECTRIZ'
    FROM 
      Acc_Principal P, 
      Acc_Tipo_Riesgo TR,
      Acc_Forma_Accidente FA
    WHERE 
      P.ID_TRIESGO = FA.ID_TRIESGO
      AND FA.idAcc_Tipo_Riesgo = TR.idAcc_Tipo_Riesgo
      AND (P.Acc_Directriz = 'COMPUTABLE' OR P.Acc_Directriz = 'NO COMPUTABLE')
      $Condiciones
    GROUP BY 
      P.acc_id_general, 
      TR.Acc_Descripcion_Tipo_Riesgo, 
      P.Acc_Directriz
      ) SOURCE PIVOT 
          (COUNT(SOURCE.DIRECTRIZ) FOR SOURCE.DIRECTRIZ IN ([COMPUTABLE],  [NO COMPUTABLE])) AS PIVOTABLE 
order by TRIESGO;";
  
  $q->ejecutar($sql, 36, "Consulta_TRCompNoCompNoComp.php");
  
  class Respuesta
  {
  public $TRCompNoComp;
  public $Computables;
  public $NoComputables;
  }
  
  if ($q->filas() > 0)
  {     
    $idx = 0; 
    while($q->Cargar())
    {
        $Respuestas[$idx] = new Respuesta();
        $Respuestas[$idx]->TRCompNoComp = $q->dato(0);
        $Respuestas[$idx]->Computables= $q->dato(1);
        $Respuestas[$idx]->NoComputables= $q->dato(2);
        $idx++;
    }
  
    echo json_encode($Respuestas);
  } else
  {
    echo 0;
  }
?>