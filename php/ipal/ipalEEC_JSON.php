<?php
  
    include("../conectar.php"); 
   $link = Conectar();

   /*$Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";*/

   $Desde = "2014-12-01 00:00:00";
   $Hasta = "2014-12-01 23:59:59";

   $sql = "SELECT 
               Ipal.idInspeccion AS 'Consecutivo', 
               Ipal.idIpal AS 'idIpal', 
                Inspecciones.fechaIngreso AS 'Fecha',
                Ipal.Empresa AS 'Empresa', 
                Ipal.Direccion AS 'Direccion', 
                Ipal.Celular AS 'Celular', 
                Ipal.VehiculoTipo AS 'VehiculoTipo', 
                Ipal.VehiculoPlaca AS 'VehiculoPlaca', 
                Ipal.GruaPlaca AS 'GruaPlaca', 
                Ipal.CanastaPlaca AS 'CanastaPlaca', 
                Ipal.MotoPlaca AS 'MotoPlaca', 
                Ipal.Trabajo AS 'Trabajo', 
                Ipal.Proceso AS 'Proceso', 
                DatosUsuarios.idLogin AS 'idLogin', 
                DatosUsuarios.Nombre AS 'Usuario', 
                Login.Usuario AS 'Cedula', 
                DatosUsuarios.IpalWeb AS 'IpalWeb', 
                Ipal.NoContrato AS 'NoContrato', 
                Ipal.Observaciones AS 'Observaciones', 
                Ipal.Zona AS 'Zona',
                Ipal.Cuadrilla AS 'Cuadrilla',
                Ipal.Cudrillero AS 'Recibio',
                Ipal.Cuadrillero2 AS 'CargoEmpresa',
                Ipal.resultado AS 'Resultado' 
            FROM 
               Ipal
                INNER JOIN DatosUsuarios ON Ipal.idLogin = DatosUsuarios.idLogin
                INNER JOIN Login ON Ipal.idLogin = Login.idLogin
                INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
            WHERE
               Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
               AND Inspecciones.Estado = 1
            ORDER BY
              Ipal.resultado DESC;";


   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
         public $IDDPT; //0
         public $IDFOR; /*Consecutivo*/
         public $IDFRM; //12
         public $ORDEN; /**/
         public $IDPRE; /**/
         public $IDREG;
         public $RESCAM1;
         public $RESCAM2; //
         public $RESCAM3; // 
         public $FCHCAP; /*Fecha*/
         public $IDPER; //0
         public $CONSECUTIVO; //0
         public $SERIAL;  /* = USRSIS*/
         public $IDOPE;
         public $FCHSIS;
         public $USRSIS; /*Usuario*/
         public $HOST; //COL\COLEECAS10
         public $HLLPRC; //0
         public $EVALUACION;
         public $IDFORTER; //com_create_guid . "wsp"
      }

      class Resultado
      {
        public $Numeral;
        public $Resultado;
        public $idPre;
      }

      $sql2 = "SELECT
                  IpalResultados.idIpal,
                  IpalResultados.Numeral,
                  IpalResultados.Resultado,
                  Ipal_Items.idPre
                FROM IpalResultados
                  INNER JOIN Ipal_Items ON IpalResultados.Numeral = Ipal_Items.Numeral
                  INNER JOIN Ipal ON Ipal.idIpal = IpalResultados.idIpal
                  INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion
                WHERE
                  Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
                  AND Inspecciones.Estado = 1
                ORDER BY  
                  IpalResultados.idIpal,
                  Ipal_Items.idPre";

      $result2 = $link->query($sql2);

      $resultadosIpal = array();
      $tmpIpal = 0;
      $idx = 0;

      while ($row2 = mysqli_fetch_assoc($result2))
         {
            if ($tmpIpal <> $row2['idIpal'])
            {
              $idx = 0;
            } 
              $resultadosIpal[$row2['idIpal']][$idx] = new Resultado;

              $resultadosIpal[$row2['idIpal']][$idx]->Numeral = $row2['Numeral'];
              $resultadosIpal[$row2['idIpal']][$idx]->idPre = $row2['idPre'];
              $resultadosIpal[$row2['idIpal']][$idx]->Resultado = $row2['Resultado'];

              if ($row2['idPre'] == "Si")
              {
                $resultadosIpal[$row2['idIpal']][$idx]->Resultado = "S";
              }

              if ($row2['idPre'] == "No")
              {
                $resultadosIpal[$row2['idIpal']][$idx]->Resultado = "N";
              }

              $tmpIpal = $row2['idIpal'];
              $idx++;
         }


      
      $idx = 0;
      $tmpConsecutivo = 50;
      $tmpFecha = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            if ($tmpFecha <> date('Y-m-d', strtotime($row['Fecha'])))
            {
              $tmpConsecutivo = 50;
            } else
            {
              $tmpConsecutivo++;
            }
            $guid = generar_guid() . "WSP";
            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 0;
            $Descripciones[$idx]->IDPRE = 185;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = date('Y-m-d', strtotime($row['Fecha']));

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 1;
            $Descripciones[$idx]->IDPRE = 186;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = date('H:i:s', strtotime($row['Fecha']));

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 2;
            $Descripciones[$idx]->IDPRE = 187;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = $row['NoContrato'];

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 3;
            $Descripciones[$idx]->IDPRE = 188;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['Direccion']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 4;
            $Descripciones[$idx]->IDPRE = 189;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['Trabajo']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 5;
            $Descripciones[$idx]->IDPRE = 190;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['NoContrato']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 6;
            $Descripciones[$idx]->IDPRE = 191;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['Recibio']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 7;
            $Descripciones[$idx]->IDPRE = 192;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['CargoEmpresa']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 8;
            $Descripciones[$idx]->IDPRE = 193;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['Celular']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 9;
            $Descripciones[$idx]->IDPRE = 194;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['VehiculoTipo']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 10;
            $Descripciones[$idx]->IDPRE = 195;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['VehiculoPlaca']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 11;
            $Descripciones[$idx]->IDPRE = 196;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['GruaPlaca']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 12;
            $Descripciones[$idx]->IDPRE = 197;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['CanastaPlaca']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 13;
            $Descripciones[$idx]->IDPRE = 198;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['MotoPlaca']);

            $idx++;

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = 14;
            $Descripciones[$idx]->IDPRE = 199;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = utf8_encode($row['Zona']);

            $idx++;



            $tmpCuadrilla = explode("_._", utf8_encode($row['Cuadrilla']));

            $idxTrabajador = 2;
            $idxOrden = 0;

            foreach ($tmpCuadrilla as $key => $value) 
            {
              if ($value <> "")
              {
                $tmpValue = explode(";", $value);

                $Descripciones[$idx] = new Descripcion();
                $Descripciones[$idx]->IDDPT = 0;
                $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
                $Descripciones[$idx]->IDFRM = 12;
                $Descripciones[$idx]->RESCAM2 = "";
                $Descripciones[$idx]->RESCAM3 = "";
                $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
                $Descripciones[$idx]->IDPER = 0;
                $Descripciones[$idx]->CONSECUTIVO = 0;
                $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
                $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
                $Descripciones[$idx]->FCHSIS = $row['Fecha'];
                $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
                $Descripciones[$idx]->HOST = "COL\COLEECAS10";
                $Descripciones[$idx]->HLLPRC = 0;
                $Descripciones[$idx]->IDFORTER = $guid;

                $Descripciones[$idx]->EVALUACION = 0;
                $Descripciones[$idx]->ORDEN = $idxOrden;
                $Descripciones[$idx]->IDPRE = 200;
                $Descripciones[$idx]->IDREG = $tmpTrabajador;
                $Descripciones[$idx]->RESCAM1 = utf8_encode($tmpValue[0]);

                $idx++;
                $idxOrden++;

                 $Descripciones[$idx] = new Descripcion();
                $Descripciones[$idx]->IDDPT = 0;
                $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
                $Descripciones[$idx]->IDFRM = 12;
                $Descripciones[$idx]->RESCAM2 = "";
                $Descripciones[$idx]->RESCAM3 = "";
                $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
                $Descripciones[$idx]->IDPER = 0;
                $Descripciones[$idx]->CONSECUTIVO = 0;
                $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
                $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
                $Descripciones[$idx]->FCHSIS = $row['Fecha'];
                $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
                $Descripciones[$idx]->HOST = "COL\COLEECAS10";
                $Descripciones[$idx]->HLLPRC = 0;
                $Descripciones[$idx]->IDFORTER = $guid;

                $Descripciones[$idx]->EVALUACION = 0;
                $Descripciones[$idx]->ORDEN = $idxOrden;
                $Descripciones[$idx]->IDPRE = 201;
                $Descripciones[$idx]->IDREG = $tmpTrabajador;
                $Descripciones[$idx]->RESCAM1 = utf8_encode($tmpValue[1]);

                $idx++;
                $idxOrden++;

                $Descripciones[$idx] = new Descripcion();
                $Descripciones[$idx]->IDDPT = 0;
                $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
                $Descripciones[$idx]->IDFRM = 12;
                $Descripciones[$idx]->RESCAM2 = "";
                $Descripciones[$idx]->RESCAM3 = "";
                $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
                $Descripciones[$idx]->IDPER = 0;
                $Descripciones[$idx]->CONSECUTIVO = 0;
                $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
                $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
                $Descripciones[$idx]->FCHSIS = $row['Fecha'];
                $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
                $Descripciones[$idx]->HOST = "COL\COLEECAS10";
                $Descripciones[$idx]->HLLPRC = 0;
                $Descripciones[$idx]->IDFORTER = $guid;

                $Descripciones[$idx]->EVALUACION = 0;
                $Descripciones[$idx]->ORDEN = $idxOrden;
                $Descripciones[$idx]->IDPRE = 202;
                $Descripciones[$idx]->IDREG = $tmpTrabajador;
                $Descripciones[$idx]->RESCAM1 = utf8_encode($tmpValue[2]);

                $idx++;
                $idxOrden++;
                $tmpTrabajador++;

              }
            }
/*
            foreach ($resultadosIpal[$row['idIpal']] as $key => $value) 
            {
              $Descripciones[$idx] = new Descripcion();
                $Descripciones[$idx]->IDDPT = 0;
                $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
                $Descripciones[$idx]->IDFRM = 12;
                $Descripciones[$idx]->RESCAM2 = "";
                $Descripciones[$idx]->RESCAM3 = "";
                $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
                $Descripciones[$idx]->IDPER = 0;
                $Descripciones[$idx]->CONSECUTIVO = 0;
                $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
                $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
                $Descripciones[$idx]->FCHSIS = $row['Fecha'];
                $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
                $Descripciones[$idx]->HOST = "COL\COLEECAS10";
                $Descripciones[$idx]->HLLPRC = 0;
                $Descripciones[$idx]->IDFORTER = $guid;

                $Descripciones[$idx]->EVALUACION = 1;
                $Descripciones[$idx]->ORDEN = $idxOrden;
                $Descripciones[$idx]->IDPRE = $value->idPre;
                $Descripciones[$idx]->IDREG = 0;
                $Descripciones[$idx]->RESCAM1 = $value->Resultado;

                $idx++;
                $idxOrden++;
            }*/

            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->IDDPT = 0;
            $Descripciones[$idx]->IDFOR = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->IDFRM = 12;
            $Descripciones[$idx]->RESCAM2 = "";
            $Descripciones[$idx]->RESCAM3 = "";
            $Descripciones[$idx]->FCHCAP = date('Y-m-d', strtotime($row['Fecha']));
            $Descripciones[$idx]->IDPER = 0;
            $Descripciones[$idx]->CONSECUTIVO = 0;
            $Descripciones[$idx]->SERIAL = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->IDOPE = date('Ymd', strtotime($row['Fecha'])) . "0" . $tmpConsecutivo;
            $Descripciones[$idx]->FCHSIS = $row['Fecha'];
            $Descripciones[$idx]->USRSIS = utf8_encode($row['IpalWeb']);
            $Descripciones[$idx]->HOST = "COL\COLEECAS10";
            $Descripciones[$idx]->HLLPRC = 0;
            $Descripciones[$idx]->IDFORTER = $guid;

            $Descripciones[$idx]->EVALUACION = 0;
            $Descripciones[$idx]->ORDEN = $idxOrden;
            $Descripciones[$idx]->IDPRE = 624;
            $Descripciones[$idx]->IDREG = 0;
            $Descripciones[$idx]->RESCAM1 = Observaciones;

            $idx++;

            $tmpFecha = date('Y-m-d', strtotime($row['Fecha']));

         }
         
            mysqli_free_result($result);  
            mysqli_free_result($result2);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
  
  function generar_guid()
  {
    mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
          $charid = strtoupper(md5(uniqid(rand(), true)));
          $hyphen = chr(45);// "-"
          $uuid = substr($charid, 0, 8).$hyphen
                  .substr($charid, 8, 4).$hyphen
                  .substr($charid,12, 4).$hyphen
                  .substr($charid,16, 4).$hyphen
                  .substr($charid,20,12);
          return $uuid;
  }
?>