<?php
    include("../conectar.php"); 
   $link = Conectar();

   date_default_timezone_set("America/Bogota");
   $fecha = date("Y-m-d") . " 00:00:00";

   $sql = "SELECT * FROM Alumbrado_1 INNER JOIN Inspecciones ON Alumbrado_1.idInspeccion = Inspecciones.idInspeccion WHERE Inspecciones.fechaCargue >= '$fecha' AND Inspecciones.Estado = 1";
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Usuario
      {
         public $idAlumbrado_1;
         public $idInspeccion;
         public $Prefijo;
         public $idLogin;
         public $OT;
         public $Tipo;
         public $FechaIngreso;
         public $Ejecutor;
         public $Municipio;
         public $CD;
         public $CoorTransformador;
         public $CapaTrafo;
         public $Fase;
         public $Ubicacion;
         public $Barrio;
         public $Direccion;
         public $TrafoDireccion;
         public $Red;
         public $UsoRed;
         public $CodPoste;
         public $Luminaria;
         public $DireccionLuminaria;
         public $CoordenadasIluminaria;
         public $TipoControl;
         public $CantLuminarias;
         public $TipoIluminarias;
         public $TipoLuminaria;
         public $Propietario;
         public $Via;
         public $TipoControl2;
         public $CoordenadasRele;
         public $TipoRed;
         public $Materiales;
         public $Estado;
         public $TiempoOperacion;
         public $Red2;
         public $Observaciones;
         public $Estado2;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Usuarios[$idx] = new Usuario();
            $Usuarios[$idx]->idAlumbrado_1 = utf8_encode($row['idAlumbrado_1']);
            $Usuarios[$idx]->idInspeccion = utf8_encode($row['idInspeccion']);
            $Usuarios[$idx]->Prefijo = utf8_encode($row['Prefijo']);
            $Usuarios[$idx]->idLogin = utf8_encode($row['idLogin']);
            $Usuarios[$idx]->OT = utf8_encode($row['OT']);
            $Usuarios[$idx]->Tipo = utf8_encode($row['Tipo']);
            $Usuarios[$idx]->FechaIngreso = utf8_encode($row['FechaIngreso']);
            $Usuarios[$idx]->Ejecutor = utf8_encode($row['Ejecutor']);
            $Usuarios[$idx]->Municipio = utf8_encode($row['Municipio']);
            $Usuarios[$idx]->CD = utf8_encode($row['CD']);
            $Usuarios[$idx]->CoorTransformador = utf8_encode($row['CoorTransformador']);
            $Usuarios[$idx]->CapaTrafo = utf8_encode($row['CapaTrafo']);
            $Usuarios[$idx]->Fase = utf8_encode($row['Fase']);
            $Usuarios[$idx]->Ubicacion = utf8_encode($row['Ubicacion']);
            $Usuarios[$idx]->Barrio = utf8_encode($row['Barrio']);
            $Usuarios[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Usuarios[$idx]->TrafoDireccion = utf8_encode($row['TrafoDireccion']);
            $Usuarios[$idx]->Red = utf8_encode($row['Red']);
            $Usuarios[$idx]->UsoRed = utf8_encode($row['UsoRed']);
            $Usuarios[$idx]->CodPoste = utf8_encode($row['CodPoste']);
            $Usuarios[$idx]->Luminaria = utf8_encode($row['Luminaria']);
            $Usuarios[$idx]->DireccionLuminaria = utf8_encode($row['DireccionLuminaria']);
            $Usuarios[$idx]->CoordenadasIluminaria = utf8_encode($row['CoordenadasIluminaria']);
            $Usuarios[$idx]->TipoControl = utf8_encode($row['TipoControl']);
            $Usuarios[$idx]->CantLuminarias = utf8_encode($row['CantLuminarias']);
            $Usuarios[$idx]->TipoIluminarias = utf8_encode($row['TipoIluminarias']);
            $Usuarios[$idx]->TipoLuminaria = utf8_encode($row['TipoLuminaria']);
            $Usuarios[$idx]->Propietario = utf8_encode($row['Propietario']);
            $Usuarios[$idx]->Via = utf8_encode($row['Via']);
            $Usuarios[$idx]->TipoControl2 = utf8_encode($row['TipoControl2']);
            $Usuarios[$idx]->CoordenadasRele = utf8_encode($row['CoordenadasRele']);
            $Usuarios[$idx]->TipoRed = utf8_encode($row['TipoRed']);
            $Usuarios[$idx]->Materiales = utf8_encode($row['Materiales']);
            $Usuarios[$idx]->Estado = utf8_encode($row['Estado']);
            $Usuarios[$idx]->TiempoOperacion = utf8_encode($row['TiempoOperacion']);
            $Usuarios[$idx]->Red2 = utf8_encode($row['Red2']);
            $Usuarios[$idx]->Observaciones = utf8_encode($row['Observaciones']);
            $Usuarios[$idx]->Estado2 = utf8_encode($row['Estado2']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Usuarios);
   } else
   {
      echo 0;
   }
?>