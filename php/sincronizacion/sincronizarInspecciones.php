<?php
    include("../conectar.php"); 
   $link = Conectar();

   date_default_timezone_set("America/Bogota");
   $fecha = date("Y-m-d") . " 00:00:00";
   
   //$nuevafecha = strtotime ( "-3 days" , strtotime ( $fecha ) ) ;
   //$fechaFin = date ( 'Y-m-d' , $nuevafecha );

   $sql = "SELECT * FROM Inspecciones WHERE fechaCargue >= '$fecha' AND Estado = 1";
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Usuario
      {
         public $idInspeccion;
         public $idInspeccionTipo;
         public $fechaIngreso;
         public $idLogin;
         public $Prefijo;
         public $idBaremo;
         public $Desplazamiento;
         public $Tiempo;
         public $Veredal;
         public $Viaticos;
         public $idMunicipio;
         public $Estado;
         public $fechaFin;
         public $fechaCargue;
         public $Coordenadas;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Usuarios[$idx] = new Usuario();
            $Usuarios[$idx]->idInspeccion = utf8_encode($row['idInspeccion']);
            $Usuarios[$idx]->idInspeccionTipo = utf8_encode($row['idInspeccionTipo']);
            $Usuarios[$idx]->fechaIngreso = utf8_encode($row['fechaIngreso']);
            $Usuarios[$idx]->idLogin = utf8_encode($row['idLogin']);
            $Usuarios[$idx]->Prefijo = utf8_encode($row['Prefijo']);
            $Usuarios[$idx]->idBaremo = utf8_encode($row['idBaremo']);
            $Usuarios[$idx]->Desplazamiento = utf8_encode($row['Desplazamiento']);
            $Usuarios[$idx]->Tiempo = utf8_encode($row['Tiempo']);
            $Usuarios[$idx]->Veredal = utf8_encode($row['Veredal']);
            $Usuarios[$idx]->Viaticos = utf8_encode($row['Viaticos']);
            $Usuarios[$idx]->idMunicipio = utf8_encode($row['idMunicipio']);
            $Usuarios[$idx]->Estado = utf8_encode($row['Estado']);
            $Usuarios[$idx]->fechaFin = utf8_encode($row['fechaFin']);
            $Usuarios[$idx]->fechaCargue = utf8_encode($row['fechaCargue']);
            $Usuarios[$idx]->Coordenadas = utf8_encode($row['Coordenadas']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Usuarios);
   } else
   {
      echo 0;
   }
?>