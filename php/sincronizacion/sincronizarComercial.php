<?php
    include("../conectar.php"); 
   $link = Conectar();

   date_default_timezone_set("America/Bogota");
   $fecha = date("Y-m-d") . " 00:00:00";
   
   $sql = "SELECT * FROM Comercial_1 INNER JOIN Inspecciones ON Comercial_1.idInspeccion = Inspecciones.idInspeccion WHERE Inspecciones.fechaCargue >= '$fecha' AND Inspecciones.Estado = 1";

   /*
   $fecha = "2014-10-02 00:00:00";
   $fecha2 = "2014-11-01 23:59:59";

   $sql = "SELECT * FROM Comercial_1 INNER JOIN Inspecciones ON Comercial_1.idInspeccion = Inspecciones.idInspeccion WHERE Inspecciones.fechaCargue >= '$fecha' AND Inspecciones.fechaCargue <= '$fecha2' AND Inspecciones.Estado = 1";
   */
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Usuario
      {
         public $idComercial_1;
         public $Prefijo;
         public $idInspeccion;
         public $OT;
         public $FechaIngreso;
         public $HoraInicio;
         public $Direccion;
         public $Atendio;
         public $Telefono;
         public $CruadrillaTipo;
         public $CuadrillaNum;
         public $CuadrillaTec1;
         public $CuadrillaAux1;
         public $CuadrillaAux2;
         public $CuadrillaSupervisor;
         public $CuadrillaCelulares;
         public $DetalleActividad;
         public $CedulaSupervisor;
         public $CuadrillaCedulas;
         public $SupervisorCelular;
         public $NoOrden;
         public $TipoInterventoria;
         public $SubProceso;
         public $Medida;
         public $SE;
         public $Medida2;
         public $ActividadEconomica;
         public $Factor;
         public $MedActiva;
         public $LecActiva;
         public $TipoFase;
         public $Marca;
         public $Red;
         public $AccesoMedidor;
         public $TipoAcometida;
         public $AcometidaCompartida;
         public $LocalizacionMedidor;
         public $MedidaReferencia;
         public $Sellos;
         public $CapaTrafo;
         public $CD;
         public $PuntoFisico;
         public $Observaciones;
         public $FormatoSupervision;
         public $Cuadrillas;
         public $InspeccionesRealizadas;
         public $PlanSupervision;
         public $ObservacionesCliente;
         public $ObservacionesPredio;
         public $Cumple;
         public $CodIncumplimiento;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Usuarios[$idx] = new Usuario();
            $Usuarios[$idx]->idComercial_1 = utf8_encode($row['idComercial_1']);
            $Usuarios[$idx]->Prefijo = utf8_encode($row['Prefijo']);
            $Usuarios[$idx]->idInspeccion = utf8_encode($row['idInspeccion']);
            $Usuarios[$idx]->OT = utf8_encode($row['OT']);
            $Usuarios[$idx]->FechaIngreso = utf8_encode($row['FechaIngreso']);
            $Usuarios[$idx]->HoraInicio = utf8_encode($row['HoraInicio']);
            $Usuarios[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Usuarios[$idx]->Atendio = utf8_encode($row['Atendio']);
            $Usuarios[$idx]->Telefono = utf8_encode($row['Telefono']);
            $Usuarios[$idx]->CruadrillaTipo = utf8_encode($row['CruadrillaTipo']);
            $Usuarios[$idx]->CuadrillaNum = utf8_encode($row['CuadrillaNum']);
            $Usuarios[$idx]->CuadrillaTec1 = utf8_encode($row['CuadrillaTec1']);
            $Usuarios[$idx]->CuadrillaAux1 = utf8_encode($row['CuadrillaAux1']);
            $Usuarios[$idx]->CuadrillaAux2 = utf8_encode($row['CuadrillaAux2']);
            $Usuarios[$idx]->CuadrillaSupervisor = utf8_encode($row['CuadrillaSupervisor']);
            $Usuarios[$idx]->CuadrillaCelulares = utf8_encode($row['CuadrillaCelulares']);
            $Usuarios[$idx]->DetalleActividad = utf8_encode($row['DetalleActividad']);
            $Usuarios[$idx]->CedulaSupervisor = utf8_encode($row['CedulaSupervisor']);
            $Usuarios[$idx]->CuadrillaCedulas = utf8_encode($row['CuadrillaCedulas']);
            $Usuarios[$idx]->SupervisorCelular = utf8_encode($row['SupervisorCelular']);
            $Usuarios[$idx]->NoOrden = utf8_encode($row['NoOrden']);
            $Usuarios[$idx]->TipoInterventoria = utf8_encode($row['TipoInterventoria']);
            $Usuarios[$idx]->SubProceso = utf8_encode($row['SubProceso']);
            $Usuarios[$idx]->Medida = utf8_encode($row['Medida']);
            $Usuarios[$idx]->SE = utf8_encode($row['SE']);
            $Usuarios[$idx]->Medida2 = utf8_encode($row['Medida2']);
            $Usuarios[$idx]->ActividadEconomica = utf8_encode($row['ActividadEconomica']);
            $Usuarios[$idx]->Factor = utf8_encode($row['Factor']);
            $Usuarios[$idx]->MedActiva = utf8_encode($row['MedActiva']);
            $Usuarios[$idx]->LecActiva = utf8_encode($row['LecActiva']);
            $Usuarios[$idx]->TipoFase = utf8_encode($row['TipoFase']);
            $Usuarios[$idx]->Marca = utf8_encode($row['Marca']);
            $Usuarios[$idx]->Red = utf8_encode($row['Red']);
            $Usuarios[$idx]->AccesoMedidor = utf8_encode($row['AccesoMedidor']);
            $Usuarios[$idx]->TipoAcometida = utf8_encode($row['TipoAcometida']);
            $Usuarios[$idx]->AcometidaCompartida = utf8_encode($row['AcometidaCompartida']);
            $Usuarios[$idx]->LocalizacionMedidor = utf8_encode($row['LocalizacionMedidor']);
            $Usuarios[$idx]->MedidaReferencia = utf8_encode($row['MedidaReferencia']);
            $Usuarios[$idx]->Sellos = utf8_encode($row['Sellos']);
            $Usuarios[$idx]->CapaTrafo = utf8_encode($row['CapaTrafo']);
            $Usuarios[$idx]->CD = utf8_encode($row['CD']);
            $Usuarios[$idx]->PuntoFisico = utf8_encode($row['PuntoFisico']);
            $Usuarios[$idx]->Observaciones = utf8_encode($row['Observaciones']);
            $Usuarios[$idx]->FormatoSupervision = utf8_encode($row['FormatoSupervision']);
            $Usuarios[$idx]->Cuadrillas = utf8_encode($row['Cuadrillas']);
            $Usuarios[$idx]->InspeccionesRealizadas = utf8_encode($row['InspeccionesRealizadas']);
            $Usuarios[$idx]->PlanSupervision = utf8_encode($row['PlanSupervision']);
            $Usuarios[$idx]->ObservacionesCliente = utf8_encode($row['ObservacionesCliente']);
            $Usuarios[$idx]->ObservacionesPredio = utf8_encode($row['ObservacionesPredio']);
            $Usuarios[$idx]->Cumple = utf8_encode($row['Cumple']);
            $Usuarios[$idx]->CodIncumplimiento = utf8_encode($row['CodIncumplimiento']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Usuarios);
   } else
   {
      echo 0;
   }
?>