<?php
    include("../conectar.php"); 
   $link = Conectar();

   date_default_timezone_set("America/Bogota");
   $fecha = date("Y-m-d") . " 00:00:00";
   

   $sql = "SELECT * FROM Ipal INNER JOIN Inspecciones ON Ipal.idInspeccion = Inspecciones.idInspeccion WHERE Inspecciones.fechaCargue >= '$fecha' AND Inspecciones.Estado = 1";
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Usuario
      {
         public $idIpal;
         public $idInspeccion;
         public $idLogin;
         public $resultado;
         public $Empresa;
         public $Direccion;
         public $Celular;
         public $VehiculoTipo;
         public $VehiculoPlaca;
         public $GruaPlaca;
         public $CanastaPlaca;
         public $MotoPlaca;
         public $NoContrato;
         public $Trabajo;
         public $Proceso;
         public $Zona;
         public $Cuadrilla;
         public $Prefijo;
         public $Observaciones;
         public $Cudrillero;
         public $Cuadrillero2;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Usuarios[$idx] = new Usuario();
            $Usuarios[$idx]->idIpal = utf8_encode($row['idIpal']);
            $Usuarios[$idx]->idInspeccion = utf8_encode($row['idInspeccion']);
            $Usuarios[$idx]->idLogin = utf8_encode($row['idLogin']);
            $Usuarios[$idx]->resultado = utf8_encode($row['resultado']);
            $Usuarios[$idx]->Empresa = utf8_encode($row['Empresa']);
            $Usuarios[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Usuarios[$idx]->Celular = utf8_encode($row['Celular']);
            $Usuarios[$idx]->VehiculoTipo = utf8_encode($row['VehiculoTipo']);
            $Usuarios[$idx]->VehiculoPlaca = utf8_encode($row['VehiculoPlaca']);
            $Usuarios[$idx]->GruaPlaca = utf8_encode($row['GruaPlaca']);
            $Usuarios[$idx]->CanastaPlaca = utf8_encode($row['CanastaPlaca']);
            $Usuarios[$idx]->MotoPlaca = utf8_encode($row['MotoPlaca']);
            $Usuarios[$idx]->NoContrato = utf8_encode($row['NoContrato']);
            $Usuarios[$idx]->Trabajo = utf8_encode($row['Trabajo']);
            $Usuarios[$idx]->Proceso = utf8_encode($row['Proceso']);
            $Usuarios[$idx]->Zona = utf8_encode($row['Zona']);
            $Usuarios[$idx]->Cuadrilla = utf8_encode($row['Cuadrilla']);
            $Usuarios[$idx]->Prefijo = utf8_encode($row['Prefijo']);
            $Usuarios[$idx]->Observaciones = utf8_encode($row['Observaciones']);
            $Usuarios[$idx]->Cudrillero = utf8_encode($row['Cudrillero']);
            $Usuarios[$idx]->Cuadrillero2 = utf8_encode($row['Cuadrillero2']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Usuarios);
   } else
   {
      echo 0;
   }
?>