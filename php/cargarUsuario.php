<?php
	include("conectar.php"); 
   $link = Conectar();

   $idLogin = addslashes($_POST['idLogin']);

   $sql = "SELECT * FROM Usuarios WHERE idLogin = '$idLogin'";
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Usuario
      {
         public $idLogin;
         public $Usuario;
         public $Empresa;
         public $Nombre;
         public $Correo;
         public $Cargo;
         public $Perfil;
         public $Estado;
         public $foto;
         public $Novedades;
      }
      class Novedad
      {
         public $idNovedad;
         public $Titulo;
         public $Fecha;
         public $Descripcion;
      }
      
            $row =  $result->fetch_array(MYSQLI_ASSOC);

            $Usuarios = new Usuario();
            $Usuarios->idLogin = utf8_encode($row['idLogin']);
            $Usuarios->Usuario = utf8_encode($row['Usuario']);
            $Usuarios->foto = utf8_encode($row['Foto']);
            $Usuarios->Nombre = utf8_encode($row['Nombre']);
            $Usuarios->Empresa = utf8_encode($row['Empresa']);
            $Usuarios->Correo = utf8_encode($row['Correo']);
            $Usuarios->Cargo = utf8_encode($row['Cargo']);
            $Usuarios->Perfil = utf8_encode($row['Perfil']);
            $Usuarios->Estado = utf8_encode($row['Estado']);

            $sql = "SELECT * FROM Novedades WHERE idLogin = '$idLogin'";
            $result2 = $link->query($sql);

            $idx = 0;
            while ($row = mysqli_fetch_assoc($result2))
            { 
               $Novedades[$idx] = new Novedad();
               $Novedades[$idx]->idNovedad = utf8_encode($row['idNovedad']);
               $Novedades[$idx]->Titulo = utf8_encode($row['Titulo']);
               $Novedades[$idx]->Fecha = utf8_encode($row['Fecha']);
               $Novedades[$idx]->Descripcion = utf8_encode($row['Descripcion']);

               $idx++;
            }
            if ($idx === 0)
            {
               $Novedades[$idx] = new Novedad();
               $Novedades[$idx]->idNovedad = 0;
               $Novedades[$idx]->Titulo = "Sin Novedades";
               $Novedades[$idx]->Fecha = "";
               $Novedades[$idx]->Descripcion = "";             
            }
            
            $Usuarios->Novedades = $Novedades;
         
   } else
   {
      $Usuarios = new Usuario();
      $Usuarios->idLogin = 0;
   }
      mysqli_free_result($result);
      echo json_encode($Usuarios);   
?>