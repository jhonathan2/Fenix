<?php
/*
	header("Content-Type: application/vnd.ms-excel");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("content-disposition: attachment;filename=Reporte.xls");
*/

	include("conectar.php"); 
	$link = Conectar();

	$Desde = $_POST['Desde'] . " 00:00:00";
   	$Hasta = $_POST['Hasta'] . " 23:59:59";

	$sql = "SELECT DISTINCT
				Comercial_1.SubProceso AS 'Proceso',
				'-' AS 'EmpresaColaboradora',
				Comercial_1.OT AS 'NoOrden',
				Comercial_1.FechaIngreso AS 'FechaAnalis',
				EEC_ComConsolidado_In.FECHACUMPLE AS 'FechaEjecucionOt',
				'-' AS 'FechaCargueEpica',
				EEC_ComConsolidado_In.IDCUADRILLA AS 'Cuadrilla',
				SubZonas.Zona AS 'Zona',
				EEC_ComConsolidado_In.ZONA AS 'Subzona',
				EEC_ComConsolidado_In.MUNICIPIO AS 'Municipio',
				Municipios.Nombre AS 'MunicipioInspeccion',
				SubZonasB.idSubZona AS 'idSubZonaInspeecion',
				SubZonasB.Zona AS 'ZonaInspeccion',
				Comercial_1.CodIncumplimiento AS 'MotivoDelRechazo',
				Comercial_1.Observaciones AS 'Observaciones',
				'Interventoría' AS 'AreaDeOrigen'
			FROM
				Comercial_1
				LEFT JOIN EEC_ComConsolidado_In ON Comercial_1.OT = EEC_ComConsolidado_In.IDOT
				LEFT JOIN SubZonas ON SubZonas.idSubZona = EEC_ComConsolidado_In.ZONA
				INNER JOIN Inspecciones ON Inspecciones.idInspeccion = Comercial_1.idInspeccion
				LEFT JOIN Municipios ON Inspecciones.idMunicipio = Municipios.idMunicipio
				LEFT JOIN SubZonas AS SubZonasB ON SubZonasB.idSubZona = Municipios.idSubZona
			WHERE
				Comercial_1.CodIncumplimiento > 0
				AND Comercial_1.FechaIngreso >= '$Desde'
				AND Comercial_1.FechaIngreso <= '$Hasta'
				AND Inspecciones.Estado = 1
				AND Inspecciones.Sucursal = 6000";

	if ($Desde <> "")
	{
		$sql .= " AND Comercial_1.FechaIngreso >= '$Desde'";
	}
	if ($Hasta <> "")
	{
		$sql .= " AND Comercial_1.FechaIngreso <= '$Hasta'";
	}

	$rechazos = array();
	$rechazos[0] = '';
	$rechazos[1] = 'Errores en el procedimiento (en caliente)';
	$rechazos[2] = 'Inconsistencia en resultado o anomalías';
	$rechazos[3] = 'Diferencia entre el material reportado e instalado';
	$rechazos[4] = 'Mala calidad de la obra';
	$rechazos[5] = 'Mala calidad en el diligenciamiento del acta';
	$rechazos[6] = 'Cliente mal vinculado al transformador';
	$rechazos[7] = 'Documentación incompleta';
	$rechazos[8] = 'Error en el factor multiplicador';
	$rechazos[9] = 'Diferencia entre el material reportado y retirado';
	$rechazos[10] = 'No se cumplió con el horario establecido para la maniobra';
	$rechazos[11] = 'No se dio respuesta a la solicitud de la orden';

	class PNC
	{
		public $Proceso;
		public $EmpresaColaboradora;
		public $NoOrden;
		public $FechaAnalis;
		public $FechaEjecucionOt;
		public $FechaCargueEpica;
		public $Cuadrilla;
		public $Zona;
		public $Subzona;
		public $Municipio;
		public $MotivoDelRechazo;
		public $Observaciones;
		public $AreaDeOrigen;
	}

//
	$result = $link->query(utf8_decode($sql));
	$idx = 0;
	while ($row = mysqli_fetch_assoc($result))
	{ 
		$pncs[$idx] = new PNC();
		$pncs[$idx]->Proceso = utf8_encode($row['Proceso']);
		$pncs[$idx]->EmpresaColaboradora = utf8_encode($row['EmpresaColaboradora']);
		$pncs[$idx]->NoOrden = utf8_encode($row['NoOrden']);
		$pncs[$idx]->FechaAnalis = utf8_encode($row['FechaAnalis']);
		

		if (date("N", strtotime($row['FechaEjecucionOt'])) == "1")
		  {
		    $ultimoLunes = date('Y-m-d', strtotime($row['FechaEjecucionOt']));
		  } else
		  {
		    $ultimoLunes = date ( 'Y-m-d' , strtotime("last Monday", strtotime($row['FechaEjecucionOt'])));
		  }

		$pncs[$idx]->FechaEjecucionOt = utf8_encode($ultimoLunes);
		$pncs[$idx]->FechaCargueEpica = utf8_encode($row['FechaCargueEpica']);
		$pncs[$idx]->Cuadrilla = utf8_encode($row['Cuadrilla']);
		if ("" == utf8_encode($row['Zona']))
		{
			$pncs[$idx]->Zona = utf8_encode($row['ZonaInspeccion']);
			$pncs[$idx]->Subzona = utf8_encode($row['idSubZonaInspeecion']);
			$pncs[$idx]->Municipio = utf8_encode($row['MunicipioInspeccion']);
		} else
		{
			$pncs[$idx]->Zona = utf8_encode($row['Zona']);
			$pncs[$idx]->Subzona = utf8_encode($row['Subzona']);
			$pncs[$idx]->Municipio = utf8_encode($row['Municipio']);
		}
		$pncs[$idx]->MotivoDelRechazo = $rechazos[$row['MotivoDelRechazo']];
		$pncs[$idx]->Observaciones = utf8_encode($row['Observaciones']);
		$pncs[$idx]->AreaDeOrigen = utf8_encode($row['AreaDeOrigen']);
		$idx++;
	} 

	if ($idx > 0)
	{
		mysqli_free_result($result);
		echo json_encode($pncs);   
	} else
	{
		echo 0;
	}
?>