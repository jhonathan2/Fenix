<?php
	include("conectar.php"); 
   $link = Conectar();

   $idUsuario = addslashes($_POST['usuario']);
   $idContrato = addslashes($_POST['idContrato']);
   include("datosUsuario.php"); 
   $Usuario = datosUsuario($idUsuario);


   $sql = "SELECT * FROM Empresas";
         $result = $link->query($sql);

         $idx2 = 0;
         while ($row = mysqli_fetch_assoc($result))
         {
            if ($idx2 == 0)
            {
               $pNombres = '["' . utf8_encode($row['Nombre']) . '"';
            } else
            {
               $pNombres .= ',"' . utf8_encode($row['Nombre']) . '"';
            }
            $idx2++;
         }
         $pNombres .= "]";

   $sql = "SELECT * FROM Empresas WHERE idEmpresa IN (SELECT DISTINCT idEmpresa FROM Contratos_has_Empresas WHERE idContrato = '$idContrato')";
   $result = $link->query($sql);

   class resultado
      {
         public $EmpresasAsociadas;
         public $Empresas;
      }

      $Resultado = new resultado();

   if ( $result->num_rows > 0)
   {
      class Empresa
      {
         public $idEmpresa;
         public $Nombre;
         public $Identificacion;
         public $Direccion;
         public $Telefono;
         public $Correo;
         public $Nombres;
      }

      $Nombres = array();
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Empresas[$idx] = new Empresa();
            $Empresas[$idx]->idEmpresa = utf8_encode($row['idEmpresa']);
            $Empresas[$idx]->Nombre = utf8_encode($row['Nombre']);
            $Empresas[$idx]->Identificacion = utf8_encode($row['NID']);
            $Empresas[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Empresas[$idx]->Telefono = utf8_encode($row['Telefono']);
            $Empresas[$idx]->Correo = utf8_encode($row['Correo']);
            
            $idx++;
         }
         
         $Resultado->EmpresasAsociadas = $Empresas;
         
   } else
   {
         $Resultado->EmpresasAsociadas = 0;
   }
      $Resultado->Empresas = $pNombres;
      mysqli_free_result($result);  

      echo json_encode($Resultado); 
?>