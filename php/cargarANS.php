<?php
  include("conectar.php"); 
  include("datosUsuario.php"); 
   
  $idLogin = $_POST['idLogin'];
  $Desde = $_POST['Desde'] . ' 00:00:00';
  $Hasta = $_POST['Hasta'] . ' 23:59:59';
   $Usuario = datosUsuario($idLogin);

   $Condiciones = "";
   if ($Usuario->idPerfil == 7) // Es Interventor
   {
      $Condiciones = " AND idLogin = '$idLogin'";
   }

  date_default_timezone_set('America/Bogota');
  
   $link = Conectar();


     class Datos
      {
         public $total;
         public $alumbrado;
         public $alumbradoRealizadas;
         public $comercial;
         public $comercialRealizadas;
         public $comercialSitioRealizadas;
         public $tecnica;
         public $tecnicaRealizadas;
         public $ipales;
         public $DatosANS;
         public $fecha;
      }
      
         $Data = new Datos();

  if (date("N", strtotime($Desde)) == "1")
  {
    $ultimoLunes = date('Y-m-d', strtotime($Desde));
  } else
  {
    $ultimoLunes = date ( 'Y-m-d' , strtotime("last Monday", strtotime($Desde)));
  }

         
        $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               EEC_ComConsolidado_In WHERE fechaCargue BETWEEN '$ultimoLunes' AND '$Hasta' AND IDSUCURSARL = 6000;";

        $result = $link->query($sql);
        $row = $result->fetch_assoc();
        
         $Data->comercial = $row['Cantidad'];

        $sql = "SELECT 
               COUNT(Inspecciones.idInspeccion) AS 'Cantidad'
            FROM 
               Inspecciones
               INNER JOIN Comercial_1 ON Inspecciones.idInspeccion = Comercial_1.idInspeccion
               INNER JOIN EEC_ComConsolidado_In ON EEC_ComConsolidado_In.IDOT = Comercial_1.OT               
             WHERE 
              EEC_ComConsolidado_In.fechaCargue BETWEEN '$ultimoLunes' AND '$Hasta'
              AND Inspecciones.fechaIngreso >= '$Desde' 
              AND Inspecciones.fechaIngreso <= '$Hasta'
              AND Inspecciones.idInspeccionTipo = 2 
              AND Inspecciones.Estado = 1 
              AND Inspecciones.Sucursal = 6000
              $Condiciones;";

          $result = $link->query($sql);
          $row = $result->fetch_assoc();

          $Data->comercialRealizadas = $row['Cantidad'];


         $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               EEC_ApConsolidado_In;";

        $result = $link->query($sql);
        $row = $result->fetch_assoc();

         $Data->alumbrado = $row['Cantidad'];

                  $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               Inspecciones
               WHERE fechaIngreso >= '$Desde' AND fechaIngreso <= '$Hasta' AND idInspeccionTipo = 3 AND Estado = 1 AND Sucursal = 6000 $Condiciones;";

          $result = $link->query($sql);
          $row = $result->fetch_assoc();

         $Data->alumbradoRealizadas = $row['Cantidad'];

         $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               Inspecciones
               WHERE fechaIngreso >= '$Desde' AND fechaIngreso <= '$Hasta' AND idInspeccionTipo = 1 AND Estado = 1 AND Sucursal = 6000 $Condiciones;";

          $result = $link->query($sql);
          $row = $result->fetch_assoc();

          $Data->ipales = $row['Cantidad'];

          $sql = "SELECT 
               COUNT(Inspecciones.idInspeccion) AS 'Cantidad'
            FROM 
               Inspecciones
               INNER JOIN Comercial_1 ON Inspecciones.idInspeccion = Comercial_1.idInspeccion
               LEFT JOIN EEC_ComConsolidado_In ON EEC_ComConsolidado_In.IDOT = Comercial_1.OT               
             WHERE 
              Inspecciones.fechaIngreso >= '$Desde' AND 
              Inspecciones.fechaIngreso <= '$Hasta'
              AND EEC_ComConsolidado_In.IDOT IS NULL
              AND EEC_ComConsolidado_In.IDSUCURSARL = 6000
              AND Inspecciones.idInspeccionTipo = 2 
              AND Inspecciones.Estado = 1 
              AND Inspecciones.Sucursal = 6000
              $Condiciones;";

          $result = $link->query($sql);
          $row = $result->fetch_assoc();

          $Data->comercialSitioRealizadas = $row['Cantidad'];


         $Data->tecnica = 0;
         $Data->total = $Data->comercial + $Data->alumbrado + $Data->tecnica ;

         $sql = "SELECT
                   DATE_FORMAT(FechaCargue,'%Y-%m-%d') AS 'FechaCargue',
                     SUM(Programadas) AS 'Programadas',
                     SUM(Realizadas) AS 'Realizadas'
                 FROM (
                 SELECT 
                   DATE_FORMAT(EEC_ComConsolidado_In.FechaCargue,'%Y-%m-%d') AS 'FechaCargue',
                     0 AS 'Programadas',
                     COUNT(Comercial_1.idInspeccion) AS 'Realizadas'
                 FROM 
                   EEC_ComConsolidado_In 
                     INNER JOIN Comercial_1 ON EEC_ComConsolidado_In.IDOT = Comercial_1.OT
                     INNER JOIN Inspecciones ON Comercial_1.idInspeccion = Inspecciones.idInspeccion
                 WHERE
                   Inspecciones.Estado = 1
                   AND Inspecciones.Sucursal = 6000
                   AND EEC_ComConsolidado_In.IDSUCURSARL = 6000
                   AND EEC_ComConsolidado_In.fechaCargue BETWEEN '$ultimoLunes' AND '$Hasta'
                   AND Inspecciones.fechaIngreso >= '$Desde' 
                   AND Inspecciones.fechaIngreso <= '$Hasta'
                 GROUP BY DATE_FORMAT(EEC_ComConsolidado_In.FechaCargue,'%Y-%m-%d')
                 UNION ALL
                 SELECT 
                   DATE_FORMAT(EEC_ComConsolidado_In.FechaCargue,'%Y-%m-%d') AS 'FechaCargue',
                   COUNT(EEC_ComConsolidado_In.IDOT) AS 'Programadas',
                     0 AS 'Realizadas'
                 FROM 
                   EEC_ComConsolidado_In 
                 WHERE
                   EEC_ComConsolidado_In.fechaCargue BETWEEN '$ultimoLunes' AND '$Hasta'
                   AND EEC_ComConsolidado_In.IDSUCURSARL = 6000
                 GROUP BY DATE_FORMAT(FechaCargue,'%Y-%m-%d')) AS Obj
                 GROUP BY FechaCargue";

        class ANSDato
        {
          public $fechaCargue;
          public $Programadas;
          public $Realizadas;
        }

        $result = $link->query(utf8_decode($sql));
        $idx = 0;
        while ($row = mysqli_fetch_assoc($result))
         { 
            $ANSDatos[$idx] = new ANSDato();
            $ANSDatos[$idx]->fechaCargue = utf8_encode($row['FechaCargue']);
            $ANSDatos[$idx]->Programadas = utf8_encode($row['Programadas']);
            $ANSDatos[$idx]->Realizadas = utf8_encode($row['Realizadas']);
            $idx++;
         }

         $Data->DatosANS = $ANSDatos;

         mysqli_free_result($result);  
         echo json_encode($Data);
?>