<?php
	include("conectar.php"); 
  include("datosUsuario.php"); 
   
  $idLogin = $_POST['idLogin'];
   $Usuario = datosUsuario($idLogin);

   $Condiciones = "";
   if ($Usuario->idPerfil == 7) // Es Interventor
   {
      $Condiciones = " AND idLogin = '$idLogin'";
   }

  date_default_timezone_set('America/Bogota');
  
  if (date("N") == "1")
  {
    $ultimoLunes = date('Y-m-d');
  } else
  {
    $ultimoLunes = date ( 'Y-m-d' , strtotime("last Monday"));
  }
  

   $link = Conectar();


     class Datos
      {
         public $total;
         public $alumbrado;
         public $alumbradoRealizadas;
         public $comercial;
         public $comercialRealizadas;
         public $tecnica;
         public $tecnicaRealizadas;
         public $ipales;
         public $fecha;
      }
      
         $Data = new Datos();

        $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               EEC_ComConsolidado_In WHERE fechaCargue >= '$ultimoLunes' AND IDSUCURSARL = 6000;";

        $result = $link->query($sql);
        $row = $result->fetch_assoc();
        
         $Data->comercial = $row['Cantidad'];

        $sql = "SELECT 
               COUNT(Inspecciones.idInspeccion) AS 'Cantidad'
            FROM 
               Inspecciones
               INNER JOIN Comercial_1 ON Inspecciones.idInspeccion = Comercial_1.idInspeccion
               INNER JOIN EEC_ComConsolidado_In ON EEC_ComConsolidado_In.IDOT = Comercial_1.OT               
             WHERE 
              Inspecciones.fechaIngreso >= '$ultimoLunes' 
              AND Inspecciones.Sucursal = 6000
              AND EEC_ComConsolidado_In.IDSUCURSARL = 6000
              AND Inspecciones.idInspeccionTipo = 2 
              AND Inspecciones.Estado = 1 $Condiciones;";

          $result = $link->query($sql);
          $row = $result->fetch_assoc();

          $Data->comercialRealizadas = $row['Cantidad'];


         $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               Inspecciones WHERE fechaIngreso >= '" . $ultimoLunes . "' AND Estado = 1 AND Sucursal = 6000 AND idInspeccionTipo = 3;";

        $result = $link->query($sql);
        $row = $result->fetch_assoc();

         $Data->alumbrado = 2442;//$row['Cantidad'];
         $Data->alumbradoRealizadas = 0;

         $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               Inspecciones
               WHERE fechaIngreso >= '$ultimoLunes' AND idInspeccionTipo = 1 AND Estado = 1 AND Sucursal = 6000 $Condiciones;";

          $result = $link->query($sql);
          $row = $result->fetch_assoc();

          $Data->ipales = $row['Cantidad'];


         $Data->tecnica = 0;
         $Data->total = $Data->comercial + $Data->alumbrado + $Data->tecnica ;//+ $Data->ipales;
         $Data->fecha = $ultimoLunes ;


         /*
         $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               Actividades;";

        $result = $link->query($sql);
        $row = $result->fetch_assoc();

         $Data->pruebas = $row['Cantidad'];

         $sql = "SELECT 
               COUNT(*) AS 'Cantidad'
            FROM 
               Contratos;";

        $result = $link->query($sql);
        $row = $result->fetch_assoc();

         $Data->contratos = $row['Cantidad'];
         */

         mysqli_free_result($result);  
         echo json_encode($Data);
?>