<?php
	include("../conectar.php"); 
    include("../Inspecciones.php"); 

   $link = Conectar();

   $idComercial = $_POST['idComercial'];

   $condicion = "Comercial_1.idComercial_1 = '$idComercial';";

   if (substr($idComercial, 0, 1) == "0")
   {
        $idComercial = devolverId($idComercial);
        $condicion = "Comercial_1.idInspeccion = '$idComercial';";

   }

   $sql = "SELECT 
                Inspecciones.idInspeccion AS 'Consecutivo', 
                Inspecciones.fechaIngreso AS 'Fecha',
                DatosUsuarios.idLogin AS 'idLogin', 
                DatosUsuarios.Nombre AS 'Usuario', 
                Comercial_1.Prefijo AS 'Prefijo', 
                Comercial_1.idComercial_1 AS 'idComercial', 
                Comercial_1.OT AS 'OT', 
                Comercial_1.HoraInicio AS 'HoraInicio', 
                Comercial_1.Direccion AS 'Direccion', 
                Comercial_1.Atendio AS 'Atendio', 
                Comercial_1.Telefono AS 'Telefono',
                Comercial_1.TipoInterventoria AS 'TipoInterventoria',
                Comercial_1.SubProceso AS 'SubProceso',
                Comercial_1.Medida AS 'Medida',
                Comercial_1.SE AS 'SE',
                Comercial_1.Medida2 AS 'Medida2',
                Comercial_1.ActividadEconomica AS 'ActividadEconomica',
                Comercial_1.Factor AS 'Factor',
                Comercial_1.MedActiva AS 'MedActiva',
                Comercial_1.LecActiva AS 'LecActiva',
                Comercial_1.TipoFase AS 'TipoFase',
                Comercial_1.Marca AS 'Marca',
                Comercial_1.Red AS 'Red',
                Comercial_1.AccesoMedidor AS 'AccesoMedidor',
                Comercial_1.TipoAcometida AS 'TipoAcometida',
                Comercial_1.AcometidaCompartida AS 'AcometidaCompartida',
                Comercial_1.LocalizacionMedidor AS 'LocalizacionMedidor',
                Comercial_1.MedidaReferencia AS 'MedidaReferencia',
                Comercial_1.Sellos AS 'Sellos',
                Comercial_1.CapaTrafo AS 'CapaTrafo',
                Comercial_1.CD AS 'CD',
                Comercial_1.PuntoFisico AS 'PuntoFisico',
                Comercial_1.CruadrillaTipo AS 'CruadrillaTipo',
                Comercial_1.CuadrillaNum AS 'CuadrillaNum',
                Comercial_1.CuadrillaTec1 AS 'CuadrillaTec1',
                Comercial_1.CuadrillaAux1 AS 'CuadrillaAux1',
                Comercial_1.CuadrillaAux2 AS 'CuadrillaAux2',
                Comercial_1.CuadrillaSupervisor AS 'CuadrillaSupervisor',
                Comercial_1.CuadrillaCelulares AS 'CuadrillaCelulares',
                Comercial_1.DetalleActividad AS 'DetalleActividad',
                Comercial_1.CedulaSupervisor AS 'CedulaSupervisor',
                Comercial_1.CuadrillaCedulas AS 'CuadrillaCedulas',
                Comercial_1.SupervisorCelular AS 'SupervisorCelular',
                Comercial_1.NoOrden AS 'NoOrden',
                Comercial_1.FormatoSupervision AS 'FormatoSupervision',
                Comercial_1.Cuadrillas AS 'Cuadrillas',
                Comercial_1.InspeccionesRealizadas AS 'InspeccionesRealizadas',
                Comercial_1.PlanSupervision AS 'PlanSupervision',
                Comercial_1.Observaciones AS 'Observaciones',
                Comercial_1.ObservacionesCliente AS 'ObservacionesCliente',
                Comercial_1.ObservacionesPredio AS 'ObservacionesPredio',
                Comercial_1.Cumple AS 'Cumple',
                Comercial_1.CodIncumplimiento AS 'CodIncumplimiento'
            FROM 
               Comercial_1,
                DatosUsuarios,
                Inspecciones
            WHERE
               Inspecciones.idLogin = DatosUsuarios.idLogin
                AND Comercial_1.idInspeccion = Inspecciones.idInspeccion
                AND $condicion";


   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
         public $Consecutivo;
         public $Fecha;
         public $idLogin;
         public $Usuario;
         public $Prefijo;
         public $idComercial;
         public $OT;
         public $HoraInicio;
         public $Direccion;
         public $Atendio;
         public $Telefono;
         public $TipoInterventoria;
         public $SubProceso;
         public $Medida;
         public $SE;
         public $Medida2;
         public $ActividadEconomica;
         public $Factor;
         public $MedActiva;
         public $LecActiva;
         public $TipoFase;
         public $Marca;
         public $Red;
         public $AccesoMedidor;
         public $TipoAcometida;
         public $AcometidaCompartida;
         public $LocalizacionMedidor;
         public $MedidaReferencia;
         public $Sellos;
         public $CapaTrafo;
         public $CD;
         public $PuntoFisico;

        public $CruadrillaTipo;
        public $CuadrillaNum;
        public $CuadrillaTec1;
        public $CuadrillaAux1;
        public $CuadrillaAux2;
        public $CuadrillaSupervisor;
        public $CuadrillaCelulares;
        public $DetalleActividad;
        public $CedulaSupervisor;
        public $CuadrillaCedulas;
        public $SupervisorCelular;
        public $NoOrden;

        public $Observaciones;
        public $FormatoSupervision;
        public $Cuadrillas;
        public $InspeccionesRealizadas;
        public $PlanSupervision;
        public $ObservacionesCliente;
        public $ObservacionesPredio;
        public $Cumple;
        public $CodIncumplimiento;

      }

        $row =  $result->fetch_array(MYSQLI_ASSOC);
      
            $Descripciones = new Descripcion();
            $Descripciones->Consecutivo = utf8_encode($row['Consecutivo']);
            $Descripciones->Fecha = utf8_encode($row['Fecha']);
            $Descripciones->idLogin = utf8_encode($row['idLogin']);
            $Descripciones->Usuario = utf8_encode($row['Usuario']);
            $Descripciones->Prefijo = utf8_encode($row['Prefijo']);
            $Descripciones->idComercial = utf8_encode($row['idComercial']);
            $Descripciones->OT = utf8_encode($row['OT']);
            $Descripciones->HoraInicio = utf8_encode($row['HoraInicio']);
            $Descripciones->Direccion = utf8_encode($row['Direccion']);
            $Descripciones->Atendio = utf8_encode($row['Atendio']);
            $Descripciones->Telefono = utf8_encode($row['Telefono']);
            $Descripciones->TipoInterventoria = utf8_encode($row['TipoInterventoria']);
            $Descripciones->SubProceso = utf8_encode($row['SubProceso']);
            $Descripciones->Medida = utf8_encode($row['Medida']);
            $Descripciones->SE = utf8_encode($row['SE']);
            $Descripciones->Medida2 = utf8_encode($row['Medida2']);
            $Descripciones->ActividadEconomica = utf8_encode($row['ActividadEconomica']);
            $Descripciones->Factor = utf8_encode($row['Factor']);
            $Descripciones->MedActiva = utf8_encode($row['MedActiva']);
            $Descripciones->LecActiva = utf8_encode($row['LecActiva']);
            $Descripciones->TipoFase = utf8_encode($row['TipoFase']);
            $Descripciones->Marca = utf8_encode($row['Marca']);
            $Descripciones->Red = utf8_encode($row['Red']);
            $Descripciones->AccesoMedidor = utf8_encode($row['AccesoMedidor']);
            $Descripciones->TipoAcometida = utf8_encode($row['TipoAcometida']);
            $Descripciones->AcometidaCompartida = utf8_encode($row['AcometidaCompartida']);
            $Descripciones->LocalizacionMedidor = utf8_encode($row['LocalizacionMedidor']);
            $Descripciones->MedidaReferencia = utf8_encode($row['MedidaReferencia']);
            $Descripciones->Sellos = utf8_encode($row['Sellos']);
            $Descripciones->CapaTrafo = utf8_encode($row['CapaTrafo']);
            $Descripciones->CD = utf8_encode($row['CD']);
            $Descripciones->PuntoFisico = utf8_encode($row['PuntoFisico']);

            $Descripciones->CruadrillaTipo = utf8_encode($row['CruadrillaTipo']);
            $Descripciones->CuadrillaNum = utf8_encode($row['CuadrillaNum']);
            $Descripciones->CuadrillaTec1 = utf8_encode($row['CuadrillaTec1']);
            $Descripciones->CuadrillaAux1 = utf8_encode($row['CuadrillaAux1']);
            $Descripciones->CuadrillaAux2 = utf8_encode($row['CuadrillaAux2']);
            $Descripciones->CuadrillaSupervisor = utf8_encode($row['CuadrillaSupervisor']);
            $Descripciones->CuadrillaCelulares = utf8_encode($row['CuadrillaCelulares']);
            $Descripciones->DetalleActividad = utf8_encode($row['DetalleActividad']);
            $Descripciones->CedulaSupervisor = utf8_encode($row['CedulaSupervisor']);
            $Descripciones->CuadrillaCedulas = utf8_encode($row['CuadrillaCedulas']);
            $Descripciones->SupervisorCelular = utf8_encode($row['SupervisorCelular']);
            $Descripciones->NoOrden = utf8_encode($row['NoOrden']);

            $Descripciones->Observaciones = utf8_encode($row['Observaciones']);
            $Descripciones->FormatoSupervision = utf8_encode($row['FormatoSupervision']);
            $Descripciones->Cuadrillas = utf8_encode($row['Cuadrillas']);
            $Descripciones->InspeccionesRealizadas = utf8_encode($row['InspeccionesRealizadas']);
            $Descripciones->PlanSupervision = utf8_encode($row['PlanSupervision']);
            $Descripciones->ObservacionesCliente = utf8_encode($row['ObservacionesCliente']);
            $Descripciones->ObservacionesPredio = utf8_encode($row['ObservacionesPredio']);
            $Descripciones->Cumple = utf8_encode($row['Cumple']);
            $Descripciones->CodIncumplimiento = utf8_encode($row['CodIncumplimiento']);

            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
    }
    else
    {
      echo 0;
    }
?>
