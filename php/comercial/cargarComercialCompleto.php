<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";

   $sql = "SELECT 
    InsComercial.*,
    IF(EEC_ComConsolidado_In.IDOT IS NULL, 'SITIO', 'FRIO')  AS 'FRIO'
    FROM 
       InsComercial 
       INNER JOIN Inspecciones ON InsComercial.idInspeccion = Inspecciones.idInspeccion
       LEFT JOIN EEC_ComConsolidado_In ON InsComercial.OT = EEC_ComConsolidado_In.IDOT
   WHERE InsComercial.FechaIngreso BETWEEN '$Desde' AND '$Hasta' AND Inspecciones.Estado = 1 AND Inspecciones.Sucursal = 6000;";


   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
        public $idComercial;
          public $Prefijo;
          public $idInspeccion;
          public $OT;
          public $FechaIngreso;
          public $HoraInicio;
          public $Direccion;
          public $Atendio;
          public $Telefono;
          public $TipoInterventoria;
          public $SubProceso;
          public $Medida;
          public $SE;
          public $Medida2;
          public $ActividadEconomica;
          public $Factor;
          public $MedActiva;
          public $LecActiva;
          public $TipoFase;
          public $Marca;
          public $Red;
          public $AccesoMedidor;
          public $TipoAcometida;
          public $AcometidaCompartida;
          public $LocalizacionMedidor;
          public $MedidaReferencia;
          public $Sellos;
          public $CapaTrafo;
          public $CD;
          public $PuntoFisico;
          public $Observaciones;
          public $FormatoSupervision;
          public $Cuadrillas;
          public $InspeccionesRealizadas;
          public $PlanSupervision;
          public $ObservacionesCliente;
          public $ObservacionesPredio;
          public $Cumple;
          public $CodIncumplimiento;
          public $Nombre;
          public $FRIO;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->idComercial =  utf8_encode($row['idComercial_1']);
            $Descripciones[$idx]->Prefijo =  utf8_encode($row['Prefijo']);
            $Descripciones[$idx]->idInspeccion =  utf8_encode($row['idInspeccion']);
            $Descripciones[$idx]->OT =  utf8_encode($row['OT']);
            $Descripciones[$idx]->FechaIngreso =  utf8_encode($row['FechaIngreso']);
            $Descripciones[$idx]->HoraInicio =  utf8_encode($row['HoraInicio']);
            $Descripciones[$idx]->Direccion =  utf8_encode($row['Direccion']);
            $Descripciones[$idx]->Atendio =  utf8_encode($row['Atendio']);
            $Descripciones[$idx]->Telefono =  utf8_encode($row['Telefono']);
            $Descripciones[$idx]->TipoInterventoria =  utf8_encode($row['TipoInterventoria']);
            $Descripciones[$idx]->SubProceso =  utf8_encode($row['SubProceso']);
            $Descripciones[$idx]->Medida =  utf8_encode($row['Medida']);
            $Descripciones[$idx]->SE =  utf8_encode($row['SE']);
            $Descripciones[$idx]->Medida2 =  utf8_encode($row['Medida2']);
            $Descripciones[$idx]->ActividadEconomica =  utf8_encode($row['ActividadEconomica']);
            $Descripciones[$idx]->Factor =  utf8_encode($row['Factor']);
            $Descripciones[$idx]->MedActiva =  utf8_encode($row['MedActiva']);
            $Descripciones[$idx]->LecActiva =  utf8_encode($row['LecActiva']);
            $Descripciones[$idx]->TipoFase =  utf8_encode($row['TipoFase']);
            $Descripciones[$idx]->Marca =  utf8_encode($row['Marca']);
            $Descripciones[$idx]->Red =  utf8_encode($row['Red']);
            $Descripciones[$idx]->AccesoMedidor =  utf8_encode($row['AccesoMedidor']);
            $Descripciones[$idx]->TipoAcometida =  utf8_encode($row['TipoAcometida']);
            $Descripciones[$idx]->AcometidaCompartida =  utf8_encode($row['AcometidaCompartida']);
            $Descripciones[$idx]->LocalizacionMedidor =  utf8_encode($row['LocalizacionMedidor']);
            $Descripciones[$idx]->MedidaReferencia =  utf8_encode($row['MedidaReferencia']);
            $Descripciones[$idx]->Sellos =  utf8_encode($row['Sellos']);
            $Descripciones[$idx]->CapaTrafo =  utf8_encode($row['CapaTrafo']);
            $Descripciones[$idx]->CD =  utf8_encode($row['CD']);
            $Descripciones[$idx]->PuntoFisico =  utf8_encode($row['PuntoFisico']);
            $Descripciones[$idx]->Observaciones =  utf8_encode($row['Observaciones']);
            $Descripciones[$idx]->FormatoSupervision =  utf8_encode($row['FormatoSupervision']);
            $Descripciones[$idx]->Cuadrillas =  utf8_encode($row['Cuadrillas']);
            $Descripciones[$idx]->InspeccionesRealizadas =  utf8_encode($row['InspeccionesRealizadas']);
            $Descripciones[$idx]->PlanSupervision =  utf8_encode($row['PlanSupervision']);
            $Descripciones[$idx]->ObservacionesCliente =  utf8_encode($row['ObservacionesCliente']);
            $Descripciones[$idx]->ObservacionesPredio =  utf8_encode($row['ObservacionesPredio']);
            $Descripciones[$idx]->Cumple =  utf8_encode($row['Cumple']);
            $Descripciones[$idx]->CodIncumplimiento =  utf8_encode($row['CodIncumplimiento']);
            $Descripciones[$idx]->Nombre =  utf8_encode($row['Nombre']);
            $Descripciones[$idx]->FRIO =  utf8_encode($row['FRIO']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>