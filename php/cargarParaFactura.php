<?php
	include("conectar.php"); 
   $link = Conectar();

   $Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";
   $Anio = substr($Desde, 0, 4);

   $sql = "SELECT * FROM (SELECT 
            SubZonas.Sede AS 'SubZona',
            Municipios.Nombre AS 'Municipio',
            (COUNT(*)*Baremo.Adjudicado$Anio) AS 'Total'
          FROM 
            Inspecciones 
            INNER JOIN Baremo ON Inspecciones.idBaremo = Baremo.idBaremo 
            LEFT JOIN Municipios ON Inspecciones.idMunicipio = Municipios.idMunicipio 
            LEFT JOIN SubZonas ON SubZonas.idSubZona = Municipios.idSubZona
          WHERE
            Inspecciones.Estado = 1
            AND Inspecciones.Sucursal = 6000
            AND Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
          GROUP BY 
              SubZonas.Sede,
              Municipios.Nombre
        UNION ALL
          SELECT 
            SubZonas.Sede AS 'SubZona',
            Municipios.Nombre AS 'Municipio',
            (COUNT(Inspecciones.idInspeccion) * Baremo.Adjudicado$Anio) AS 'Total'
          FROM 
            Inspecciones 
            LEFT JOIN Municipios ON Inspecciones.idMunicipio = Municipios.idMunicipio 
            LEFT JOIN SubZonas ON SubZonas.idSubZona = Municipios.idSubZona,
            Baremo 
          WHERE
            Inspecciones.Estado = 1
            AND Inspecciones.Sucursal = 6000
            AND Inspecciones.Veredal = 1
            AND Baremo.idBaremo = 15
            AND Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
          GROUP BY 
              SubZonas.Zona,
              SubZonas.Sede,
              Municipios.Nombre) AS Datos ORDER BY 1;";



            
//echo $sql;
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
        
         public $SubZona;
         public $Municipio;
         public $Total;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
          $strAdjudicado = "Adjudicado$Anio";
            $Descripciones[$idx] = new Descripcion();
            
            $Descripciones[$idx]->SubZona = utf8_encode($row['SubZona']);
            $Descripciones[$idx]->Municipio = utf8_encode($row['Municipio']);
            $Descripciones[$idx]->Total = utf8_encode($row['Total']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>