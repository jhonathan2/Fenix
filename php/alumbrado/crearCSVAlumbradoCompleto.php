<?php
	include("../conectar.php"); 
    include("../phpExcel/PHPExcel.php"); 
   $link = Conectar();

   $Desde = $_GET['Desde'] . " 00:00:00";
   $Hasta = $_GET['Hasta'] . " 23:59:59";

   $sql = "SELECT
                Alumbrado_1.idAlumbrado_1 AS 'ID',
                Alumbrado_1.OT AS 'OT',
                Alumbrado_1.Tipo AS 'TIPO_OT',
                Alumbrado_1.FechaIngreso AS 'FECHA_EJEC',
                Login.Usuario AS 'EJECUTOR',
                Alumbrado_1.Municipio AS 'MUNICIPIO',
                Alumbrado_1.CD AS 'CD',
                SPLIT_STR(Alumbrado_1.CoorTransformador, ',', 1) AS 'Trafo_nox',
                SPLIT_STR(Alumbrado_1.CoorTransformador, ',', 2) AS 'Trafo_noy',
                Alumbrado_1.CapaTrafo AS 'trafo_capa',
                Alumbrado_1.Fase AS 'trafo_fase',
                Alumbrado_1.Municipio AS 'trafo_mun',
                Alumbrado_1.Ubicacion AS 'trafo_ubic',
                Alumbrado_1.Direccion AS 'trafo_barr',
                Alumbrado_1.TrafoDireccion AS 'trafo_dir',
                Alumbrado_1.Red AS 'clase_red',
                Alumbrado_1.UsoRed AS 'uso_red',
                Alumbrado_1.CodPoste AS 'punto_sig',
                Alumbrado_1.Luminaria AS 'LUMINARIA', 
                Alumbrado_1.DireccionLuminaria AS 'ILU_DIR', 
                SPLIT_STR(Alumbrado_1.CoordenadasIluminaria, ',', 1) AS 'ilu_nox',
                SPLIT_STR(Alumbrado_1.CoordenadasIluminaria, ',', 2) AS 'ilu_noy',
                Alumbrado_1.TipoControl AS 'ilu_tipoco', 
                Alumbrado_1.CantLuminarias AS 'ilu_cant', 
                Alumbrado_1.TipoIluminarias AS 'ilu_tiplam', 
                Alumbrado_1.TipoLuminaria AS 'ilu_ilumin', 
                Alumbrado_1.Propietario AS 'ilu_propie', 
                Alumbrado_1.Via AS 'ilu_tipvia', 
                Alumbrado_1.TipoControl2 AS 'rele_tipo', 
                SPLIT_STR(Alumbrado_1.CoordenadasRele, ',', 1) AS 'Rele_nox',
                SPLIT_STR(Alumbrado_1.CoordenadasRele, ',', 2) AS 'Rele_noy',
                Alumbrado_1.TipoRed AS 'red_tipseg', 
                Alumbrado_1.Materiales AS 'red_mat', 
                Alumbrado_1.Estado AS 'red_estado', 
                Alumbrado_1.TiempoOperacion AS 'tiempo_op', 
                Alumbrado_1.Red2 AS 'tipo_red', 
                Alumbrado_1.Observaciones AS 'observacio', 
                Alumbrado_1.Estado2 AS 'estado'
            FROM
                Alumbrado_1
                INNER JOIN Login ON Alumbrado_1.idLogin = Login.idLogin
                INNER JOIN Inspecciones ON Alumbrado_1.idInspeccion = Inspecciones.idInspeccion
            WHERE Alumbrado_1.FechaIngreso BETWEEN '$Desde' AND '$Hasta' AND  Inspecciones.Estado = 1 AND Inspecciones.Sucursal = 6000;";

//echo $sql;
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'OT')
                ->setCellValue('C1', 'TIPO_OT')
                ->setCellValue('D1', 'FECHA_EJEC')
                ->setCellValue('E1', 'EJECUTOR')
                ->setCellValue('F1', 'MUNICIPIO')
                ->setCellValue('G1', 'CD')
                ->setCellValue('H1', 'Trafo_nox')
                ->setCellValue('I1', 'Trafo_noy')
                ->setCellValue('J1', 'trafo_capa')
                ->setCellValue('K1', 'trafo_fase')
                ->setCellValue('L1', 'trafo_ubic')
                ->setCellValue('M1', 'trafo_mun')
                ->setCellValue('N1', 'trafo_barr')
                ->setCellValue('O1', 'trafo_dir')
                ->setCellValue('P1', 'clase_red')
                ->setCellValue('Q1', 'uso_red')
                ->setCellValue('R1', 'punto_sig')
                ->setCellValue('S1', 'LUMINARIA')
                ->setCellValue('T1', 'ILU_DIR')
                ->setCellValue('U1', 'ilu_nox')
                ->setCellValue('V1', 'ilu_noy')
                ->setCellValue('W1', 'ilu_tipoco')
                ->setCellValue('X1', 'ilu_cant')
                ->setCellValue('Y1', 'ilu_tiplam')
                ->setCellValue('Z1', 'ilu_ilumin')
                ->setCellValue('AA1', 'ilu_propie')
                ->setCellValue('AB1', 'ilu_tipvia')
                ->setCellValue('AC1', 'rele_tipo')
                ->setCellValue('AD1', 'Rele_nox')
                ->setCellValue('AE1', 'Rele_noy')
                ->setCellValue('AF1', 'red_tipseg')
                ->setCellValue('AG1', 'red_mat')
                ->setCellValue('AH1', 'red_estado')
                ->setCellValue('AI1', 'tiempo_op')
                ->setCellValue('AJ1', 'tipo_red')
                ->setCellValue('AK1', 'observacio')
                ->setCellValue('AL1', 'estado');
      
            $idx = 2;
            $datos = array();
         while ($row = mysqli_fetch_assoc($result))
         { 
            $iluCant = $row['ilu_cant'];
            if ($iluCant == "")
            {
                if ($row['estado'] < 7)
                {
                    $iluCant = '1/1';
                } else
                {
                    $iluCant = 0;
                }
            }
            if ($iluCant == "0")
            {
                if ($row['estado'] < 7)
                {
                    $iluCant = '1/1';
                }
            }
            if ($iluCant == "1")
            {
                $iluCant = '1/1';
            }

            $objPHPExcel->getActiveSheet()
                ->getCell('A' . $idx)->setValue( strtoupper($row['ID']));
            $objPHPExcel->getActiveSheet()
                ->getCell('B' . $idx)->setValue( strtoupper($row['OT']));
            $objPHPExcel->getActiveSheet()
                ->getCell('C' . $idx)->setValue( strtoupper($row['TIPO_OT']));
            $objPHPExcel->getActiveSheet()
                ->getCell('D' . $idx)->setValue( strtoupper($row['FECHA_EJEC']));
            $objPHPExcel->getActiveSheet()
                ->getCell('E' . $idx)->setValue( strtoupper($row['EJECUTOR']));
            $objPHPExcel->getActiveSheet()
                ->getCell('F' . $idx)->setValue( strtoupper($row['MUNICIPIO']));
            $objPHPExcel->getActiveSheet()
                ->getCell('G' . $idx)->setValue( strtoupper($row['CD']));
            $objPHPExcel->getActiveSheet()
                ->getCell('H' . $idx)->setValue( strtoupper($row['Trafo_nox']));
            $objPHPExcel->getActiveSheet()
                ->getCell('I' . $idx)->setValue( strtoupper($row['Trafo_noy']));
            $objPHPExcel->getActiveSheet()
                ->getCell('J' . $idx)->setValue( strtoupper($row['trafo_capa']));
            $objPHPExcel->getActiveSheet()
                ->getCell('K' . $idx)->setValue( strtoupper($row['trafo_fase']));
            $objPHPExcel->getActiveSheet()
                ->getCell('L' . $idx)->setValue( strtoupper($row['trafo_ubic']));
            $objPHPExcel->getActiveSheet()
                ->getCell('M' . $idx)->setValue( strtoupper($row['trafo_mun']));
            $objPHPExcel->getActiveSheet()
                ->getCell('N' . $idx)->setValue( strtoupper($row['trafo_barr']));
            $objPHPExcel->getActiveSheet()
                ->getCell('O' . $idx)->setValue( strtoupper($row['trafo_dir']));
            $objPHPExcel->getActiveSheet()
                ->getCell('P' . $idx)->setValue( strtoupper($row['clase_red']));
            $objPHPExcel->getActiveSheet()
                ->getCell('Q' . $idx)->setValue( strtoupper($row['uso_red']));
            $objPHPExcel->getActiveSheet()
                ->getCell('R' . $idx)->setValue( str_replace(" ", "", strtoupper($row['punto_sig'])));
            $objPHPExcel->getActiveSheet()
                ->getCell('S' . $idx)->setValue( str_replace(" ", "", strtoupper($row['LUMINARIA'])));
            $objPHPExcel->getActiveSheet()
                ->getCell('T' . $idx)->setValue( strtoupper($row['ILU_DIR']));
            $objPHPExcel->getActiveSheet()
                ->getCell('U' . $idx)->setValue( strtoupper($row['ilu_nox']));
            $objPHPExcel->getActiveSheet()
                ->getCell('V' . $idx)->setValue( strtoupper($row['ilu_noy']));
            $objPHPExcel->getActiveSheet()
                ->getCell('W' . $idx)->setValue( strtoupper($row['ilu_tipoco']));
            $objPHPExcel->getActiveSheet()
                ->getCell('X' . $idx)->setValue( $iluCant);
            $objPHPExcel->getActiveSheet()
                ->getCell('Y' . $idx)->setValue( strtoupper($row['ilu_tiplam']));
            $objPHPExcel->getActiveSheet()
                ->getCell('Z' . $idx)->setValue( strtoupper($row['ilu_ilumin']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AA' . $idx)->setValue( strtoupper($row['ilu_propie']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AB' . $idx)->setValue( strtoupper($row['ilu_tipvia']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AC' . $idx)->setValue( strtoupper($row['rele_tipo']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AD' . $idx)->setValue( strtoupper($row['Rele_nox']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AE' . $idx)->setValue( strtoupper($row['Rele_noy']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AF' . $idx)->setValue( strtoupper($row['red_tipseg']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AG' . $idx)->setValue( strtoupper($row['red_mat']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AH' . $idx)->setValue( strtoupper($row['red_estado']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AI' . $idx)->setValue( strtoupper($row['tiempo_op']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AJ' . $idx)->setValue( strtoupper($row['tipo_red']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AK' . $idx)->setValue( strtoupper($row['observacio']));
            $objPHPExcel->getActiveSheet()
                ->getCell('AL' . $idx)->setValue( strtoupper($row['estado']));

                //$objPHPExcel->garbageCollect();
                $idx++;
         }
         
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="Alumbrado'.$Desde. '_' . $Hasta . '.xlsx"');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');

                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0*/

          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
          //$objWriter->save();
          $objWriter->save('php://output');
                

            mysqli_free_result($result);
            
   } else
   {
      echo 0;
   }
?>