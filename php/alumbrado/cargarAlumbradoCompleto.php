<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";

   $sql = "SELECT
                Alumbrado_1.Prefijo,
                Alumbrado_1.idAlumbrado_1 AS 'ID',
                Alumbrado_1.OT AS 'OT',
                Alumbrado_1.Tipo AS 'TIPO_OT',
                Alumbrado_1.FechaIngreso AS 'FECHA_EJEC',
                Login.Usuario AS 'EJECUTOR',
                Alumbrado_1.Municipio AS 'MUNICIPIO',
                Alumbrado_1.CD AS 'CD',
                SPLIT_STR(Alumbrado_1.CoorTransformador, ',', 1) AS 'Trafo_nox',
                SPLIT_STR(Alumbrado_1.CoorTransformador, ',', 2) AS 'Trafo_noy',
                Alumbrado_1.CapaTrafo AS 'trafo_capa',
                Alumbrado_1.Fase AS 'trafo_fase',
                Alumbrado_1.Ubicacion AS 'trafo_ubic',
                Alumbrado_1.Direccion AS 'trafo_barr',
                Alumbrado_1.TrafoDireccion AS 'trafo_dir',
                Alumbrado_1.Red AS 'clase_red',
                Alumbrado_1.UsoRed AS 'uso_red',
                Alumbrado_1.CodPoste AS 'punto_sig',
                Alumbrado_1.Luminaria AS 'LUMINARIA', 
                Alumbrado_1.DireccionLuminaria AS 'ILU_DIR', 
                SPLIT_STR(Alumbrado_1.CoordenadasIluminaria, ',', 1) AS 'ilu_nox',
                SPLIT_STR(Alumbrado_1.CoordenadasIluminaria, ',', 2) AS 'ilu_noy',
                Alumbrado_1.TipoControl AS 'ilu_tipoco', 
                Alumbrado_1.CantLuminarias AS 'ilu_cant', 
                Alumbrado_1.TipoIluminarias AS 'ilu_tiplam', 
                Alumbrado_1.TipoLuminaria AS 'ilu_ilumin', 
                Alumbrado_1.Propietario AS 'ilu_propie', 
                Alumbrado_1.Via AS 'ilu_tipvia', 
                Alumbrado_1.TipoControl2 AS 'rele_tipo', 
                SPLIT_STR(Alumbrado_1.CoordenadasRele, ',', 1) AS 'Rele_nox',
                SPLIT_STR(Alumbrado_1.CoordenadasRele, ',', 2) AS 'Rele_noy',
                Alumbrado_1.TipoRed AS 'red_tipseg', 
                Alumbrado_1.Materiales AS 'red_mat', 
                Alumbrado_1.Estado AS 'red_estado', 
                Alumbrado_1.TiempoOperacion AS 'tiempo_op', 
                Alumbrado_1.Red2 AS 'tipo_red', 
                Alumbrado_1.Observaciones AS 'observacio', 
                Alumbrado_1.Estado2 AS 'estado'
            FROM
                Alumbrado_1
                INNER JOIN Login ON Alumbrado_1.idLogin = Login.idLogin
                INNER JOIN Inspecciones ON Alumbrado_1.idInspeccion = Inspecciones.idInspeccion
            WHERE Alumbrado_1.FechaIngreso BETWEEN '$Desde' AND '$Hasta' AND Inspecciones.Estado = 1 AND Inspecciones.Sucursal = 6000;";

//echo $sql;
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
        public $Prefijo;
        public $ID;
        public $OT;
        public $TIPO_OT;
        public $FECHA_EJEC;
        public $EJECUTOR;
        public $MUNICIPIO;
        public $CD;
        public $Trafo_nox;
        public $Trafo_noy;
        public $trafo_capa;
        public $trafo_fase;
        public $trafo_ubic;
        public $trafo_barr;
        public $trafo_dir;
        public $clase_red;
        public $uso_red;
        public $punto_sig;
        public $LUMINARIA;
        public $ILU_DIR;
        public $ilu_nox;
        public $ilu_noy;
        public $ilu_tipoco;
        public $ilu_cant;
        public $ilu_tiplam;
        public $ilu_ilumin;
        public $ilu_propie;
        public $ilu_tipvia;
        public $rele_tipo;
        public $Rele_nox;
        public $Rele_noy;
        public $red_tipseg;
        public $red_mat;
        public $red_estado;
        public $tiempo_op;
        public $tipo_red;
        public $observacio;
        public $estado;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Descripciones[$idx] = new Descripcion();

            $Descripciones[$idx]->Prefijo =  utf8_encode($row['Prefijo']);
            $Descripciones[$idx]->ID =  utf8_encode($row['ID']);
            $Descripciones[$idx]->OT =  utf8_encode($row['OT']);
            $Descripciones[$idx]->TIPO_OT =  utf8_encode($row['TIPO_OT']);
            $Descripciones[$idx]->FECHA_EJEC =  utf8_encode($row['FECHA_EJEC']);
            $Descripciones[$idx]->MUNICIPIO =  utf8_encode($row['MUNICIPIO']);
            $Descripciones[$idx]->EJECUTOR =  utf8_encode($row['EJECUTOR']);
            $Descripciones[$idx]->CD =  utf8_encode($row['CD']);
            $Descripciones[$idx]->Trafo_nox =  utf8_encode($row['Trafo_nox']);
            $Descripciones[$idx]->Trafo_noy =  utf8_encode($row['Trafo_noy']);
            $Descripciones[$idx]->trafo_capa =  utf8_encode($row['trafo_capa']);
            $Descripciones[$idx]->trafo_fase =  utf8_encode($row['trafo_fase']);            
            $Descripciones[$idx]->trafo_ubic =  utf8_encode($row['trafo_ubic']);
            $Descripciones[$idx]->trafo_barr =  utf8_encode($row['trafo_barr']);
            $Descripciones[$idx]->trafo_dir =  utf8_encode($row['trafo_dir']);
            $Descripciones[$idx]->clase_red =  utf8_encode($row['clase_red']);
            $Descripciones[$idx]->uso_red =  utf8_encode($row['uso_red']);
            $Descripciones[$idx]->punto_sig =  utf8_encode($row['punto_sig']);
            $Descripciones[$idx]->LUMINARIA =  utf8_encode($row['LUMINARIA']);
            $Descripciones[$idx]->ILU_DIR =  utf8_encode($row['ILU_DIR']);
            $Descripciones[$idx]->ilu_nox =  utf8_encode($row['ilu_nox']);
            $Descripciones[$idx]->ilu_noy =  utf8_encode($row['ilu_noy']);
            $Descripciones[$idx]->ilu_tipoco =  utf8_encode($row['ilu_tipoco']);
            $Descripciones[$idx]->ilu_cant =  utf8_encode($row['ilu_cant']);
            $Descripciones[$idx]->ilu_tiplam =  utf8_encode($row['ilu_tiplam']);
            $Descripciones[$idx]->ilu_ilumin =  utf8_encode($row['ilu_ilumin']);
            $Descripciones[$idx]->ilu_propie =  utf8_encode($row['ilu_propie']);
            $Descripciones[$idx]->ilu_tipvia =  utf8_encode($row['ilu_tipvia']);
            $Descripciones[$idx]->rele_tipo =  utf8_encode($row['rele_tipo']);
            $Descripciones[$idx]->Rele_nox =  utf8_encode($row['Rele_nox']);
            $Descripciones[$idx]->Rele_noy =  utf8_encode($row['Rele_noy']);
            $Descripciones[$idx]->red_tipseg =  utf8_encode($row['red_tipseg']);
            $Descripciones[$idx]->red_mat =  utf8_encode($row['red_mat']);
            $Descripciones[$idx]->red_estado =  utf8_encode($row['red_estado']);
            $Descripciones[$idx]->tiempo_op =  utf8_encode($row['tiempo_op']);
            $Descripciones[$idx]->tipo_red =  utf8_encode($row['tipo_red']);
            $Descripciones[$idx]->observacio =  utf8_encode($row['observacio']);
            $Descripciones[$idx]->estado =  utf8_encode($row['estado']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>