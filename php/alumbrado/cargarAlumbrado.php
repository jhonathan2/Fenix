<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";

   $sql = "SELECT 
                Inspecciones.idInspeccion AS 'Consecutivo', 
                Inspecciones.fechaIngreso AS 'Fecha',
                Inspecciones.idLogin AS 'idLogin', 
                DatosUsuarios.Nombre AS 'Usuario', 
                Comercial_1.idComercial_1 AS 'idComercial', 
                Comercial_1.OT AS 'OT', 
                Comercial_1.HoraInicio AS 'HoraInicio', 
                Comercial_1.Direccion AS 'Direccion', 
                Comercial_1.TipoInterventoria AS 'TipoInterventoria',
                Comercial_1.SubProceso AS 'SubProceso'                
            FROM 
               Comercial_1,
                DatosUsuarios,
                Inspecciones
            WHERE
               Inspecciones.idLogin = DatosUsuarios.idLogin
                AND Comercial_1.idInspeccion = Inspecciones.idInspeccion
                AND Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
                AND Inspecciones.Estado = 1
                AND Inspecciones.Sucursal = 6000;";
//echo $sql;
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
         public $Consecutivo;
         public $idComercial;
         public $idLogin;
         public $Usuario;
         public $Fecha;
         public $OT;
         public $HoraInicio;
         public $Direccion;
         public $TipoInterventoria;
         public $SubProceso;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->Consecutivo = utf8_encode($row['Consecutivo']);
            $Descripciones[$idx]->idLogin = utf8_encode($row['idLogin']);
            $Descripciones[$idx]->Usuario = utf8_encode($row['Usuario']);
            $Descripciones[$idx]->idComercial = utf8_encode($row['idComercial']);
            $Descripciones[$idx]->Fecha = utf8_encode($row['Fecha']);
            $Descripciones[$idx]->OT = utf8_encode($row['OT']);
            $Descripciones[$idx]->HoraInicio = utf8_encode($row['HoraInicio']);
            $Descripciones[$idx]->Direccion = utf8_encode($row['Direccion']);
            $Descripciones[$idx]->TipoInterventoria = utf8_encode($row['TipoInterventoria']);
            $Descripciones[$idx]->SubProceso = utf8_encode($row['SubProceso']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>