<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";

   $sql = "SELECT 
               NTC2859.* 
            FROM 
               NTC2859
            WHERE
               NTC2859.FechaInicio BETWEEN '$Desde' AND '$Hasta';";

   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Descripciones[$row['Prefijo']] = array();
            foreach ($row as $key => $value) 
            {
              $Descripciones[$row['Prefijo']][$key] = utf8_encode($value);
            }
            $Descripciones[$row['Prefijo']]['Aprobados'] = 0;
            $Descripciones[$row['Prefijo']]['No_Aprobados'] = 0;

            $idx++;
         }

         $sql = "SELECT Prefijo, SUM(Aprobados) AS Aprobados, SUM(No_Aprobados) AS No_Aprobados 
                FROM
                  (SELECT 
                    Prefijo, 
                    if (Resultado = 'Aprobado', 1, 0) AS Aprobados,
                    if (Resultado = 'No Aprobado', 1, 0) AS No_Aprobados
                  FROM 
                    NTC8259_Detalle
                  WHERE 
                    fecha BETWEEN '$Desde' AND '$Hasta') 
                  AS Datos
                  GROUP BY Prefijo";

         $result = $link->query($sql);

         while ($row = mysqli_fetch_assoc($result))
         { 
            foreach ($row as $key => $value) 
            {
              $Descripciones[$row['Prefijo']][$key] = utf8_encode($value);
            }
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>