<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Prefijo = $_POST['Prefijo'];

   $sql = "SELECT 
            NTC8259_Detalle.fecha,
            NTC8259_Detalle.idInspeccion,
            InspecccionTipo.Nombre AS Tipo,
            NTC8259_Detalle.Resultado,
            NTC8259_Detalle.Observaciones
          FROM  NTC8259_Detalle
            INNER JOIN Inspecciones ON Inspecciones.idInspeccion = NTC8259_Detalle.idInspeccion
            INNER JOIN InspecccionTipo ON Inspecciones.idInspeccionTipo = InspecccionTipo.idInspecccionTipo
          WHERE
            NTC8259_Detalle.Prefijo = '$Prefijo';";

   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Descripciones[$idx] = array();
            foreach ($row as $key => $value) 
            {
              $Descripciones[$idx][$key] = utf8_encode($value);
            }

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>