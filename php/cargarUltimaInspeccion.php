<?php
	include("conectar.php"); 
   $link = Conectar();

   $idLogin = addslashes($_POST['idLogin']);
   
   $sql = "SELECT 
            Inspecciones.idInspeccion,
            Inspecciones.fechaIngreso, 
            Inspecciones.idInspeccionTipo,  
            InspecccionTipo.Nombre 
         FROM 
            Inspecciones, 
            InspecccionTipo 
         WHERE 
            InspecccionTipo.idInspecccionTipo = Inspecciones.idInspeccionTipo 
            AND idInspeccion = (SELECT MAX(idInspeccion) FROM Inspecciones WHERE idLogin = '$idLogin')";

   $result = $link->query(utf8_decode($sql));

   if ( $result->num_rows > 0)
   {
      $row = $result->fetch_assoc();
      class Inspeccion
      {
         public $idInspeccion;
         public $fechaIngreso;
         public $idInspeccionTipo;
         public $Nombre;
      }
      
      $Inspecciones = new Inspeccion();
      $Inspecciones->idInspeccion = utf8_encode($row['idInspeccion']);
      $Inspecciones->fechaIngreso = utf8_encode($row['fechaIngreso']);
      $Inspecciones->idInspeccionTipo = utf8_encode($row['idInspeccionTipo']);
      $Inspecciones->Nombre = utf8_encode($row['Nombre']);
         
      mysqli_free_result($result);  
      echo json_encode($Inspecciones);   
   } else
   {
      echo 0;
   }
?>