<?php
	include("conectar.php"); 
   $link = Conectar();

   $Desde = $_POST['Desde'] . " 00:00:00";
   $Hasta = $_POST['Hasta'] . " 23:59:59";
   $Anio = substr($Desde, 0, 4);

   $sql = "SELECT 
            Baremo.Tipo,
            Baremo.Actividad,
            Baremo.Adjudicado$Anio,
            SubZonas.Zona AS 'Zona',
            SubZonas.Sede AS 'SubZona',
            Municipios.Nombre AS 'Municipio',
            COUNT(*) AS 'Cantidad',
            (COUNT(*)*Baremo.Adjudicado$Anio) AS 'Total'
          FROM 
            Inspecciones 
            INNER JOIN Baremo ON Inspecciones.idBaremo = Baremo.idBaremo 
            LEFT JOIN Municipios ON Inspecciones.idMunicipio = Municipios.idMunicipio 
            LEFT JOIN SubZonas ON SubZonas.idSubZona = Municipios.idSubZona
          WHERE
            Inspecciones.Estado = 1
            AND Inspecciones.Sucursal = 6000
            AND Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
          GROUP BY 
              Baremo.Actividad,
              Baremo.Adjudicado$Anio,
              SubZonas.Zona,
              SubZonas.Sede,
              Municipios.Nombre
        UNION ALL
          SELECT 
            Baremo.Tipo,
            Baremo.Actividad,
            Baremo.Adjudicado$Anio,
            SubZonas.Zona AS 'Zona',
            SubZonas.Sede AS 'SubZona',
            Municipios.Nombre AS 'Municipio',
            COUNT(Inspecciones.idInspeccion) AS 'Cantidad',
            (COUNT(Inspecciones.idInspeccion) * Baremo.Adjudicado$Anio) AS 'Total'
          FROM 
            Inspecciones 
            LEFT JOIN Municipios ON Inspecciones.idMunicipio = Municipios.idMunicipio 
            LEFT JOIN SubZonas ON SubZonas.idSubZona = Municipios.idSubZona,
            Baremo 
          WHERE
            Inspecciones.Estado = 1
            AND Inspecciones.Sucursal = 6000
            AND Inspecciones.Veredal = 1
            AND Baremo.idBaremo = 15
            AND Inspecciones.fechaIngreso BETWEEN '$Desde' AND '$Hasta'
          GROUP BY 
              Baremo.Tipo,
              Baremo.Actividad,
              Baremo.Adjudicado$Anio,
              SubZonas.Zona,
              SubZonas.Sede,
              Municipios.Nombre;";

            
//echo $sql;
   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
        public $Tipo;
         public $Actividad;
         public $Adjudicado;
         public $Zona;
         public $SubZona;
         public $Municipio;
         public $Cantidad;
         public $Total;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
          $strAdjudicado = "Adjudicado$Anio";
            $Descripciones[$idx] = new Descripcion();
            $Descripciones[$idx]->Tipo = utf8_encode($row['Tipo']);
            $Descripciones[$idx]->Actividad = utf8_encode($row['Actividad']);
            $Descripciones[$idx]->Zona = utf8_encode($row['Zona']);
            $Descripciones[$idx]->SubZona = utf8_encode($row['SubZona']);
            $Descripciones[$idx]->Municipio = utf8_encode($row['Municipio']);
            $Descripciones[$idx]->Adjudicado = utf8_encode($row[$strAdjudicado]);
            $Descripciones[$idx]->Cantidad = utf8_encode($row['Cantidad']);
            $Descripciones[$idx]->Total = utf8_encode($row['Total']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>