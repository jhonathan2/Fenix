<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Desde = addslashes($_POST['Desde']);
   $Hasta = addslashes($_POST['Hasta']);

   $Condiciones = "";

   if ($Desde <> "")
   {
      $Condiciones = " AND Inspecciones.fechaIngreso >= '" . $Desde . " 00:00:00' ";
   }

   if ($Hasta <> "")
   {
      if ($Condiciones <> "")
      {
         $Condiciones .= " AND Inspecciones.fechaIngreso <= '" . $Hasta . " 23:59:59' ";
      } else
      {
         $Condiciones .= " AND Inspecciones.fechaIngreso <= '" . $Hasta . " 23:59:59' ";
      }
   }

   $sql = "SELECT
            FacturacionDetalle.*
         FROM
            FacturacionDetalle 
            INNER JOIN Inspecciones ON Inspecciones.Prefijo = FacturacionDetalle.PrefijoIns
         WHERE
            Inspecciones.Sucursal = 6000
            Inspecciones.Estado = 1
            $Condiciones;";


   $result = $link->query(utf8_decode($sql));

   if ( $result->num_rows > 0)
   {
      class Facturacion
      {
         public $Consecutivo;
         public $idFacturacionTerreno;
         public $idFacturacion;
         public $Prefijo;
         public $PrefijoIns;
         public $NumCuenta;
         public $NumMedidor;
         public $Sucursal;
         public $Direccion;
         public $Barrio;
         public $Ciclo;
         public $Grupo;
         public $Lectura;
         public $Anomalia;
         public $EntregaFact;
         public $EntregaOpor;
         public $Cumple;
         public $Observaciones;
      }
      
      $idx = 0;
      while ($row = mysqli_fetch_assoc($result))
      {    
         $Descripciones[$idx] = new Facturacion();
         $Descripciones[$idx]->Consecutivo = utf8_encode($row['Consecutivo']);
         $Descripciones[$idx]->idFacturacionTerreno = utf8_encode($row['idFacturacionTerreno']);
         $Descripciones[$idx]->idFacturacion = utf8_encode($row['idFacturacion']);
         $Descripciones[$idx]->Prefijo = utf8_encode($row['Prefijo']);
         $Descripciones[$idx]->PrefijoIns = utf8_encode($row['PrefijoIns']);
         $Descripciones[$idx]->NumCuenta = utf8_encode($row['NumCuenta']);
         $Descripciones[$idx]->NumMedidor = utf8_encode($row['NumMedidor']);
         $Descripciones[$idx]->Sucursal = utf8_encode($row['Sucursal']);
         $Descripciones[$idx]->Direccion = utf8_encode($row['Direccion']);
         $Descripciones[$idx]->Barrio = utf8_encode($row['Barrio']);
         $Descripciones[$idx]->Ciclo = utf8_encode($row['Ciclo']);
         $Descripciones[$idx]->Grupo = utf8_encode($row['Grupo']);
         $Descripciones[$idx]->Lectura = utf8_encode($row['Lectura']);
         $Descripciones[$idx]->Anomalia = utf8_encode($row['Anomalia']);
         $Descripciones[$idx]->EntregaFact = utf8_encode($row['EntregaFact']);
         $Descripciones[$idx]->EntregaOpor = utf8_encode($row['EntregaOpor']);
         $Descripciones[$idx]->Cumple = utf8_encode($row['Cumple']);
         $Descripciones[$idx]->Observaciones = utf8_encode($row['Observaciones']);
         $idx++;
      }
   
      mysqli_free_result($result);  
      echo json_encode($Descripciones);   
   } else
   {
      echo 0;
   }
?>