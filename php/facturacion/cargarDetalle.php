<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Parametro = $_POST['Parametro'];
   $Parametro = explode("_", $Parametro);

   $idFacturacion = $Parametro[0];
   $Grupo = $Parametro[1];

   $sql = "SELECT 
              Facturacion.Fecha AS Fecha,
              Facturacion.HoraIni AS HoraIni,
              Facturacion.HoraFin AS HoraFin,
              Facturacion.Colaboradora AS Colaboradora,
              Facturacion.Municipio AS Municipio,
              Facturacion.SubZona AS SubZona,
              Facturacion.Supervisor AS Supervisor,
              Facturacion.SupervisorCedula AS SupervisorCedula,
              Facturacion.HoraLlegada AS HoraLlegada,
              Facturacion.HoraSalida AS HoraSalida,
              Facturacion.Charla AS Charla,
              Facturacion.CharlaTema AS CharlaTema,
              Facturacion.FormatoSup AS FormatoSup,
              Facturacion.CuadrillasACargo AS Cuadrillas,
              Facturacion.InspeccionesRealizadas AS Inspecciones,
              Facturacion.CumplePlanSup AS CumplePlanSup,
              Facturacion.Observaciones AS Observaciones,
              Facturacion.ObservacionesGenerales AS ObservacionesGenerales
            FROM 
               Facturacion
            WHERE
               Facturacion.idFacturacion = '$idFacturacion'";                

   $result = $link->query($sql);

   if ( $result->num_rows > 0)
   {
      class Descripcion
      {
          public $Fecha;
          public $HoraIni;
          public $HoraFin;
          public $Colaboradora;
          public $Municipio;
          public $SubZona;
          public $Supervisor;
          public $SupervisorCedula;
          public $HoraLlegada;
          public $HoraSalida;
          public $Charla;
          public $CharlaTema;
          public $FormatoSup;
          public $Cuadrillas;
          public $Inspecciones;
          public $CumplePlanSup;
          public $Observaciones;
          public $ObservacionesGenerales;
          public $Detalle;
      }

      class Detalle
      {
        public $Consecutivo;
        public $NumCuenta;
        public $NumMedidor;
        public $Sucursal;
        public $Direccion;
        public $Barrio;
        public $Ciclo;
        public $Grupo;
        public $Lectura;
        public $Anomalia;
        public $EntregaFactura;
        public $EntregaOportuna;
        public $Cumple;
        public $Observaciones;
      }
        $row =  $result->fetch_array(MYSQLI_ASSOC);
      
            $Descripciones = new Descripcion();
            $Descripciones->Fecha  = utf8_encode($row['Fecha']);
            $Descripciones->HoraIni  = utf8_encode($row['HoraIni']);
            $Descripciones->HoraFin  = utf8_encode($row['HoraFin']);
            $Descripciones->Colaboradora  = utf8_encode($row['Colaboradora']);
            $Descripciones->Municipio  = utf8_encode($row['Municipio']);
            $Descripciones->SubZona  = utf8_encode($row['SubZona']);
            $Descripciones->Supervisor  = utf8_encode($row['Supervisor']);
            $Descripciones->SupervisorCedula  = utf8_encode($row['SupervisorCedula']);
            $Descripciones->HoraLlegada  = utf8_encode($row['HoraLlegada']);
            $Descripciones->HoraSalida  = utf8_encode($row['HoraSalida']);
            $Descripciones->Charla  = utf8_encode($row['Charla']);
            $Descripciones->CharlaTema  = utf8_encode($row['CharlaTema']);
            $Descripciones->FormatoSup  = utf8_encode($row['FormatoSup']);
            $Descripciones->Cuadrillas  = utf8_encode($row['Cuadrillas']);
            $Descripciones->Inspecciones  = utf8_encode($row['Inspecciones']);
            $Descripciones->CumplePlanSup  = utf8_encode($row['CumplePlanSup']);
            $Descripciones->Observaciones  = utf8_encode($row['Observaciones']);
            $Descripciones->ObservacionesGenerales  = utf8_encode($row['ObservacionesGenerales']);


        $sql = "SELECT 
                  idFacturacionTerreno,
                  NumCuenta,
                  NumMedidor,
                  Sucursal,
                  Direccion,
                  Barrio,
                  Ciclo,
                  Grupo,
                  Lectura,
                  Anomalia,
                  EntregaFact,
                  EntregaOpor,
                  Cumple,
                  Observaciones
                FROM 
                  FacturacionTerreno
                WHERE  
                  idFacturacion = '$idFacturacion'
                  AND Grupo = '$Grupo';";
                  

       $result = $link->query(utf8_decode($sql));

       if ( $result->num_rows > 0)
       {
          $idx = 0;
             while ($row = mysqli_fetch_assoc($result))
             { 
                $Detalles[$idx] = new Detalle();
                $Detalles[$idx]->Consecutivo = utf8_encode($row['idFacturacionTerreno']);
                $Detalles[$idx]->NumCuenta = utf8_encode($row['NumCuenta']);
                $Detalles[$idx]->NumMedidor = utf8_encode($row['NumMedidor']);
                $Detalles[$idx]->Sucursal = utf8_encode($row['Sucursal']);
                $Detalles[$idx]->Direccion = utf8_encode($row['Direccion']);
                $Detalles[$idx]->Barrio = utf8_encode($row['Barrio']);
                $Detalles[$idx]->Ciclo = utf8_encode($row['Ciclo']);
                $Detalles[$idx]->Grupo = utf8_encode($row['Grupo']);
                $Detalles[$idx]->Lectura = utf8_encode($row['Lectura']);
                $Detalles[$idx]->Anomalia = utf8_encode($row['Anomalia']);
                $Detalles[$idx]->EntregaFactura = utf8_encode($row['EntregaFact']);
                $Detalles[$idx]->EntregaOportuna = utf8_encode($row['EntregaOpor']);
                $Detalles[$idx]->Cumple = utf8_encode($row['Cumple']);
                $Detalles[$idx]->Observaciones = utf8_encode($row['Observaciones']);

                $idx++;
             }
             $Descripciones->Detalle = $Detalles;

       } else
       {
          $Descripciones->Detalle = 0;
       }
            mysqli_free_result($result);  
            echo json_encode($Descripciones);   
    }
    else
    {
      echo 0;
    }
?>
