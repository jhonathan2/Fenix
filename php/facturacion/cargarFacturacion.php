<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Desde = addslashes($_POST['Desde']);
   $Hasta = addslashes($_POST['Hasta']);

   $Condiciones = "";

   if ($Desde <> "")
   {
      $Condiciones = "AND Facturacion.Fecha >= '" . $Desde . " 00:00:00' ";
   }

   if ($Hasta <> "")
   {
      if ($Condiciones <> "")
      {
         $Condiciones .= " AND Facturacion.Fecha <= '" . $Hasta . " 23:59:59' ";
      } else
      {
         $Condiciones .= " AND Facturacion.Fecha <= '" . $Hasta . " 23:59:59' ";
      }
   }

   $sql = "SELECT
            Facturacion.idFacturacion, 
            Facturacion.Fecha,
            Facturacion.Colaboradora,
            Facturacion.Municipio,
            FacturacionTerreno.Grupo,
            COUNT(FacturacionTerreno.Grupo) AS Puntos,
            FacturacionTerreno.Observaciones,
            Facturacion.ObservacionesGenerales,
            DatosUsuarios.Nombre,
            Facturacion.idLogin
         FROM
            Facturacion 
            INNER JOIN FacturacionTerreno ON Facturacion.Prefijo = FacturacionTerreno.Prefijo
            LEFT JOIN DatosUsuarios ON Facturacion.idLogin = DatosUsuarios.idLogin
            INNER JOIN Inspecciones ON Inspecciones.Prefijo = FacturacionTerreno.PrefijoIns
         WHERE
            Inspecciones.Sucursal = 6000
            AND Inspecciones.Estado = 1
            $Condiciones 
         GROUP BY
            Facturacion.Prefijo,
            FacturacionTerreno.Grupo";


   $result = $link->query(utf8_decode($sql));

   if ( $result->num_rows > 0)
   {
      class Facturacion
      {
         public $idFacturacion;
         public $Fecha;
         public $Colaboradora;
         public $Municipio;
         public $Grupo;
         public $Puntos;
         public $Observaciones;
         public $ObservacionesGenerales;
         public $idLogin;
         public $Nombre;
      }
      
      
      $idx = 0;
      while ($row = mysqli_fetch_assoc($result))
      {    
         $Facturaciones[$idx] = new Facturacion();
         $Facturaciones[$idx]->idFacturacion  = utf8_encode($row['idFacturacion']);
         $Facturaciones[$idx]->Fecha  = utf8_encode($row['Fecha']);
         $Facturaciones[$idx]->Colaboradora  = utf8_encode($row['Colaboradora']);
         $Facturaciones[$idx]->Municipio  = utf8_encode($row['Municipio']);
         $Facturaciones[$idx]->Grupo  = utf8_encode($row['Grupo']);
         $Facturaciones[$idx]->Puntos  = utf8_encode($row['Puntos']);
         $Facturaciones[$idx]->Observaciones  = utf8_encode($row['Observaciones']);
         $Facturaciones[$idx]->ObservacionesGenerales  = utf8_encode($row['ObservacionesGenerales']);
         $Facturaciones[$idx]->Nombre  = utf8_encode($row['Nombre']);
         $Facturaciones[$idx]->idLogin  = utf8_encode($row['idLogin']);
         $idx++;
      }
   
      mysqli_free_result($result);  
      echo json_encode($Facturaciones);   
   } else
   {
      echo 0;
   }
?>