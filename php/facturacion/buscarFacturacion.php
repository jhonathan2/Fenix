<?php
	include("../conectar.php"); 
   $link = Conectar();

   $Prefijo = addslashes($_POST['Prefijo']);

   $sql = "SELECT 
            Fecha,
            HoraIni,
            HoraFin,
            Colaboradora,
            Municipio,
            SubZona,
            Supervisor,
            SupervisorCedula,
            HoraLlegada,
            HoraSalida,
            Charla,
            CharlaTema,
            ObservacionesGenerales
         FROM Facturacion WHERE Prefijo = '$Prefijo'";


   $result = $link->query(utf8_decode($sql));

   if ( $result->num_rows > 0)
   {
      class Facturacion
      {
         public $Fecha;
         public $HoraIni;
         public $HoraFin;
         public $Colaboradora;
         public $Municipio;
         public $SubZona;
         public $Supervisor;
         public $SupervisorCedula;
         public $HoraLlegada;
         public $HoraSalida;
         public $Charla;
         public $CharlaTema;
         public $ObservacionesGenerales;
      }
      
      $row = $result->fetch_assoc();

      $Facturaciones = new Facturacion();
      $Facturaciones->Fecha  = utf8_encode($row['Fecha']);
      $Facturaciones->HoraIni  = utf8_encode($row['HoraIni']);
      $Facturaciones->HoraFin  = utf8_encode($row['HoraFin']);
      $Facturaciones->Colaboradora  = utf8_encode($row['Colaboradora']);
      $Facturaciones->Municipio  = utf8_encode($row['Municipio']);
      $Facturaciones->SubZona  = utf8_encode($row['SubZona']);
      $Facturaciones->Supervisor  = utf8_encode($row['Supervisor']);
      $Facturaciones->SupervisorCedula  = utf8_encode($row['SupervisorCedula']);
      $Facturaciones->HoraLlegada  = utf8_encode($row['HoraLlegada']);
      $Facturaciones->HoraSalida  = utf8_encode($row['HoraSalida']);
      $Facturaciones->Charla  = utf8_encode($row['Charla']);
      $Facturaciones->CharlaTema  = utf8_encode($row['CharlaTema']);
      $Facturaciones->ObservacionesGenerales  = utf8_encode($row['ObservacionesGenerales']);
   
      mysqli_free_result($result);  
      echo json_encode($Facturaciones);   
   } else
   {
      echo 0;
   }
?>