<?php
    include("../conectar.php"); 
   $link = Conectar();

   $minimo = $_GET['minimo'];
   $maximo = $_GET['maximo'];
   $sql = "SELECT * FROM ipal_Emgesa WHERE Estado < 1;";
   
   $result = $link->query($sql);
   if ( $result->num_rows > 0)
   {
      class Ipal
      {
        public $idIpal;
		public $Prefijo;
		public $HoraLlegada;
		public $Ipal;
		public $fechaIni;
		public $fechaFin;
		public $Tipo;
		public $Jefe;
		public $Empresa;
		public $Contrato;
		public $NoAsignado;
		public $Central;
		public $Area;
		public $Trabajo;
		public $Lugar;
		public $Cuadrilla;
		public $Evaluacion;
		public $Firma;
		public $Observaciones;
		public $Responsable;
		public $Cargo;
		public $Recibio;
		public $RecibioCargo;
		public $Coordenadas;
		public $Estado;
      }
      
      $idx = 0;
         while ($row = mysqli_fetch_assoc($result))
         { 
            $Ipales[$idx] = new Ipal();
            $Ipales[$idx]->idIpal = utf8_encode($row['idIpal']);
			$Ipales[$idx]->Prefijo = utf8_encode($row['Prefijo']);
			$Ipales[$idx]->HoraLlegada = utf8_encode($row['HoraLlegada']);
			$Ipales[$idx]->Usuario = utf8_encode($row['Usuario']);
			$Ipales[$idx]->fechaIni = utf8_encode($row['fechaIni']);
			$Ipales[$idx]->fechaFin = utf8_encode($row['fechaFin']);
			$Ipales[$idx]->Tipo = utf8_encode($row['Tipo']);
			$Ipales[$idx]->Jefe = utf8_encode($row['Jefe']);
			$Ipales[$idx]->Empresa = utf8_encode($row['Empresa']);
			$Ipales[$idx]->Contrato = utf8_encode($row['Contrato']);
			$Ipales[$idx]->NoAsignado = utf8_encode($row['NoAsignado']);
			$Ipales[$idx]->Central = utf8_encode($row['Central']);
			$Ipales[$idx]->Area = utf8_encode($row['Area']);
			$Ipales[$idx]->Trabajo = utf8_encode($row['Trabajo']);
			$Ipales[$idx]->Lugar = utf8_encode($row['Lugar']);
			$Ipales[$idx]->Cuadrilla = utf8_encode($row['Cuadrilla']);
			$Ipales[$idx]->Evaluacion = utf8_encode($row['Evaluacion']);
			$Ipales[$idx]->Firma = utf8_encode($row['Firma']);
			$Ipales[$idx]->Observaciones = utf8_encode($row['Observaciones']);
			$Ipales[$idx]->Responsable = utf8_encode($row['Responsable']);
			$Ipales[$idx]->Cargo = utf8_encode($row['Cargo']);
			$Ipales[$idx]->Recibio = utf8_encode($row['Recibio']);
			$Ipales[$idx]->RecibioCargo = utf8_encode($row['RecibioCargo']);
			$Ipales[$idx]->Coordenadas = utf8_encode($row['Coordenadas']);
			$Ipales[$idx]->Estado = utf8_encode($row['Estado']);

            $idx++;
         }
         
            mysqli_free_result($result);  
            echo json_encode($Ipales);
   } else
   {
      echo 0;
   }
?>