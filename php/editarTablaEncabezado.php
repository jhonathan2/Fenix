<?php
  include("conectar.php"); 
  $link = Conectar();

  $nomTabla = $_POST['tabla'];
  $sql = "SHOW COLUMNS FROM " . $nomTabla;
  $result = $link->query(utf8_decode($sql));

  $idx = 0;

  class Titulo
    {
      public $Campo;
      public $Tipo;
      public $Nulo;
      public $Llave;
      public $Defecto;
      public $Extra;
    }

  while ($row = mysqli_fetch_assoc($result))
  { 
    $Titulos[$idx] = new Titulo();
    $Titulos[$idx]->Campo = utf8_encode($row['Field']);
    $Titulos[$idx]->Tipo = utf8_encode($row['Type']);
    $Titulos[$idx]->Nulo = utf8_encode($row['Null']);
    $Titulos[$idx]->Llave = utf8_encode($row['Key']);
    $Titulos[$idx]->Defecto = utf8_encode($row['Default']);
    $Titulos[$idx]->Extra = utf8_encode($row['Extra']);

    $idx++;
  }
     
  mysqli_free_result($result);  
  echo json_encode($Titulos);
?>